###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running a test if a dst file contains particles for all fired trigger lines.

Runs over the output of the hlt2_all_lines test, manually running the unpackers
and DecReports decoder, and checks per event that each firing trigger line has
a non-zero number of Particle objects in the expected location.

Takes command-line arguments:

    python hlt2_check_output.py <DST file> <HltANNSvc.Hlt2SelectionID JSON> <HltANNSvc.PackedObjectLocations JSON> <raw_event_format>
"""
from __future__ import print_function
import json
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (ApplicationMgr, CondDB, LHCbApp, HltANNSvc,
                           HltDecReportsDecoder)
from GaudiConf.reading import mc_unpackers, unpackers, decoder


def error(msg):
    print("Hlt2CheckOutput ERROR", error)


LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

print(sys.argv)

# import HltANNSvc configuration from json file
# for decision
with open(sys.argv[2], 'r') as inputfile:
    dict1 = json.load(inputfile)
# convert unicode strings in input file to normal strings
HltANNSvc(Hlt2SelectionID={str(k): v for k, v in dict1.items()})
# for packing
with open(sys.argv[3], 'r') as inputfile:
    dict1 = json.load(inputfile)
HltANNSvc(PackedObjectLocations={str(k): v for k, v in dict1.items()})

raw_event_format = float(sys.argv[4])

dec_reports = HltDecReportsDecoder(SourceID=2)
algs = [dec_reports, decoder(raw_event_format=raw_event_format)]
algs += mc_unpackers() + unpackers()

ApplicationMgr(TopAlg=algs)

IOHelper('ROOT').inputFiles([sys.argv[1]], clear=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

# Flag to record whether we saw any events
# The test can't check anything if no event fired
found_events = False
found_child_relations = False
while TES['/Event']:
    print('Checking next event.')
    # get decReport
    decRep = TES[str(dec_reports.OutputHltDecReportsLocation)].decReports()
    for name, report in decRep.items():
        if report.decision():
            print('Checking line {}'.format(name))
            prefix = '/Event/HLT2/{}'.format(name[:-len("Decision")])
            container = TES[prefix + '/Particles']
            if not container:
                error("no Particles container")
            if container.size() == 0:
                error("empty Particles container")

            relations = TES[prefix + '/Particle2VertexRelations']
            # The CopyParticles algorithm should ensure P2PV relations are saved for the
            # top-level object (the decay head)
            if not relations:
                error("no P2PV relations")
            # Special selection algorithm configuration is required to ensure
            # P2PV relations for the descendents are propagated (they are only
            # created if an object uses PVs in its selection). Here we check
            # that at least some lines have child relations, but don't require
            # it for every line (it's valid for a line not to require PVs to
            # perform its selection).
            if relations.size() > container.size():
                found_child_relations = True

            found_events = True
    appMgr.run(1)

if not found_events:
    error('ERROR: no positive decisions found')
if not found_child_relations:
    error('ERROR: no child P2PV relations found')
