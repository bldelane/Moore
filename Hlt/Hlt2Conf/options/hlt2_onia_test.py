###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 onia lines.

Run like any other options file:

    ./Moore/run gaudirun.py Moore/Hlt/Hlt2Conf/options/Hlt2_onia_test.py
"""
from GaudiKernel.ProcessJobOptions import importOptions
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom

from Hlt2Conf.lines.onia import all_lines

# Enable/disable the usage of hlt1 filtered samples through the hlt1filtered flag

# Comment/uncomment the decay_descriptor line

# Check the event types from the Dirac bookkeeping, and change the simVersion accordingly.
# Check/modify: /eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/getPFNs.py if you get errors due to input files

# Relevant bookkeeping paths:
# before HLT1: /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/<evtNumber>/LDST
# after HLT1: /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/<evtNumber>/LDST

simVersion = 'Sim09c-Up02/Reco-Up01'
trigVersion = '/'

hlt1filtered = True

if hlt1filtered:
    trigVersion = '/Trig0x52000000'

# a list of available samples for testing
#decay_descriptor = '30000000' # MinBias
#decay_descriptor = '11114101' # B0 -> K0S mu+ mu-
#decay_descriptor = '11140400' # B0 -> J/psi omega
#decay_descriptor = '11142401' # B0 -> J/psi p0
decay_descriptor = '12143001'  # B+ -> J/psi K+
#decay_descriptor = '12245021' # B+ -> J/psi K+ pi- pi+
#decay_descriptor = '13144011' # Bs --> Jpsi phi
#decay_descriptor = '15144001' # Lb -> J/psi p K
#decay_descriptor = '16145139' # Xib -> J/psi Lambda K
#decay_descriptor = '16166011' # Lbstar5912 -> Lb pi+pi- (Lb->Lcpi)
#decay_descriptor = '18112001'  # inclusive Upsilon

importOptions('/eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/%s%s/%s/pfns.py' %
              (simVersion, trigVersion, decay_descriptor))
importOptions(
    '/eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/%s/dddbTag.py' % simVersion)
importOptions('/eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/%s/simcondTag.py'
              % simVersion)
importOptions('/eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/%s/ftDecoding.py'
              % simVersion)

options.input_type = 'ROOT'
options.input_raw_format = 4.3  # from Brunel output

# set the options
options.evt_max = 1000
options.simulation = True
options.data_type = 'Upgrade'
options.output_file = 'hlt2_onia_%s_%s.dst' % (simVersion, decay_descriptor)
options.output_type = 'ROOT'


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
