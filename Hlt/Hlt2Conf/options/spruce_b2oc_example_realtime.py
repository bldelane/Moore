###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test running second exclusive hlt2 line on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco.dst

Run like any other options file:

    ./Moore/run gaudirun.py spruce_b2oc_example_realtime.py
"""
import json
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Configurables import HltANNSvc

from Hlt2Conf.lines.b_to_open_charm.b_to_dh import BdToDsmK_DsmToHHH_sprucing_line

input_files = ['hlt2_2or3bodytopo_realtime.dst']

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_realtimereco.dst'
options.output_type = 'ROOT'

with open("toport.hlt2.annsvc.pol.json") as f:
    tck = json.load(f)
    HltANNSvc(
        "HltANNSvcReading",
        PackedObjectLocations={str(k): v
                               for k, v in tck.items()},
        OutputLevel=1)


def make_lines():
    return [
        #BdToDsmPi_DsmToHHH_line(name='Spruce_BdToDsmPi_DsmToHHH_Line'),
        BdToDsmK_DsmToHHH_sprucing_line(name='Spruce_BdToDsmK_DsmToHHH_line')
    ]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    run_moore(options, make_lines, public_tools)

with open('sprucert.spruce.annsvc.pol.json', 'w') as outfile:
    json.dump(
        HltANNSvc("HltANNSvc").PackedObjectLocations,
        outfile,
        indent=4,
        sort_keys=True)
