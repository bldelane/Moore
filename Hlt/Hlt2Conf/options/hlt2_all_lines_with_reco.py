###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines with on-the-fly reconstruction.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_all_lines.py
"""
from __future__ import print_function
import json

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.protoparticles import make_charged_protoparticles
from Hlt2Conf.lines import all_lines
import re

options.evt_max = 200
options.output_file = 'hlt2_all_lines_with_reco.dst'
options.output_type = 'ROOT'
# temporarily use HiveWhiteBoard, see lhcb/LHCb!2878
options.use_iosvc = False
options.event_store = 'HiveWhiteBoard'


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.match(pattern_to_remove, name) is None
    }
    print("Removed lines: ", set(lines_dict) - set(filtered))
    return filtered


# Remove lines which either use gamma or pi0 or jets.
# gamma and pi0 can be added back when Moore!593 is merged.
pattern_to_remove = "(?i)(hlt2jets)"

hlt2_lines = remove_lines(all_lines, pattern_to_remove)

print("Number of HLT2 lines {}".format(len(hlt2_lines)))


def make_lines():
    return [builder() for builder in hlt2_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False),\
     make_charged_protoparticles.bind(enable_muon_id=True):
    run_moore(options, make_lines, public_tools)

# temporarily save the trigger configuration to get it back when reading in the file
from Configurables import HltANNSvc
with open('hlt2_all_lines_with_reco.annsvc.selid.json', 'w') as outfile:
    json.dump(HltANNSvc().Hlt2SelectionID, outfile, indent=4, sort_keys=True)
# temporarily save the PackedObjectLocations to get it back when reading in the file
with open('hlt2_all_lines_with_reco.annsvc.pol.json', 'w') as outfile:
    json.dump(
        HltANNSvc().PackedObjectLocations, outfile, indent=4, sort_keys=True)
