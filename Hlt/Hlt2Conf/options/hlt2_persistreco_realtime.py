###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_testpersistreco_realtime.dst

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_persistreco_realtime.py
"""
import json

from Moore import options, run_moore
from Moore.config import HltLine
from Hlt2Conf.lines.charm.d0_to_hh import make_dzeros, make_charm_kaons
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.protoparticles import make_charged_protoparticles as make_charged_protoparticles_from
from RecoConf.reconstruction_objects import reconstruction, make_pvs, upfront_reconstruction

###Define 2 orthoganol lines (PIDK cut) with and without persistreco


def test_persistreco_line(name='Hlt2_test_persistrecoLine',
                          prescale=1,
                          persistreco=True):
    kaons = make_charm_kaons(pid_cut='PIDK > 15')
    dzeros = make_dzeros(
        particles=kaons, descriptors=['D0 -> K- K+'], pvs=make_pvs())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [dzeros],
        prescale=prescale,
        persistreco=persistreco)


def test_nopersistreco_line(name='Hlt2_test_nopersistrecoLine',
                            prescale=1,
                            persistreco=False):
    kaons = make_charm_kaons(pid_cut='PIDK < 15')
    dzeros = make_dzeros(
        particles=kaons, descriptors=['D0 -> K- K+'], pvs=make_pvs())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [dzeros],
        prescale=prescale,
        persistreco=persistreco)


input_files = [
    # evttype 27163002
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000033_2.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 500
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'hlt2_test_persistreco_realtime.dst'
options.output_type = 'ROOT'


def make_lines():
    return [test_persistreco_line(), test_nopersistreco_line()]


public_tools = [stateProvider_with_simplified_geom()]
# This test runs single-threaded, so we can run the non-thread-safe muon ID
with reconstruction.bind(from_file=False), \
     make_charged_protoparticles_from.bind(enable_muon_id=True):
    run_moore(options, make_lines, public_tools)

# temporarily save the PackedObjectLocations to get it back when reading in the file
from Configurables import HltANNSvc
with open('hlt2_persistreco_realtime.annsvc.pol.json', 'w') as outfile:
    json.dump(
        HltANNSvc().PackedObjectLocations, outfile, indent=4, sort_keys=True)
