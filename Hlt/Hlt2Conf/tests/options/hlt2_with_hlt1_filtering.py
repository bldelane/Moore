###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Demonstrate filtering on HLT1 decisions written by Allen.

This is not intended as an example of how to do HLT1 filtering in HLT2, as
that is not yet supported natively in Moore. This test configures the
necessary algorithms by hand.
"""
from Moore import options, run_moore
from Moore.config import HltLine
from PyConf.Algorithms import (
    LoKi__HDRFilter as HltDecReportsFilter,
    HltDecReportsDecoder,
)
from PyConf.application import default_raw_event
from RecoConf.global_tools import stateProvider_with_simplified_geom


def filter_hlt1_line(name="Hlt2FilterHlt1Line", prescale=1):
    # A standard HLT2 line would use a helper method to create an HLT1 decision
    # filter algorithm, but that helper doesn't yet exist, so in this test we
    # set up the necessary algorithms by hand
    hlt1_dec_reports = HltDecReportsDecoder(
        RawEventLocations=default_raw_event(["HltDecReports"]),
        SourceID=1,
    )
    decision_filter = HltDecReportsFilter(
        Location=hlt1_dec_reports.OutputHltDecReportsLocation,
        # This line name is hard-coded on the test validator logic
        # If you change this line name, also update the test!
        Code="HLT_PASS('Hlt1LowPtMuonDecision')",
    )
    return HltLine(
        name=name,
        algs=[hlt1_dec_reports, decision_filter],
        prescale=prescale,
    )


def make_lines():
    return [filter_hlt1_line()]


public_tools = [stateProvider_with_simplified_geom()]
# Set allen_hlt1 to true if Allen HLT1 filtered files are processed
# For Moore HLT1 filtered files, set allen_hlt1=False (default value)
run_moore(options, make_lines, public_tools, allen_hlt1=True)
