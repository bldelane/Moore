###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options to test if packed reco objects are persisted into dst and if persistreco flag works as expected. Also tests the unpacking of the tracks from pRec/ to Rec/.

Runs over the output file passed as the last argument to this script.

A cleverer version of this script would check the number of packed objects
against the number of objects created upstream in Moore. But we cannot yet
run a GaudiPython job in Moore to get the container counts (see LBCOMP-101),
so instead we use heuristics to estimate the number of objects we should
expect.
"""
from __future__ import print_function
import json
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (ApplicationMgr, CondDB, LHCbApp,
                           HltDecReportsDecoder, HltANNSvc)
from Moore.persistence import DEFAULT_OUTPUT_PREFIX
from GaudiConf.PersistRecoConf import PersistRecoPacking
from GaudiConf.reading import mc_unpackers, unpackers, decoder

# Assume here that each event contains a single D0 -> K+ K- candidate, such
# that we expect exactly 2 tracks for a non-PersistReco triggered event and 3
# particles for any triggered event
N_TURBO = 2


##Helper function for finding positive line decisions
def advance_HLT(decision):
    while True:
        appMgr.run(1)
        if not TES['/Event']:
            sys.exit("Did not find positive {0} decision".format(decision))

        reports = TES['/Event/Hlt/DecReports']
        report = reports.decReport('{0}Decision'.format(decision))
        if report.decision() == 1:
            break

    return


def packed_container_size(container):
    """Return the number of packed objects in the container."""
    # Packed containers have a non-uniform API for accessing the vector of
    # packed objects, so use a dict to resolve the attribute name
    attr = {
        GP.gbl.LHCb.PackedCaloHypos: "hypos",
        GP.gbl.LHCb.PackedTracks: "tracks",
        GP.gbl.LHCb.PackedProtoParticles: "protos",
        GP.gbl.LHCb.PackedRecVertices: "vertices",
    }.get(type(container), "data")
    return getattr(container, attr)().size()


prp = PersistRecoPacking(stream=DEFAULT_OUTPUT_PREFIX, data_type='Upgrade')

##Supply DecReport decoder dict
dict = {
    "Hlt2_test_nopersistrecoLineDecision": 1,
    "Hlt2_test_persistrecoLineDecision": 2
}
HltANNSvc(Hlt2SelectionID={str(k): v for k, v in dict.items()})

# import HltANNSvc configuration from json file
with open(sys.argv[-1], 'r') as inputfile:
    dict1 = json.load(inputfile)
# convert unicode strings in input file to normal strings
HltANNSvc(PackedObjectLocations={str(k): v for k, v in dict1.items()})

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

algs = [decoder()]
algs += [HltDecReportsDecoder()]
#algs += [HltDecReportsDecoder(RawEventLocations="/Event/Trigger/RawEvent",SourceID=2)]
algs += mc_unpackers(stream=DEFAULT_OUTPUT_PREFIX) + unpackers(
    stream=DEFAULT_OUTPUT_PREFIX, data_type='Upgrade')

ApplicationMgr(TopAlg=algs)

IOHelper('ROOT').inputFiles([sys.argv[-2]], clear=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

# needed to check if we didn't find something in TES
not_found = cppyy.bind_object(0, cppyy.gbl.DataObject)

##Here we expect the reconstruction of whole event to be persisted
print('Checking persistreco==True event...')
# Average over 5 events to avoid statistical fluctuations
nevents = 5
npacked_avg = {loc_packed: 0 for loc_packed in prp.packedToOutputLocationMap()}
for ii in range(nevents):
    advance_HLT('Hlt2_test_persistrecoLine')
    if ii == 0:
        TES.dump()
    for loc_packed, loc_unpacked in prp.packedToOutputLocationMap().items():
        packed = TES[loc_packed]
        unpacked = TES[loc_unpacked]
        if packed == not_found:
            print("Unpacking ERROR Unpacked location not found: ", loc_packed)
            npacked = -1
        else:
            npacked = packed_container_size(packed)
        npacked_avg[loc_packed] += npacked
        if unpacked == not_found:
            print("Packing ERROR Packed location not found: ", loc_unpacked)
            nunpacked = -1
        else:
            nunpacked = len(unpacked)
        if npacked != nunpacked:
            print("(Un)packing ERROR Unpacked/packed number mismatched: ",
                  loc_packed, npacked, loc_unpacked, nunpacked)
    # We should still be persisting the HLT2 line candidates
    if packed_container_size(TES["/Event/HLT2/pPhys/Particles"]) < 3:
        print(
            "Persistence ERROR Physics objects are not being persisted in persist reco event"
        )
for loc_packed in prp.packedToOutputLocationMap():
    npacked = 1. * npacked_avg[loc_packed] / nevents
    if npacked_avg[loc_packed] < N_TURBO:
        print("Packing ERROR Too few objects in packed location: ", loc_packed,
              npacked)

##Here we expect only the reconstruction of the selected physics objects to be persisted
print('Checking persistreco==False event...')
# Average over 5 events to avoid statistical fluctuations
nevents = 2
npacked_avg = {loc_packed: 0 for loc_packed in prp.packedToOutputLocationMap()}
for ii in range(nevents):
    advance_HLT('Hlt2_test_nopersistrecoLine')
    for loc_packed, loc_unpacked in prp.packedToOutputLocationMap().items():
        packed = TES[loc_packed]
        unpacked = TES[loc_unpacked]
        if packed == not_found:
            print("Packing ERROR Unpacked location not found: ", loc_packed)
            npacked = -1
        else:
            npacked = packed_container_size(packed)
        npacked_avg[loc_packed] += npacked
        if unpacked == not_found:
            print("Packing ERROR Packed location not found: ", loc_unpacked)
            nunpacked = -1
        else:
            nunpacked = len(unpacked)
        if npacked != nunpacked:
            print("(Un)packing ERROR Unpacked/packed number mismatched: ",
                  loc_packed, npacked, loc_unpacked, nunpacked)
    # We should still be persisting the HLT2 line candidates
    if packed_container_size(TES["/Event/HLT2/pPhys/Particles"]) < 3:
        print(
            "Persistence ERROR Physics objects are not being persisted in persist reco event"
        )
for loc_packed in prp.packedToOutputLocationMap():
    npacked = 1. * npacked_avg[loc_packed] / nevents
    # More than 2 PVs is OK, everything else is not
    if npacked > N_TURBO and "Vertex" not in loc_packed:
        print("Packing ERROR Too many objects in packed location: ",
              loc_packed, npacked)
