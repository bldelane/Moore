###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Selection and combiner wrappers.

Algorithms that inherit from DVCommonBase, like FilterDesktop and
CombineParticles, are not functional and do not expose input/output
DataHandles. They also do some funky internal location mangling to save
additional objects next to the Particle objects they create. The wrappers here
try to work around these traits to make the algorithms behave like any other
functional algorithms.
"""
from __future__ import absolute_import, division, print_function
import string

from Configurables import (
    LoKi__Hybrid__DictTransform_TMVATransform_ as TMVAtransform,
    LoKi__Hybrid__DictOfFunctors as DictOfFunctors,
    LoKi__Hybrid__DictValue as DictValue,
)

from PyConf.Algorithms import (CombineParticles, FilterDesktop,
                               DaVinci__N3BodyDecays as N3BodyDecays,
                               DaVinci__N4BodyDecays as N4BodyDecays)

__all__ = [
    'ParticleFilter', 'ParticleCombiner', 'ParticleFilterWithPVs',
    'ParticleCombinerWithPVs', 'require_all', 'N3BodyCombiner',
    'N3BodyCombinerWithPVs', 'N4BodyCombiner', 'N4BodyCombinerWithPVs',
    'NeutralParticleCombiner', 'NeutralParticleCombinerWithPVs'
]


def require_all(*cuts):
    """Return a cut string requiring all arguments.

    Example:

        >>> require_all('PT > {pt_min}', 'DLLK < {dllk_max}')
        '(PT > {pt_min}) & (DLLK < {dllk_max})'
    """
    cuts = ['({})'.format(c) for c in cuts]
    return ' & '.join(cuts)


def _dvalgorithm_inputs(particle_inputs, pvs):
    """Return a dict suitable for a DVAlgorithm input transform."""
    # ExtraInputs is added by the data handle mixin, so we bundle all inputs
    # there to make them available to the scheduler
    d = {'Inputs': particle_inputs, 'ExtraInputs': particle_inputs}
    if pvs:
        d['InputPrimaryVertices'] = pvs
    return d


def _dvalgorithm_inputs_1(ParticlesA, PrimaryVertices=None):
    return _dvalgorithm_inputs([ParticlesA], PrimaryVertices)


def _dvalgorithm_inputs_2(ParticlesA, ParticlesB, PrimaryVertices=None):
    return _dvalgorithm_inputs([ParticlesA, ParticlesB], PrimaryVertices)


def _dvalgorithm_inputs_3(ParticlesA,
                          ParticlesB,
                          ParticlesC,
                          PrimaryVertices=None):
    return _dvalgorithm_inputs([ParticlesA, ParticlesB, ParticlesC],
                               PrimaryVertices)


def _dvalgorithm_inputs_4(ParticlesA,
                          ParticlesB,
                          ParticlesC,
                          ParticlesD,
                          PrimaryVertices=None):
    return _dvalgorithm_inputs(
        [ParticlesA, ParticlesB, ParticlesC, ParticlesD], PrimaryVertices)


def _dvalgorithm_inputs_5(ParticlesA,
                          ParticlesB,
                          ParticlesC,
                          ParticlesD,
                          ParticlesE,
                          PrimaryVertices=None):
    return _dvalgorithm_inputs(
        [ParticlesA, ParticlesB, ParticlesC, ParticlesD, ParticlesE],
        PrimaryVertices)


def _dvalgorithm_outputs(Particles):
    """Return a dict suitable for a DVAlgorithm output transform."""
    # ExtraOutputs is added by the data handle mixin, so we can add the output
    # there to make it available to the scheduler
    d = {'Output': Particles, 'ExtraOutputs': [Particles]}
    return d


def _dvalgorithm_outputs_with_pvs(Particles, Particle2VertexRelations):
    """Return a dict suitable for a DVAlgorithm-with-PVs output transform.

    Note:
        The name of the `Particle2VertexRelations` argument is special: it
        corresponds to the TES prefix given to P2PV relations by DVAlgorithm.
        By setting the argument name like this, the auto-generated PyConf
        location will coincide with the location DVAlgorithm sets internally.
        This is a hack; DVAlgorithm (or an equivalent replacement) should be
        functional and declare its outputs.
    """
    d = {
        'Output': Particles,
        'ExtraOutputs': [Particles, Particle2VertexRelations]
    }
    return d


def make_dvalgorithm(algorithm, ninputs=1):
    """Return a DVAlgorithm that's wrapped to make it behave nicely."""
    # TODO(AP, NN): Workaround for CombineParticles accepting a list of inputs
    # We have to have one 'Algorithm' wrapper per number of inputs, as we have
    # to have one named input property per input container
    input_transform = {
        1: _dvalgorithm_inputs_1,
        2: _dvalgorithm_inputs_2,
        3: _dvalgorithm_inputs_3,
        4: _dvalgorithm_inputs_4,
        5: _dvalgorithm_inputs_5
    }[ninputs]

    def wrapped(**kwargs):
        uses_pvs = "PrimaryVertices" in kwargs
        output_transform = _dvalgorithm_outputs_with_pvs if uses_pvs else _dvalgorithm_outputs
        return algorithm(
            input_transform=input_transform,
            output_transform=output_transform,
            # If this algorithm consumes PVs, force the creation of P2PV
            # relations tables and write them to the TES; they will be
            # persisted if the line holding this algorithm fires
            WriteP2PVRelations=uses_pvs,
            ForceP2PVBuild=uses_pvs,
            ModifyLocations=False,
            **kwargs)

    return wrapped


filter_desktop = make_dvalgorithm(FilterDesktop)
# Map number of inputs to the combiner that should be used
combiners = {
    1: make_dvalgorithm(CombineParticles),
    2: make_dvalgorithm(CombineParticles, ninputs=2),
    3: make_dvalgorithm(CombineParticles, ninputs=3),
    4: make_dvalgorithm(CombineParticles, ninputs=4),
    5: make_dvalgorithm(CombineParticles, ninputs=5)
}

threebodycombiners = {
    1: make_dvalgorithm(N3BodyDecays),
    2: make_dvalgorithm(N3BodyDecays, ninputs=2),
    3: make_dvalgorithm(N3BodyDecays, ninputs=3),
    4: make_dvalgorithm(N3BodyDecays, ninputs=4)
}

fourbodycombiners = {
    1: make_dvalgorithm(N4BodyDecays),
    2: make_dvalgorithm(N4BodyDecays, ninputs=2),
    3: make_dvalgorithm(N4BodyDecays, ninputs=3),
    4: make_dvalgorithm(N4BodyDecays, ninputs=4)
}


def ParticleFilter(particles, **kwargs):
    """Return a filter algorithm that takes `particles` as inputs.

    Additional keyword arguments are forwarded to FilterDesktop.
    """
    return filter_desktop(ParticlesA=particles, **kwargs).Particles


def ParticleFilterWithPVs(particles, pvs, **kwargs):
    """Return a filter algorithm that takes `particles` and `pvs` as inputs.

    Additional keyword arguments are forwarded to FilterDesktop.
    """
    return ParticleFilter(particles=particles, PrimaryVertices=pvs, **kwargs)


def ParticleFilterWithTMVA(name,
                           particles,
                           pvs,
                           mva_code,
                           mva_name,
                           xml_file,
                           bdt_vars,
                           Key="BDT",
                           **kwargs):
    """Return a filter algorithm that takes `particles`,  the `MVACode`, the `MVA_name`,

    an `XMLFile` and the `BDTVars` as inputs. The `Key` is an optional input.

    Additional keyword arguments are forwarded to FilterDesktop.
    """

    #setup the name for the filter
    particlefiltered = ParticleFilterWithPVs(
        particles=particles,
        pvs=pvs,
        Code=mva_code.format(mva_name=mva_name),
        **kwargs)

    #setup the names for the DictValue, TMVA and MVADict.
    #each tool needs to be named relative to its 'owner'
    dv_name = "{owner}.{mva_name}".format(
        owner=particlefiltered.producer.name, mva_name=mva_name)
    tmva_name = "{owner}.TMVA".format(owner=dv_name)
    mvadict_name = "{owner}.MVAdict".format(owner=tmva_name)

    tmva_source = "LoKi::Hybrid::DictOfFunctors/MVAdict"
    dv_source = "LoKi::Hybrid::DictTransform<TMVATransform>/TMVA"

    Options = {"XMLFile": xml_file, "Name": Key, "KeepVars": "0"}
    # Just need to make sure each Configurable is instantied; don't need to
    # assign them to anything
    TMVAtransform(name=tmva_name, Options=Options, Source=tmva_source)
    DictOfFunctors(name=mvadict_name, Variables=bdt_vars)
    DictValue(name=dv_name, Key=Key, Source=dv_source)

    return particlefiltered


def ParticleCombiner(particles, my_combiners=combiners, **kwargs):
    """Return a combiner algorithm that takes `particles` as inputs.

    Additional keyword arguments are forwarded to CombineParticles.
    """
    particles = particles if isinstance(particles, list) else [particles]
    ninputs = len(particles)

    # Need to dispatch to the right combiner, based on the number of inputs
    assert len(my_combiners
               ) >= ninputs, 'Do not have a combiner for {} inputs'.format(
                   ninputs)
    combiner = my_combiners[ninputs]

    # Map each input container to an input property name
    inputs = {
        'Particles' + letter: p
        for p, letter in zip(particles, string.ascii_uppercase)
    }
    # We need to merge dicts, we make sure we don't have overlapping keys (the
    # caller really shouldn't specify ParticleX keys anyway)
    assert set(inputs).intersection(kwargs) == set()
    kwargs = dict(list(inputs.items()) + list(kwargs.items()))

    return combiner(**kwargs).Particles


def N3BodyCombiner(particles, **kwargs):
    """Return a N3BodyDecays combiner algorithm that takes particles as inputs.

    Additional keyword arguments are forwarded to N3BodyDecays.
    """
    return ParticleCombiner(
        particles, my_combiners=threebodycombiners, **kwargs)


def N4BodyCombiner(particles, **kwargs):
    """Return a N4BodyDecays combiner algorithm that takes particles as inputs.

    Additional keyword arguments are forwarded to N4BodyDecays.
    """
    return ParticleCombiner(
        particles, my_combiners=fourbodycombiners, **kwargs)


def ParticleCombinerWithPVs(particles, pvs, **kwargs):
    """Return a combiner algorithm that takes `particles` and `pvs` as inputs.

    Additional keyword arguments are forwarded to CombineParticles.
    """
    return ParticleCombiner(particles=particles, PrimaryVertices=pvs, **kwargs)


def N3BodyCombinerWithPVs(particles, pvs, **kwargs):
    """Return a combiner algorithm that takes `particles` and `pvs` as inputs.

    Additional keyword arguments are forwarded to CombineParticles.
    """
    ## TODO:  eliminate duplication of code with ParticleCombinerWithPVs
    return N3BodyCombiner(particles=particles, PrimaryVertices=pvs, **kwargs)


def N4BodyCombinerWithPVs(particles, pvs, **kwargs):
    """Return a combiner algorithm that takes `particles` and `pvs` as inputs.

    Additional keyword arguments are forwarded to CombineParticles.
    """
    ## TODO:  eliminate duplication of code with ParticleCombinerWithPVs
    return N4BodyCombiner(particles=particles, PrimaryVertices=pvs, **kwargs)


def NeutralParticleCombinerWithPVs(particles, pvs, **kwargs):
    """Return a combiner algorithm that takes `particles` and `pvs` as inputs.

    No vertex fit is performed, just momentum addition

    Additional keyword arguments are forwarded to CombineParticles.
    """
    return NeutralParticleCombiner(
        particles=particles, PrimaryVertices=pvs, **kwargs)


def NeutralParticleCombiner(particles, **kwargs):
    """Return a combiner algorithm that takes `particles` as input.
    No vertex fit is performed, just momentum addition

    Additional keyword arguments are forwarded to CombineParticles.
    """
    return ParticleCombiner(
        particles=particles, ParticleCombiners={"": "ParticleAdder"}, **kwargs)
