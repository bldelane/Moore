###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Maker functions for jet definitions common across HLT2.
"""
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV

from PyConf.Algorithms import (ParticleMakerForParticleFlow, HltJetBuilderRun3,
                               HltParticleFlowRun3, HltJetTag,
                               HltParticleFlowFilter)

from PyConf.Tools import LoKi__FastJetMaker as FastJet

from .standard_particles import (get_long_track_selector,
                                 get_down_track_selector,
                                 standard_protoparticle_filter)

from PyConf import configurable

from .algorithms import (require_all, ParticleFilter)

from RecoConf.reconstruction_objects import (make_charged_protoparticles as
                                             _make_charged_protoparticles,
                                             make_pvs as _make_pvs)


def get_fastjet():
    return FastJet(
        PtMin=5000,  #  FastJet: min pT
        RParameter=0.5,  #  FastJet: cone size
        Sort=2,  #  FastJet: sort by pt
        Strategy=1,  #  FastJet: strategy: 0 N3Dumb, 1 Best, 2 NlnN, ...,
        Type=2,  #  FastJet: anti-kt code:  0 kt, 1 cambridge, 2 anti-kt, ...
        Recombination=0,  #  FastJet: scheme : 0 E, 1 pT, 2 pT^2, ...
        JetID=98,  # LHCb: Jet PID number
    )


@configurable
def make_pf_particles(make_protoparticles=_make_charged_protoparticles,
                      get_track_selector=get_long_track_selector,
                      make_protoparticle_filter=standard_protoparticle_filter,
                      c_over_e_cut=0):
    chargedProtos = make_protoparticles()
    return ParticleMakerForParticleFlow(
        InputProtoParticles=chargedProtos,
        TrackSelector=get_track_selector(),
        c_over_e_cut=c_over_e_cut,
        ProtoParticleFilter=make_protoparticle_filter()).Output


@configurable
def tag_jets(jets, tags, name="TagJets"):
    return HltJetTag(Jets=jets, Tags=tags, name=name).Output


@configurable
def build_jets(pflow, JetsByVtx=False, name='JetBuilder'):
    return HltJetBuilderRun3(
        Input=pflow,
        PVLocation=_make_pvs(),
        FastJet=get_fastjet(),
        JetsByVtx=JetsByVtx,
        name=name).Output


@configurable
def make_trackjets(name='TrackJets',
                   pt_min=10 * GeV,
                   JetsByVtx=False,
                   tags=None):
    pflow = make_onlytrack_particleflow()
    jets = build_jets(pflow, JetsByVtx, name='JetBuilder' + name)

    if tags is not None:
        taggedjets = tag_jets(jets, tags, name="TagJet" + name)
        jets = taggedjets

    code = require_all("ABSID == 'CELLjet'",
                       "PT > {pt_min}".format(pt_min=pt_min))
    return ParticleFilter(jets, name=name, Code=code)


@configurable
def particleflow_filter(pflow, toban, name="PFFilter"):
    return HltParticleFlowFilter(
        Inputs=pflow, ParticlesToBan=toban, name=name).Output


def make_onlytrack_particleflow(name='PFTracksOnly'):
    return HltParticleFlowRun3(
        Inputs=[
            make_pf_particles(get_track_selector=get_long_track_selector),
            make_pf_particles(get_track_selector=get_down_track_selector)
        ],
        name=name).Output
