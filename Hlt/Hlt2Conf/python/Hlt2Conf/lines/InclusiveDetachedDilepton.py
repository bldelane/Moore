###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive detached dilepton HLT2 lines.
"""
from __future__ import absolute_import, division, print_function

from RecoConf.hlt1_tracking import require_pvs
from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)
from .rd.builders.rdbuilder import make_detached_tracks, make_down_tracks
from .rd.builders.rdbuilder import filter_electron_muon, filter_dielectron, filter_dimuon

all_lines = {}


# make sure we passed GEC and have PV in event
# Turn off GEC for now
def prefilters():
    #return [require_gec(), require_pvs(make_pvs())]
    return [require_pvs(make_pvs())]


@register_line_builder(all_lines)
def incldetdimuon_line(name="Hlt2InclDetDiMuonLine", prescale=1):

    dimuons = filter_dimuon()
    #todo once !593 is merged:
    #add neutrals back in
    extra_outputs = [
        ("extra_pions_nopid", make_detached_tracks()),
        ("extra_down_pions_nopid", make_down_tracks()),
        # ("extra_photons", make_rd_photons()),
        # ("extra_pizeros_r", make_rd_resolved_pi0s()),
        # ("extra_pizeros_m", make_rd_merged_pi0s()),
    ]

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [dimuons],
        extra_outputs=extra_outputs,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def incldetdielectron_line(name="Hlt2InclDetDiElectronLine", prescale=1):

    dielectrons = filter_dielectron()
    #todo once !593 is merged:
    #add neutrals back in
    extra_outputs = [
        ("extra_pions_nopid", make_detached_tracks()),
        ("extra_down_pions_nopid", make_down_tracks()),
        # ("extra_photons", make_rd_photons()),
        # ("extra_pizeros_r", make_rd_resolved_pi0s()),
        # ("extra_pizeros_m", make_rd_merged_pi0s()),
    ]

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [dielectrons],
        extra_outputs=extra_outputs,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def incldetdiemu_line(name="Hlt2InclDetDiEMuLine", prescale=1):

    dielectronmuons = filter_electron_muon()
    #todo once !593 is merged:
    #add neutrals back in
    extra_outputs = [
        ("extra_pions_nopid", make_detached_tracks()),
        ("extra_down_pions_nopid", make_down_tracks()),
        # ("extra_photons", make_rd_photons()),
        # ("extra_pizeros_r", make_rd_resolved_pi0s()),
        # ("extra_pizeros_m", make_rd_merged_pi0s()),
    ]

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [dielectronmuons],
        extra_outputs=extra_outputs,
        prescale=prescale,
    )
