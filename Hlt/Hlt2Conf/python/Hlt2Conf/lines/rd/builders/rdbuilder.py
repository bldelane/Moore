###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of RD builders taking into account RICHes acceptance and radiator thresholds.
Use track reduced chi square and ghost probability used in Run 2
Separate detached and prompt categories. Prompt to be cheched for rate.
Use DLL so far for PID response.
currently more or less copy paste from onia
"""
from GaudiKernel.SystemOfUnits import GeV, MeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleFilterWithTMVA, ParticleFilterWithPVs, ParticleFilter
from PyConf import configurable

from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_kaons, make_long_pions,
    make_long_kaons, make_long_muons, make_long_protons,
    make_long_electrons_no_brem, make_has_rich_long_protons,
    make_has_rich_down_pions, make_photons, make_resolved_pi0s,
    make_merged_pi0s, make_detached_mumu, make_detached_ee, make_detached_mue)

####################################
# Charged hadron selections        #
####################################


@configurable
def make_filter_tracks(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        name="rd_has_rich_long_pions",
        pt_min=250. * MeV,  #TBC with Reco
        p_min=2.0 * GeV,
        p_max=150. * GeV,
        eta_min=2.,
        eta_max=5.,
        trchi2dof_max=4,  #TBC with Reco
        trghostprob_max=0.4,  #TBC with Reco
        mipchi2dvprimary_min=None,
        pid=None):

    code = require_all('PT > {pt_min}', 'in_range({p_min}, P, {p_max})',
                       'in_range({eta_min}, ETA, {eta_max})',
                       'TRCHI2DOF < {trchi2dof_max}',
                       'TRGHOSTPROB < {trghostprob_max}').format(
                           pt_min=pt_min,
                           p_min=p_min,
                           p_max=p_max,
                           eta_min=eta_min,
                           eta_max=eta_max,
                           trchi2dof_max=trchi2dof_max,
                           trghostprob_max=trghostprob_max)

    if pid is not None:
        code += ' & ({})'.format(pid)

    if mipchi2dvprimary_min is not None:
        code += '& (MIPCHI2DV(PRIMARY) > {mipchi2dvprimary_min})'.format(
            mipchi2dvprimary_min=mipchi2dvprimary_min)

    return ParticleFilterWithPVs(
        make_particles(), make_pvs(), name=name, Code=code)


####################################
# Detached                         #
####################################


@configurable
def make_detached_tracks(name="rd_detached_tracks", mipchi2dvprimary_min=3.):
    """
    Return RD detached tracks.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_pions,
        mipchi2dvprimary_min=mipchi2dvprimary_min)


@configurable
def make_detached_muons(
        name="rd_detached_muons",
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDmu > 0.'):
    """
    Return RD detached pions.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_muons,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_detached_electrons(
        name="rd_detached_electrons",
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDe > 0.'):
    """
    Return RD detached pions.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_electrons_no_brem,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_detached_pions(
        name="rd_detached_pions",
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDK <= 0.'):
    """
    Return RD detached pions.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_long_pions,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_detached_kaons(
        name="rd_detached_kaons",
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDK > 0.'):
    """
    Return RD detached kaons.
    """
    return make_filter_tracks(
        make_particles=make_long_kaons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_detached_protons(
        name="rd_detached_protons",
        p_min=10. * GeV,
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDp > 0.'):
    """
    Return RD detached protons.
    """
    return make_filter_tracks(
        make_particles=make_long_protons,
        name=name,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


####################################
# Prompt                           #
####################################


@configurable
def make_prompt_pions(name="rd_prompt_pions", pid='PIDK < 0.'):
    """
    Return RD prompt pions.
    """
    return make_filter_tracks(
        make_particles=make_has_rich_long_pions, name=name, pid=pid)


@configurable
def make_prompt_kaons(name="rd_prompt_kaons", pid='PIDK > 0.'):
    """
    Return RD prompt kaons.
    """
    return make_filter_tracks(
        make_particles=make_has_rich_long_kaons, name=name, pid=pid)


@configurable
def make_prompt_protons(name="rd_prompt_protons",
                        p_min=10. * GeV,
                        pid='PIDp > 0.'):
    """
    Return RD prompt protons.
    """
    return make_filter_tracks(
        make_particles=make_has_rich_long_protons,
        name=name,
        p_min=p_min,
        pid=pid)


####################################
# Downstream tracks                #
####################################
def make_down_tracks(name="rd_down_tracks", pid=None):
    """
    Return RD down hadrons with pion mass hypothesis.
    """
    return make_filter_tracks(
        make_particles=make_down_pions, name=name, pid=pid)


def make_down_pions(name="rd_down_pions", pid=None):
    """
    Return RD down hadrons with pion mass hypothesis.
    """
    return make_filter_tracks(
        make_particles=make_has_rich_down_pions, name=name, pid=pid)


#dont use until !593 is merged
# Play with neutrals
@configurable
def make_rd_photons(name="rd_photons",
                    make_particles=make_photons,
                    CL_min=0.1,
                    et_min=250 * MeV,
                    e_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = require_all('CL > {CL_min}', 'PT > {et_min}', 'P > {e_min}').format(
        CL_min=CL_min, et_min=et_min, e_min=e_min)

    return ParticleFilter(make_particles(), Code=code, name=name)


#dont use until !593 is merged
@configurable
def make_rd_resolved_pi0s(
        name="rd_resolved_pi0s",
        make_particles=make_resolved_pi0s,
        pt_min=250 * MeV,
        p_min=0 * MeV,
        photon_args={
            'ConfLevelCut': 0.1,
            'PtCut': 250. * MeV
        },
):
    """For the time being just a dummy selection"""

    code = require_all('PT > {pt_min}', 'P > {p_min}').format(
        pt_min=pt_min, p_min=p_min)

    return ParticleFilter(
        make_particles(photon_args=photon_args), Code=code, name=name)


#dont use until !593 is merged
@configurable
def make_rd_merged_pi0s(name="rd_merged_pi0s",
                        make_particles=make_merged_pi0s,
                        pt_min=250 * MeV,
                        p_min=0 * MeV):
    """For the time being just a dummy selection"""

    code = require_all('PT > {pt_min}', 'P > {p_min}').format(
        pt_min=pt_min, p_min=p_min)

    return ParticleFilter(make_particles(), Code=code, name=name)


#Take the standard detached dileptons, filter with an MVA and save the necessary related tracks
@configurable
def filter_dimuon(mvacut=0.1):

    dimuons = make_detached_mumu()
    pvs = make_pvs()

    filterdimuonmva_name = "DiMuonMVAFilter"

    MVACode = "VALUE('LoKi::Hybrid::DictValue/{{mva_name}}') > {mvacut}".format(
        mvacut=mvacut)
    XMLFile = "$PARAMFILESROOT/data/Upgrade_InclusiveDetachedDimuon_weights.xml"
    BDTVars = {
        "TwoBody_DIRA_OWNPV": "BPVDIRA()",
        "TwoBody_DOCAMAX": "PFUNA(ADOCAMAX(''))",
        "log(TwoBody_ENDVERTEX_CHI2)": "log(VFASPF(VCHI2/VDOF))",
        "log(TwoBody_FDCHI2_OWNPV)": "log(BPVVDCHI2())",
        "log(TwoBody_MINIPCHI2)": "log(MIPCHI2DV(PRIMARY))",
        "TwoBody_Mcorr": "BPVCORRM()",
        "TwoBody_PT": "PT",
        "log(Track1_MINIPCHI2)": "log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "Track1_PT": "CHILD(PT,1)",
        "log(Track2_MINIPCHI2)": "log(CHILD(MIPCHI2DV(PRIMARY),2))",
        "Track2_PT": "CHILD(PT,2)",
    }
    MVA_name = "TwoBodyMVA"

    filterdimuonmva = ParticleFilterWithTMVA(
        name=filterdimuonmva_name,
        particles=dimuons,
        pvs=pvs,
        mva_code=MVACode,
        mva_name=MVA_name,
        xml_file=XMLFile,
        bdt_vars=BDTVars)

    return filterdimuonmva


#Take the standard detached dileptons, filter with an MVA and save the necessary related tracks
@configurable
def filter_dielectron(mvacut=0.1):

    dielectrons = make_detached_ee()
    pvs = make_pvs()

    filterdielectronmva_name = "DiElectronMVAFilter"

    MVACode = "VALUE('LoKi::Hybrid::DictValue/{{mva_name}}') > {mvacut}".format(
        mvacut=mvacut)
    XMLFile = "$PARAMFILESROOT/data/Upgrade_InclusiveDetachedDielectron_weights.xml"
    BDTVars = {
        "TwoBody_DIRA_OWNPV": "BPVDIRA()",
        "TwoBody_DOCAMAX": "PFUNA(ADOCAMAX(''))",
        "log(TwoBody_ENDVERTEX_CHI2)": "log(VFASPF(VCHI2/VDOF))",
        "log(TwoBody_FDCHI2_OWNPV)": "log(BPVVDCHI2())",
        "log(TwoBody_MINIPCHI2)": "log(MIPCHI2DV(PRIMARY))",
        "TwoBody_Mcorr": "BPVCORRM()",
        "TwoBody_PT": "PT",
        "log(Track1_MINIPCHI2)": "log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "Track1_PT": "CHILD(PT,1)",
        "log(Track2_MINIPCHI2)": "log(CHILD(MIPCHI2DV(PRIMARY),2))",
        "Track2_PT": "CHILD(PT,2)",
    }
    MVA_name = "TwoBodyMVA"

    filterdielectronmva = ParticleFilterWithTMVA(
        name=filterdielectronmva_name,
        particles=dielectrons,
        pvs=pvs,
        mva_code=MVACode,
        mva_name=MVA_name,
        xml_file=XMLFile,
        bdt_vars=BDTVars)

    return filterdielectronmva


#Take the standard detached dileptons, filter with an MVA and save the necessary related tracks
@configurable
def filter_electron_muon(mvacut=0.1):

    diemu = make_detached_mue()
    pvs = make_pvs()

    filterdiemumva_name = "DiElectronMuonMVAFilter"

    MVACode = "VALUE('LoKi::Hybrid::DictValue/{{mva_name}}') > {mvacut}".format(
        mvacut=mvacut)
    XMLFile = "$PARAMFILESROOT/data/Upgrade_InclusiveDetachedElectronMuon_weights.xml"
    BDTVars = {
        "TwoBody_DIRA_OWNPV": "BPVDIRA()",
        "TwoBody_DOCAMAX": "PFUNA(ADOCAMAX(''))",
        "log(TwoBody_ENDVERTEX_CHI2)": "log(VFASPF(VCHI2/VDOF))",
        "log(TwoBody_FDCHI2_OWNPV)": "log(BPVVDCHI2())",
        "log(TwoBody_MINIPCHI2)": "log(MIPCHI2DV(PRIMARY))",
        "TwoBody_Mcorr": "BPVCORRM()",
        "TwoBody_PT": "PT",
        "log(Track1_MINIPCHI2)": "log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "Track1_PT": "CHILD(PT,1)",
        "log(Track2_MINIPCHI2)": "log(CHILD(MIPCHI2DV(PRIMARY),2))",
        "Track2_PT": "CHILD(PT,2)",
    }
    MVA_name = "TwoBodyMVA"

    filterdiemumva = ParticleFilterWithTMVA(
        name=filterdiemumva_name,
        particles=diemu,
        pvs=pvs,
        mva_code=MVACode,
        mva_name=MVA_name,
        xml_file=XMLFile,
        bdt_vars=BDTVars)

    return filterdiemumva
