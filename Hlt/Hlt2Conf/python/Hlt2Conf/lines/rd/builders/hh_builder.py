###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of HH builder for inclusive_radiative_b selections
"""
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable


@configurable
def make_hh(hadrons,
            pvs,
            apt_min=2.0 * GeV,
            num_neutral_tracks_max=2,
            doca_chi2_max=1000.,
            am_max=10.0 * GeV,
            mipchi2dv_max=16.,
            num_charged_tracks_max=2,
            vtx_chi2_max=1000.,
            bpvvdchi2_min=16.,
            eta_min=2.,
            eta_max=5.):
    """Builds two-hadron particle for inclusive_radiative_b"""
    combination_code = require_all(
        "APT > {apt_min}",
        "ANUM((ID=='KS0')|(ABSID=='Lambda0')) < {num_neutral_tracks_max}",
        "ACUTDOCACHI2({doca_chi2_max}, '')", "AM < {am_max}",
        "ANUM((ABSID=='K+') & (MIPCHI2DV(PRIMARY) < {mipchi2dv_max})) < {num_charged_tracks_max}"
    ).format(
        apt_min=apt_min,
        num_neutral_tracks_max=num_neutral_tracks_max,
        doca_chi2_max=doca_chi2_max,
        am_max=am_max,
        mipchi2dv_max=mipchi2dv_max,
        num_charged_tracks_max=num_charged_tracks_max)

    vertex_code = require_all(
        "HASVERTEX", "VFASPF(VCHI2) < {vtx_chi2_max}",
        "BPVVDCHI2() > {bpvvdchi2_min}",
        "in_range({eta_min}, BPVETA(), {eta_max})").format(
            vtx_chi2_max=vtx_chi2_max,
            bpvvdchi2_min=bpvvdchi2_min,
            eta_min=eta_min,
            eta_max=eta_max)

    return ParticleCombinerWithPVs(
        particles=[hadrons],
        pvs=pvs,
        DecayDescriptors=["K*(892)0 -> pi+ pi-"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)
