###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import MeV, mm, picosecond

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.standard_particles import (
    make_has_rich_down_pions,
    make_has_rich_long_pions,
)
from Hlt2Conf.algorithms import (require_all, ParticleFilterWithPVs,
                                 ParticleCombinerWithPVs, ParticleFilter)
from PyConf import configurable

_PION_M = 139.57061


@configurable
def filter_long_pions(particles, pvs, bpvipchi2_min=36):
    code = require_all('BPVIPCHI2() > {bpvipchi2_min}').format(
        bpvipchi2_min=bpvipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


@configurable
def filter_down_pions(particles, pt_min=175 * MeV, p_min=3000 * MeV):
    code = require_all('PT > {pt_min}', 'P > {p_min}').format(
        pt_min=pt_min, p_min=p_min)
    return ParticleFilter(particles, Code=code)


@configurable
def filter_soft_pions(particles, pt_min=200 * MeV):
    code = require_all('PT > {pt_min}').format(pt_min=pt_min)
    return ParticleFilter(particles, Code=code)


@configurable
def make_ll_kaons(particles,
                  pvs,
                  am_dmass=50 * MeV,
                  m_dmass=35 * MeV,
                  vchi2pdof_max=30,
                  bpvltime_min=2.0 * picosecond,
                  vz_min=-100 * mm,
                  vz_max=500 * mm,
                  pt_min=500 * MeV,
                  bpvipchi2_min=4):
    """
    Returns maker for KS0 -> pi+ pi- constructed with two long pions
    """
    combination_code = require_all("ADAMASS('KS0')<{am_dmass}").format(
        am_dmass=am_dmass)

    vertex_code = require_all(
        "ADMASS('KS0')<{m_dmass}", "CHI2VXNDOF<{vchi2pdof_max}",
        "BPVLTIME() > {bpvltime_min}", "PT>{pt_min}", "VFASPF(VZ)>{vz_min}",
        "VFASPF(VZ)<{vz_max}", "BPVIPCHI2()>{bpvipchi2_min}").format(
            m_dmass=m_dmass,
            vchi2pdof_max=vchi2pdof_max,
            bpvltime_min=bpvltime_min,
            pt_min=pt_min,
            vz_min=vz_min,
            vz_max=vz_max,
            bpvipchi2_min=bpvipchi2_min)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=['KS0 -> pi+ pi-'],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dd_kaons(particles,
                  pvs,
                  am_dmass=80 * MeV,
                  m_dmass=64 * MeV,
                  vchi2pdof_max=30,
                  bpvltime_min=0.5 * picosecond,
                  bpvvdz_min=400 * mm,
                  vz_min=300 * mm,
                  vz_max=2275 * mm,
                  pt_min=500 * MeV,
                  bpvipchi2_min=4):
    """
    Returns maker for KS0 -> pi+ pi- constructed with two downstream pions
    """
    combination_code = require_all("ADAMASS('KS0')<{am_dmass}").format(
        am_dmass=am_dmass)

    vertex_code = require_all(
        "ADMASS('KS0')<{m_dmass}", "CHI2VXNDOF<{vchi2pdof_max}",
        "BPVLTIME() > {bpvltime_min}", "BPVVDZ()>{bpvvdz_min}", "PT>{pt_min}",
        "VFASPF(VZ)>{vz_min}", "VFASPF(VZ)<{vz_max}",
        "BPVIPCHI2()>{bpvipchi2_min}").format(
            m_dmass=m_dmass,
            vchi2pdof_max=vchi2pdof_max,
            bpvltime_min=bpvltime_min,
            bpvvdz_min=bpvvdz_min,
            pt_min=pt_min,
            vz_min=vz_min,
            vz_max=vz_max,
            bpvipchi2_min=bpvipchi2_min)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=['KS0 -> pi+ pi-'],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dzeros(particles,
                pvs,
                am_min=1775 * MeV,
                am_max=1955 * MeV,
                sum_child_pt_min=1500 * MeV,
                bpvvdchi2_min=5,
                vchi2pdof_max=10,
                cos_bpvdira_min=0.999401479):
    """
    Returns maker for D0  -> KS0 KS0 constructed with two long-reconstructed
    KS0 or two donstream-reconstructed KS0
    """
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "ACHILD(PT,1) + ACHILD(PT,2)>{sum_child_pt_min}").format(
            am_min=am_min, am_max=am_max, sum_child_pt_min=sum_child_pt_min)

    vertex_code = require_all("BPVVDCHI2()>{bpvvdchi2_min}",
                              "CHI2VXNDOF<{vchi2pdof_max}",
                              "BPVDIRA() > {cos_bpvdira_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  vchi2pdof_max=vchi2pdof_max,
                                  cos_bpvdira_min=cos_bpvdira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=['D0 -> KS0 KS0'],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_ld_dzeros(particles,
                   pvs,
                   am_min=1775 * MeV,
                   am_max=1955 * MeV,
                   sum_child_pt_min=1500 * MeV,
                   bpvvdchi2_min=5,
                   vchi2pdof_max=10,
                   cos_bpvdira_min=0.999401479):
    """
    Returns maker for D0 -> KS0 KS0 constructed with one long-reconstructed
    KS0 and one downstream-reconstructed KS0
    """
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "ACHILD(PT,1) + ACHILD(PT,2)>{sum_child_pt_min}",
        "((ANUM(NINTREE(ISLONG) == 2) == 1) & (ANUM(NINTREE(ISDOWN) == 2) == 1))"
    ).format(
        am_min=am_min, am_max=am_max, sum_child_pt_min=sum_child_pt_min)
    vertex_code = require_all("BPVVDCHI2()>{bpvvdchi2_min}",
                              "CHI2VXNDOF<{vchi2pdof_max}",
                              "BPVDIRA() > {cos_bpvdira_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  vchi2pdof_max=vchi2pdof_max,
                                  cos_bpvdira_min=cos_bpvdira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=['D0 -> KS0 KS0'],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dstars(dzeros,
                soft_pions,
                pvs,
                q_am_min=-75 * MeV - _PION_M,
                q_am_max=170 * MeV - _PION_M,
                q_m_min=-75 * MeV - _PION_M,
                q_m_max=170 * MeV - _PION_M,
                vchi2pdof_max=25):
    """
    Return D*+ maker for tagging a D0 with a soft pion.
    """
    combination_code = require_all(
        "in_range({q_am_min}, (AM - AM1 - AM2), {q_am_max})").format(
            q_am_min=q_am_min,
            q_am_max=q_am_max,
        )

    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}",
        "in_range({q_m_min}, (M - M1 - M2), {q_m_max})").format(
            q_m_min=q_m_min, q_m_max=q_m_max, vchi2pdof_max=vchi2pdof_max)

    return ParticleCombinerWithPVs(
        particles=[dzeros, soft_pions],
        pvs=pvs,
        DecayDescriptors=['D*(2010)+ -> D0 pi+', 'D*(2010)- -> D0 pi-'],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


all_lines = {}


def charm_prefilters():
    """Return a list of prefilters common to charm HLT2 lines."""
    return [require_gec(), require_pvs(make_pvs())]


@register_line_builder(all_lines)
def DsToD0Pi_D0ToKsKsLL_LooseLine(name="Hlt2DsToD0Pi_D0ToKsKsLLLooseLine",
                                  prescale=1):
    pvs = make_pvs()

    long_pions = filter_long_pions(make_has_rich_long_pions(), pvs=pvs)
    long_kaons = make_ll_kaons(long_pions, pvs=pvs)
    dzeros = make_dzeros(long_kaons, pvs=pvs)
    soft_pions = filter_soft_pions(make_has_rich_long_pions())
    dstars = make_dstars(dzeros, soft_pions, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def DsToD0Pi_D0ToKsKsLD_LooseLine(name="Hlt2DsToD0Pi_D0ToKsKsLDLooseLine",
                                  prescale=1):
    pvs = make_pvs()

    long_pions = filter_long_pions(make_has_rich_long_pions(), pvs=pvs)
    long_kaons = make_ll_kaons(long_pions, pvs=pvs)

    down_pions = filter_down_pions(make_has_rich_down_pions())
    down_kaons = make_dd_kaons(down_pions, pvs=pvs)

    dzeros = make_ld_dzeros([long_kaons, down_kaons], pvs=pvs)

    soft_pions = filter_soft_pions(make_has_rich_long_pions())

    dstars = make_dstars(dzeros, soft_pions, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def DsToD0Pi_D0ToKsKsDD_LooseLine(name="Hlt2DsToD0Pi_D0ToKsKsDDLooseLine",
                                  prescale=1):
    pvs = make_pvs()
    down_pions = filter_down_pions(make_has_rich_down_pions())
    down_kaons = make_dd_kaons(down_pions, pvs=pvs)

    dzeros = make_dzeros(down_kaons, pvs=pvs)
    soft_pions = filter_soft_pions(make_has_rich_long_pions())
    dstars = make_dstars(dzeros, soft_pions, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def DsToD0Pi_D0ToKsKsLL_TightLine(name="Hlt2DsToD0Pi_D0ToKsKsLLTightLine",
                                  prescale=1):
    pvs = make_pvs()

    long_pions = filter_long_pions(make_has_rich_long_pions(), pvs=pvs)
    long_kaons = make_ll_kaons(long_pions, pvs=pvs, m_dmass=30 * MeV)
    dzeros = make_dzeros(long_kaons, pvs=pvs)
    soft_pions = filter_soft_pions(make_has_rich_long_pions())
    dstars = make_dstars(dzeros, soft_pions, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def DsToD0Pi_D0ToKsKsLD_TightLine(name="Hlt2DsToD0Pi_D0ToKsKsLDTightLine",
                                  prescale=1):
    pvs = make_pvs()

    long_pions = filter_long_pions(make_has_rich_long_pions(), pvs=pvs)
    long_kaons = make_ll_kaons(long_pions, pvs=pvs, m_dmass=30 * MeV)

    down_pions = filter_down_pions(make_has_rich_down_pions())
    down_kaons = make_dd_kaons(down_pions, pvs=pvs, m_dmass=60 * MeV)

    dzeros = make_ld_dzeros([long_kaons, down_kaons], pvs=pvs)
    soft_pions = filter_soft_pions(make_has_rich_long_pions())
    dstars = make_dstars(dzeros, soft_pions, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def DsToD0Pi_D0ToKsKsDD_TightLine(name="Hlt2DsToD0Pi_D0ToKsKsDDTightLine",
                                  prescale=1):
    pvs = make_pvs()
    down_pions = filter_down_pions(make_has_rich_down_pions())
    down_kaons = make_dd_kaons(down_pions, pvs=pvs, m_dmass=60 * MeV)

    dzeros = make_dzeros(down_kaons, pvs=pvs)
    soft_pions = filter_soft_pions(make_has_rich_long_pions())
    dstars = make_dstars(dzeros, soft_pions, pvs=pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dstars],
        prescale=prescale,
    )
