###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
First version of Jet lines
The missing steps are summarised in the HltJetBuilderRun3, HltParticleFlowRun3 code
"""

from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV

from RecoConf.reconstruction_objects import upfront_reconstruction

from ..algorithms import require_all, ParticleFilter, ParticleCombiner
from PyConf import configurable

from PyConf.Algorithms import HltParticleFlowRun3

from Moore.config import HltLine, register_line_builder

from ..standard_jets import (make_trackjets, make_pf_particles, tag_jets,
                             particleflow_filter, build_jets)
from ..standard_particles import (
    make_photons, make_KsLL, make_KsDD, make_LambdaLL, make_merged_pi0s,
    make_resolved_pi0s, make_detached_mumu, make_ismuon_long_muon)

from ..standard_particles import (
    get_long_track_selector,
    get_down_track_selector,
)

all_lines = {}


@configurable
def make_jets(pflow, pt_min=10 * GeV, JetsByVtx=False, tags=None):
    jets = build_jets(pflow, JetsByVtx)

    if tags is not None:
        taggedjets = tag_jets(jets, tags)
        jets = taggedjets

    code = require_all("ABSID == 'CELLjet'",
                       "PT > {pt_min}".format(pt_min=pt_min))

    return ParticleFilter(jets, Code=code)


@configurable
def make_dijets(jets, pt_min=17 * GeV, dphi=0.0):
    """Make two-jet combinations"""

    descriptors = ["CLUSjet -> CELLjet CELLjet"]

    combination_code = require_all(
        "abs(ACHILD(PHI,1) - ACHILD(PHI,2)) > {dphi}".format(dphi=dphi))

    return ParticleCombiner(
        particles=[jets],
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut="ALL",
        ParticleCombiners={'': 'ParticleAdder'})


@configurable
def make_particleflow():
    return HltParticleFlowRun3(Inputs=[
        make_pf_particles(get_track_selector=get_long_track_selector),
        make_pf_particles(get_track_selector=get_down_track_selector),
        make_KsDD(),
        make_KsLL(),
        make_LambdaLL(),
        make_merged_pi0s(),
        ParticleFilter(
            make_resolved_pi0s(), Code='ALL', CloneFilteredParticles=True),
        # FIXME use just make_resolved_pi0s() above
        # The HltParticleFlowRun3 MergingTransformer expects inputs of type
        # "Particles" (KeyedContainer<Particle, ...>) but make_resolved_pi0s()
        # produces a "Selection" (SharedObjectsContainer<Particle>), which
        # is not accepted. Instead, explicitly produce Particles.
        make_photons(),
    ]).Output


@register_line_builder(all_lines)
@configurable
def jetpt10_func_line(name='Hlt2JetsPt10Line', prescale=1):
    pflow = make_particleflow()
    jets = make_jets(pflow=pflow, pt_min=10 * GeV)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [jets],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def onlytrack_jetpt10_func_line(name='Hlt2TrackJetsPt10Line', prescale=1):
    jets = make_trackjets(pt_min=10 * GeV, name="BuildTrackJets_10GeV_noPV")
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [jets],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def jetpt20_func_line(name='Hlt2JetsPt20Line', prescale=1):
    pflow = make_particleflow()
    jets = make_jets(pflow=pflow, pt_min=20 * GeV)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [jets],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dijetpt10_func_line(name='Hlt2DiJetsPt10Line', prescale=1):
    pflow = make_particleflow()
    jets = make_jets(pflow=pflow, pt_min=10 * GeV)
    dijets = make_dijets(jets)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [dijets],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def detachedmumuplusmujet(name='Hlt2DetachedMuMuPlusMuJetLine', prescale=1):
    dmumu = make_detached_mumu()
    pflow = make_particleflow()
    pflowfiltered = particleflow_filter(
        pflow=pflow, toban=dmumu, name="PFFilterJPsi")
    muons = make_ismuon_long_muon()
    jets = make_jets(pflow=pflowfiltered, tags=muons, pt_min=10 * GeV)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [dmumu, jets],
        prescale=prescale,
    )
