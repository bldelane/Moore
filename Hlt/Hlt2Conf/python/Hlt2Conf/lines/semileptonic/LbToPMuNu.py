###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import

from GaudiKernel.SystemOfUnits import MeV

from Moore.config import HltLine, register_line_builder
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from .builders.base_builder import make_bachelor_muons, make_protons
from PyConf import configurable
from .builders import sl_line_prefilter_topo

all_lines = {}


@register_line_builder(all_lines)
def lbtopmunu_line(name="Hlt2LbToPMuNuLine", prescale=1):
    protons = make_protons(
        p_min=15000 * MeV,
        pt_min=1000 * MeV,
        trch2dof_max=6.,
        mipchi2_min=16.,
        pid='(PIDp>10.) & (PIDp - PIDK > 10.)')
    muons = make_bachelor_muons(
        p_min=3000 * MeV, pt_min=1500 * MeV, mipchi2_min=16)
    lbs = make_lb2pmunu([protons, muons], ["[Lambda_b0 -> p+ mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter_topo() + [lbs],
        prescale=prescale,
        extra_outputs=[])


@configurable
def make_lb2pmunu(particles,
                  descriptors,
                  name="BToPLNuCombiner",
                  am_min=1000 * MeV,
                  vtx_chi2pdof_max=9.0,
                  bpvdira_min=0.9994,
                  lb_pt=1500 * MeV,
                  b_fdchi2=150):

    combination_code = require_all("(AM>{am_min} * MeV)").format(am_min=am_min)
    vertex_code = require_all("VFASPF(VCHI2PDOF)< {vtx_chi2pdof_max}",
                              "BPVDIRA() > {bpvdira_min}", "PT > {lb_pt}",
                              "BPVVDCHI2() >{b_fdchi2}").format(
                                  vtx_chi2pdof_max=vtx_chi2pdof_max,
                                  bpvdira_min=bpvdira_min,
                                  lb_pt=lb_pt,
                                  b_fdchi2=b_fdchi2)

    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)
