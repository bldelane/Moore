###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import

from Moore.config import HltLine, register_line_builder

from .builders.base_builder import make_bachelor_muons
from .builders.charm_hadron_builder import make_d0_tokpi
from .builders.b_builder import make_b2xlnu
from .builders import sl_line_prefilter

all_lines = {}


@register_line_builder(all_lines)
def bctod0munu_d0tokpi_line(name="Hlt2BcToD0MuNu_D0ToKPiLine", prescale=1):
    muons = make_bachelor_muons()
    dzs = make_d0_tokpi()
    bcs = make_b2xlnu(
        particles=[dzs, muons],
        descriptors=["[B_c+ -> D0 mu+]cc", "[B_c- -> D0 mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [bcs],
        prescale=prescale,
    )
