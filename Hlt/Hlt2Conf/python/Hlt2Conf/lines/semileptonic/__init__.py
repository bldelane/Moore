###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the SL HLT2 lines
"""

from . import LbToPMuNu, LbToLcMuNu, BuToD0MuNu_D0ToKPi, BuToD0MuNu_D0ToKPi_FakeMuon, BcToD0MuNu_D0ToKPi

all_lines = {}
all_lines.update(LbToPMuNu.all_lines)
all_lines.update(LbToLcMuNu.all_lines)
all_lines.update(BuToD0MuNu_D0ToKPi.all_lines)
all_lines.update(BuToD0MuNu_D0ToKPi_FakeMuon.all_lines)
all_lines.update(BcToD0MuNu_D0ToKPi.all_lines)
