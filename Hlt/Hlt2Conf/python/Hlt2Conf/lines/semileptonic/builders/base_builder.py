###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Builder for base objects in semileptonic decays.
"""

# format imports according to conventions
from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV
from RecoConf.reco_objects_from_file import make_pvs
from Hlt2Conf.algorithms import (require_all, ParticleFilter, ParticleFilterWithPVs, ParticleCombinerWithPVs, N3BodyCombinerWithPVs)
from Hlt2Conf.standard_particles import (
    standard_protoparticle_filter,
    make_has_rich_long_pions, make_has_rich_long_kaons, make_has_rich_long_protons,
    make_ismuon_long_muon, make_long_muons,
    make_photons, make_resolved_pi0s, make_merged_pi0s)


###################################
# Base for final-state tracks     #
###################################
@configurable
def make_candidate(make_particles=make_has_rich_long_pions,
                   make_pvs=make_pvs,
                   p_min=2. * GeV,
                   pt_min=250. * MeV,
                   trchi2todof_max=3.,
                   trghostprob_max=0.35,
                   mipchi2_min=4.,
                   pid=None,
                   trpchi2_min=None,
                   **kwargs):
    """Base for track selection, for the moment based on B2D0MuNuX."""

    code = require_all('P > {p_min}',
                       'TRCHI2DOF < {trchi2todof_max}',
                       'TRGHOSTPROB < {trghostprob_max}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           p_min=p_min,
                           trchi2todof_max=trchi2todof_max,
                           trghostprob_max=trghostprob_max,
                           mipchi2_min=mipchi2_min)
    if pt_min is not None:
        code += ' & (PT > {pt_min})'.format(pt_min=pt_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    if trpchi2_min is not None:
        code += ' & (TRPCHI2 > {trpchi2_min})'.format(trpchi2_min=trpchi2_min)

    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_pions(pid='PIDK<20.'):
    """Return pions filtered by thresholds in B2D0MuNuX.
    To be extended to common vals in SLWG.
    """
    return make_candidate(make_particles=make_has_rich_long_pions, pid=pid)


@configurable
def make_prong_pions(
        pt_min=150 * MeV,
        mipchi2_min=16.,
        #trp_min=2. * GeV, #check
        #Pi_PROBNNpi2. * GeV, #check
        trghostprob_max=0.4,
        trch2dof_max=4,
        pid='PIDK<8.'):
    """Return pions from taus. Cuts from StdLooseDetachedTau3pi
    DOUBLE CHECK THIS 
    """
    return make_candidate(make_particles=make_has_rich_long_pions, pid=pid, trghostprob_max=trghostprob_max, mipchi2_min = mipchi2_min, trch2dof_max = trch2dof_max, pt_min = pt_min)

@configurable
def make_kaons(pid='PIDK>-2.'):
    """Return kaons filtered by thresholds in B2D0MuNuX.
    To be extended to common vals in SLWG.
    """
    return make_candidate(make_particles=make_has_rich_long_kaons, pid=pid)


@configurable
def make_protons(p_min=8. * GeV,
                 pt_min=250 * MeV,
                 trch2dof_max=3.,
                 mipchi2_min=4.,
                 pid='(PIDp>0.) & (PIDp-PIDK > 0.)'):
    """
    Return protons filtered by thresholds in B2D0MuNu.
    """
    return make_candidate(
        make_particles=make_has_rich_long_protons, p_min=8 * GeV, pid=pid)


@configurable
def make_bachelor_muons(make_particles=make_ismuon_long_muon,
                        p_min=6. * GeV,
                        pt_min=1. * GeV,
                        mipchi2_min=9.,
                        pid='PIDmu>0.0'):
    """Return bachelor muons filtered by thresholds in B2D0MuNuX.
    To be extended to common vals in SLWG.
    """
    return make_candidate(
        make_particles=make_particles,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid)


@configurable
def make_tauons_muonic_decay(p_min=3. * GeV,
                             pt_min = None,
                             mipchi2_min=16.,
                             trghostprob_max=0.5,
                             pid=None):
    """Return tauons filtered by thresholds in Hb2HcTauNu,
    where the tau decays muonically.
    """
    with make_candidate.bind(trghostprob_max=trghostprob_max):
        return make_bachelor_muons(
            p_min=p_min,
            pt_min=pt_min,
            mipchi2_min=mipchi2_min,
            pid=pid)


@configurable
def make_tauons_hadronic_decay(
        particles,
        pvs,
        decay_descriptor="[tau+ -> pi+ pi- pi+]cc",
        pt_max=300 * MeV,
        am_min=400 * MeV,
        am_max=3500 * MeV,
        name="tau_hadronic_builder",
        make_pvs=make_pvs,
        adoca_max=0.15 * mm,
        am_12_min=1670 * MeV,
        am_23_min=1670 * MeV,
        # adoca13_max=0.5 * mm,
        # adoca23_max=0.5 * mm,
        vchi2pdof_max=10,
        bpvdira_min=0.99,
        mipchi2=5):
    """
    DOUBLE CHECK AND RUN IN MOORE LOCALLY
    Parameters
    ----------
    Maker algorithm instances for input particles.
    descriptors : list                                                    
    Decay descriptors to be reconstructed.                                                                         
    make_pvs : callable
    Primary vertex maker function.                                                                                 
    """
    combination12_code = require_all("AM(1,2) > {am12_min}").format(
        am12_min=am12_min)
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "ASUM(PT) > {asumpt_min}",
        "ANUM( PT < {pt_max} ) <= 1",
        "(1<ANUM( ('pi+'==ABSID) & (MIPCHI2DV(PRIMARY)>{mipchi2})))",
    ).format(
        am_min=am_min, am_max=am_max, pt_max=pt_max, mipchi2=mipchi2)
    if am13_min is not None:
        combination_code = require_all(
            combination_code, "AM(1,3) > {am13_min}").format(am13_min=am13_min)
    if am23_min is not None:
        combination_code = require_all(
            combination_code, "AM(2,3) > {am23_min}").format(am23_min=am23_min)
    vertex_code = require_all(
        "M > 400.*MeV", "M < 3500.*MeV", "BPVDIRA>{bpvdira_min}",
        "VFASPF(VCHI2) < 25").format(bpvdira_min=bpvdira_min)

    return N3BodyCombinerWithPVs(
        name=name,
        Inputs=particles,
        pvs=make_pvs(),
        DecayDescriptors=[decay_descriptor],
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)



############################################
# WIP: input to isolation tools            #
############################################
# additions needed:
#   - upstream soft kaons and pions
#   - configure the neutrals
@configurable
def make_soft_long_pions(pid=None,
                         p_min=2. * GeV,
                         pt_min = 100 * MeV):
    """Return pions filtered by thresholds in B2D0MuNuX.
    To be extended to common vals in SLWG.
    """
    return make_candidate(
           make_particles=make_has_rich_long_pions,
           p_min=p_min,
           pt_min=pt_min,
           pid=pid) # selection not finalised


@configurable
def make_soft_long_kaons(pid=None,
                         p_min=2. * GeV,
                         pt_min = 100 * MeV):
    """return pions filtered by thresholds in b2d0munux.
    to be extended to common vals in slwg.
    """
    return make_candidate(
           make_particles=make_has_rich_long_kaons,
           p_min=p_min,
           pt_min=pt_min,
           pid=pid) # selection not finalised


@configurable
def make_photons(make_particles=make_photons, CL_min=0.25, et_min=150 * MeV):
    """Following B2OC: placeholder, input needed for selection"""
    code = require_all('CL > {CL_min}', 'PT > {et_min}').format(
        CL_min=CL_min, et_min=et_min)
    return ParticleFilter(make_particles(), Code=code)


@configurable
def make_resolved_pi0s(make_particles=make_resolved_pi0s, pt_min=0 * MeV):
    """Following B2OC: placeholder, input needed for selection"""
    code = require_all('PT > {pt_min}').format(pt_min=pt_min)
    return ParticleFilter(make_particles(), Code=code)


@configurable
def make_merged_pi0s(make_particles=make_merged_pi0s, pt_min=0 * MeV):
    """Following B2OC: placeholder, input needed for selection"""
    code = require_all('PT > {pt_min}').format(pt_min=pt_min)
    return ParticleFilter(make_particles(), Code=code)


############################################
# Make in_muon candidates for fakemu lines #
############################################
@configurable
def make_inmuon_candidate():
    with standard_protoparticle_filter.bind(Code='PP_HASMUONINFO'):
        return make_long_muons()


@configurable
def make_fake_muons(pid='PIDmu<0.0'):
    """Return bachelor muons filtered by thresholds in B2D0MuNuX.
    To be extended to common vals in SLWG.
    """
    return make_bachelor_muons(pid='PIDmu<0.0')


@configurable
def make_fake_tauons_muonic_decay():
    """Return fake muons used in Hb2HcTauNu analysis, where the tauon
    is suppossed to decay muonically.
    """
    with make_bachelor_muons.bind(make_particles=make_inmuon_candidate):
        return make_tauons_muonic_decay(pid='~ISMUON')
