###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Semileptonic B builder, based on B2OC code.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from PyConf import configurable
from RecoConf.reco_objects_from_file import make_pvs


@configurable
def make_b2xlnu(particles,
                descriptors,
                name="BToXLNuCombiner",
                am_min=2200 * MeV,
                am_max=8000 * MeV,
                b_docachi2_max=10.,
                sum_pt_min=5 * GeV,
                vtx_chi2pdof_max=9.0,
                bpvdira_min=0.999,
                b_d_dzs=-2.0 * mm):
    '''
    Base SL B decay maker: based on B2D0MuNuX stripping line.
    '''
    combination_code = require_all(
        "in_range({am_min}, AM, {am_max})",
        "ADOCACHI2CUT({b_docachi2_max}, '')").format(
            am_min=am_min, am_max=am_max, b_docachi2_max=b_docachi2_max)

    vertex_code = require_all("VFASPF(VCHI2PDOF) < {vtx_chi2pdof_max}",
                              "BPVDIRA() > {bpvdira_min}",
                              "MINTREE(((ABSID=='D+')|(ABSID=='D0')|(ABSID=='Lambda_c+')|(ABSID=='Omega_c0')|(ABSID=='Xi_c+')|(ABSID=='Xi_c0'))"\
                              ", VFASPF(VZ))-VFASPF(VZ) > {b_d_dzs}").format(
                                  vtx_chi2pdof_max=vtx_chi2pdof_max,
                                  bpvdira_min=bpvdira_min,
                                  b_d_dzs=b_d_dzs)

    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_b2xtaunu(particles,
                  descriptors,
                  name="BToXTauNuCombiner",
                  m_min=0. * MeV,
                  m_max=10000. * MeV,
                  am_min=0. * MeV,
                  am_max=11000. * MeV,
                  adoca_max=0.5 * mm,
                  vchi2pdof_max=6,
                  bpvdira_min=0.999,
                  bpvvdchi2_min=50):
    '''
    Semi-tauonic B decay maker: based on B2DplusTauNu stripping line.
    '''
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "AMAXDOCA('') < {adoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       adoca_max=adoca_max)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})", "VFASPF(VCHI2PDOF) < {vchi2pdof_max}",
        "BPVDIRA() > {bpvdira_min}", "BPVVDCHI2() > {bpvvdchi2_min}").format(
            m_min=m_min,
            m_max=m_max,
            vchi2pdof_max=vchi2pdof_max,
            bpvdira_min=bpvdira_min,
            bpvvdchi2_min=bpvvdchi2_min)

    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)

