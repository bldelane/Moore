###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Builder for charmed hadrons in semileptonic decays.
   Based on B2OC upgrade branch.
"""
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from RecoConf.reco_objects_from_file import make_pvs
from PyConf import configurable
import base_builder


@configurable
def make_lambdacs(protons,
                  kaons,
                  pions,
                  pvs,
                  am_min=2080 * MeV,
                  am_max=2480 * MeV,
                  amindocachi2_max=20,
                  vchi2pdof_max=6,
                  bpvdira_min=0.999,
                  bpvvdchi2_min=25):
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "ADOCACHI2CUT({amindocachi2_max}, '')").format(
            am_min=am_min, am_max=am_max, amindocachi2_max=amindocachi2_max)

    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}",
                              "BPVDIRA() > {bpvdira_min}",
                              "BPVVDCHI2() > {bpvvdchi2_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvdira_min=bpvdira_min,
                                  bpvvdchi2_min=bpvvdchi2_min)

    return ParticleCombinerWithPVs(
        particles=[protons, kaons, pions],
        pvs=pvs,
        DecayDescriptors=["[Lambda_c+ -> p+ K- pi+]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


############################################################
# D decay builders, nBody base and mode-specific builders. #
############################################################


# === D0 to 2-body base ===
@configurable
def make_d_to_twobody(particles,
                      descriptors,
                      name="DTo2BodyBuilder",
                      make_pvs=make_pvs,
                      am_min=1784.84 * MeV,
                      am_max=1944.84 * MeV,
                      amindocachi2_max=20,
                      vchi2pdof_max=6,
                      bpvvdchi2_min=25,
                      bpvdira_min=0.99):
    """
    A base D>2Body builder.
    Based on B2OC template and cuts in B2DMuNuX_D0 line.


    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "ADOCACHI2CUT({amindocachi2_max}, '')").format(
            am_min=am_min, am_max=am_max, amindocachi2_max=amindocachi2_max)

    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvdira_min=bpvdira_min)
    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# === D0 to Kpi ===
@configurable
def make_d0_tokpi(**decay_arguments):
    """
    A base D0->2Body builder.
    Based on B2OC template and cuts in B2DMuNuX_D0 line.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the D->2Body
                        builder.
    """
    particles = [base_builder.make_kaons(), base_builder.make_pions()]
    descriptors = ['D0 -> pi+ K-', 'D0 -> K+ pi-']

    return make_d_to_twobody(
        particles, descriptors, name="D0ToKPiBuilder", **decay_arguments)


# === Generic Hc builder ===
@configurable
def make_Hc_to_nbody(num_daughters,
                     particles,
                     descriptors,
                     am_min,
                     am_max,
                     name="HcToNBodyBuilder",
                     make_pvs=make_pvs,
                     apt_min=2000 * MeV,
                     apt_any_min=None,
                     apt_sum_min=None,
                     amindoca_max=0.1 * mm,
                     vchi2pdof_max=4,
                     bpvdira_min=0.999,
                     bpvvdchi2_min=25):
    """
    A base builder for a generic Hc -> n body decay, with n <= 4.

    Parameters
    ----------
    num_daughters :
        Number of daughter particles in the decay.
    particles :
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    am_min :
        Lower invariant mass limit for the particle combination.
    am_max :
        Upper invariant mass limit for the particle combination.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """

    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})", "APT > {apt_min}",
        "AMINDOCA('') < {amindoca_max}").format(
            am_min=am_min,
            am_max=am_max,
            apt_min=apt_min,
            amindoca_max=amindoca_max)

    if apt_any_min is not None:
        combination_code += " & ((APT1 > {apt_any_min})".format(apt_any_min=apt_any_min)
        for i in range(2,num_daughters+1):
            combination_code += " | (APT"+str(i)+" > {apt_any_min})".format(apt_any_min=apt_any_min)
        combination_code += ")"

    if apt_sum_min is not None:
        combination_code += " & ((APT1"
        for i in range(2,num_daughters+1):
            combination_code += "+APT"+str(i)
        combination_code += ") > {apt_sum_min})".format(apt_sum_min=apt_sum_min)

    mother_code = require_all("VFASPF(VCHI2PDOF) < {vchi2pdof_max}",
                              "BPVDIRA() > {bpvdira_min}",
                              "BPVVDCHI2() > {bpvvdchi2_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvdira_min=bpvdira_min,
                                  bpvvdchi2_min=bpvvdchi2_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=mother_code)


# === Dplus to Kpipi ===
@configurable
def make_dplus_tokpipi(name="DpToKPiPiBuilder",
                       am_min=1830 * MeV,
                       am_max=1910 * MeV,
                       apt_any_min=800 * MeV,
                       apt_sum_min=2500 * MeV,
                       daughter_p_min=5 * GeV,
                       daughter_pt_min=300 * MeV,
                       daughter_trghostprob_max=0.5,
                       daughter_trpchi2_min=None,
                       daughter_mipchi2_min=9.,
                       kaon_pid='PIDK>4.',
                       pion_pid='PIDK<2.'):

    """
    A base builder for the decay D+->K-pi+pi+.
    Selection based on the muonic R(D+) analysis.
    """

    with base_builder.make_candidate.bind(
            p_min=daughter_p_min,
            pt_min=daughter_pt_min,
            trghostprob_max=daughter_trghostprob_max,
            trpchi2_min=daughter_trpchi2_min,
            mipchi2_min=daughter_mipchi2_min):
        particles = [base_builder.make_kaons(pid=kaon_pid), base_builder.make_pions(pid=pion_pid)]

    descriptors = ["[D+ -> K- pi+ pi+]cc"]

    return make_Hc_to_nbody(3, particles, descriptors, am_min, am_max, name=name, apt_any_min=apt_any_min, apt_sum_min=apt_sum_min)


# === Dsplus to KKpi ===
@configurable
def make_ds_tokkpi(name="DsToKKPiBuilder",
                       am_min=1920 * MeV,
                       am_max=2010 * MeV,
                       apt_any_min=800 * MeV,
                       apt_sum_min=2500 * MeV,
                       daughter_p_min=5 * GeV,
                       daughter_pt_min=300 * MeV,
                       daughter_trghostprob_max=0.5,
                       daughter_mipchi2_min=9.,
                       kaon_pid='PIDK>4.',
                       pion_pid='PIDK<2.'):

    """
    A base builder for the decay Ds+->K+K-pi+.
    Selection based on the muonic R(Ds+) analysis.
    """

    with base_builder.make_candidate.bind(
            p_min=daughter_p_min,
            pt_min=daughter_pt_min,
            trghostprob_max=daughter_trghostprob_max,
            mipchi2_min=daughter_mipchi2_min):
        particles = [base_builder.make_kaons(pid=kaon_pid), base_builder.make_pions(pid=pion_pid)]

    descriptors = ["[D_s+ -> K+ K- pi+]cc"]

    return make_Hc_to_nbody(3, particles, descriptors, am_min, am_max, name=name, apt_any_min=apt_any_min, apt_sum_min=apt_sum_min)


