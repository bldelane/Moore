###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from Hlt2Conf.lines.topological_b import require_topo_candidate

def sl_line_prefilter():
    filters = upfront_reconstruction()
    filters.append(require_gec())
    filters.append(require_pvs(make_pvs()))
    return filters

def sl_line_prefilter_topo():
    filters = upfront_reconstruction()
    filters.append(require_gec())
    filters.append(require_pvs(make_pvs()))
    filters.append(require_topo_candidate())
    return filters
