###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import

from Moore.config import HltLine, register_line_builder

from .builders.base_builder import make_bachelor_muons, make_fake_muons
from .builders.charm_hadron_builder import make_d0_tokpi
from .builders.b_builder import make_b2xlnu
from .builders import sl_line_prefilter

all_lines = {}

# B+->D0(->KPi)MuNu line
@register_line_builder(all_lines)
def butod0munu_d0tokpi_line(name="Hlt2BuToD0MuNu_D0ToKPiLine", prescale=1):
    muons = make_bachelor_muons()
    dzs = make_d0_tokpi()
    bus = make_b2xlnu(
        particles=[dzs, muons],
        descriptors=["[B- -> D0 mu-]cc", "[B+ -> D0 mu+]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [bus],
        prescale=prescale,
    )

# fakemuon line with DLLmu<0 on companion muon
@register_line_builder(all_lines)
def butod0munu_d0tokpi_fakemuon_line(name="Hlt2BuToD0MuNu_D0ToKPi_FakeMuonLine", prescale=1):
    muons = make_fake_muons()
    dzs = make_d0_tokpi()
    bus = make_b2xlnu(
        particles=[dzs, muons],
        descriptors=["[B- -> D0 mu-]cc", "[B+ -> D0 mu+]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [bus],
        prescale=prescale,
    )
