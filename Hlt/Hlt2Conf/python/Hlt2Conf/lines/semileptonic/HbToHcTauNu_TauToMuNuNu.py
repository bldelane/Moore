###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore.config import HltLine, register_line_builder

from builders.base_builder import make_tauons_muonic_decay, make_fake_tauons_muonic_decay, make_pions, make_kaons
from builders.charm_hadron_builder import make_dplus_tokpipi, make_ds_tokkpi
from builders.b_builder import make_b2xtaunu

from builders import sl_line_prefilter

all_lines = {}


@register_line_builder(all_lines)
def b0todptaunu_dptokpipi_tautomununu_line(name="Hlt2B0ToDpTauNu_DpToKPiPi_TauToMuNuNuLine",
                            prescale=1):
    kaons = make_kaons()
    pions = make_pions()
    tauons = make_tauons_muonic_decay()
    dps = make_dplus_tokpipi()
    b0s = make_b2xtaunu(
        particles=[dps, tauons],
        descriptors=["[B+ -> D- mu+]cc", "[B+ -> D- mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [b0s],
        extra_outputs=[
            ("Kaons", kaons),
            ("Pions", pions),
            ("Muons", tauons),
        ],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def b0todptaunu_dptokpipi_fakemuon_line(name="Hlt2B0ToDpTauNu_DpToKPiPi_FakeMuonLine",
                            prescale=0.1):
    kaons = make_kaons()
    pions = make_pions()
    fake_tauons = make_fake_tauons_muonic_decay()
    dps = make_dplus_tokpipi()
    b0s = make_b2xtaunu(
        particles=[dps, fake_tauons],
        descriptors=["[B+ -> D- mu+]cc", "[B+ -> D- mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [b0s],
        extra_outputs=[
            ("Kaons", kaons),
            ("Pions", pions),
            ("Muons", fake_tauons),
        ],
        prescale=prescale,
    )



@register_line_builder(all_lines)
def bstodstaunu_dstokkpi_tautomununu_line(name="Hlt2BsToDsTauNu_DsToKKPi_TauToMuNuNuLine",
                            prescale=1):
    kaons = make_kaons()
    pions = make_pions()
    tauons = make_tauons_muonic_decay()
    dss = make_ds_tokkpi()
    bss = make_b2xtaunu(
        particles=[dss, tauons],
        descriptors=["[B_s0 -> D_s- mu+]cc", "[B_s~0 -> D_s+ mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [bss],
        extra_outputs=[
            ("Kaons", kaons),
            ("Pions", pions),
            ("Muons", tauons),
        ],
        prescale=prescale,
    )

@register_line_builder(all_lines)
def bstodstaunu_dstokkpi_fakemuon_line(name="Hlt2BsToDsTauNu_DsToKKPi_FakeMuonLine",
                            prescale=1):
    kaons = make_kaons()
    pions = make_pions()
    fake_tauons = make_fake_tauons_muonic_decay()
    dss = make_ds_tokkpi()
    bss = make_b2xtaunu(
        particles=[dss, fake_tauons],
        descriptors=["[B_s0 -> D_s- mu+]cc", "[B_s~0 -> D_s+ mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [bss],
        extra_outputs=[
            ("Kaons", kaons),
            ("Pions", pions),
            ("Muons", fake_tauons),
        ],
        prescale=prescale,
    )
