# ###############################################################################
# # (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
# #                                                                             #
# # This software is distributed under the terms of the GNU General Public      #
# # Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
# #                                                                             #
# # In applying this licence, CERN does not waive the privileges and immunities #
# # granted to it by virtue of its status as an Intergovernmental Organization  #
# # or submit itself to any jurisdiction.                                       #
# ###############################################################################

# from Moore.config import HltLine, register_line_builder

# from builders import sl_line_prefilter
# from builders.charm_hadron_builder import make_dplus_tokpipi, make_ds_tokkpi
# from builders.b_builder import make_b2xtaunu
# from builders.base_builder import make_pions, make_kaons, make_tauons_hadronic_decay, make_prong_pions

# all_lines = {}
# """
# need to double check the tau hadronic and prong pions builders
# """

# @register_line_builder(all_lines)
# def b0todptaunu_dptokpipi_tautopipipinu_line(
#         name="Hlt2B0ToDpTauNu_DpToKPiPi_TauToPiPiPiNuLine", prescale=1):
#     kaons = make_kaons()
#     pions = make_pions()
#     pis = make_prong_pions()
#     tauons = make_tauons_hadronic_decay(particles=[pis, pis, pis])
#     dps = make_dplus_tokpipi(daughter_trpchi2_min = 0.01)
#     b0s = make_b2xtaunu(
#         particles=[dps, tauons],
#         descriptors=["[B+ -> D- tau+]cc", "[B+ -> D- tau-]cc"])

#     return HltLine(
#         name=name,
#         algs=sl_line_prefilter() + [b0s],
#         extra_outputs=[
#             ("Kaons", kaons),
#             ("Pions", pions),
#             ("Pis", tauons),
#         ],
#         prescale=prescale,
#     )
