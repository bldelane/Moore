###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import

from Moore.config import HltLine, register_line_builder
from RecoConf.reconstruction_objects import make_pvs

from .builders.base_builder import make_bachelor_muons, make_protons, make_pions, make_kaons
from .builders.charm_hadron_builder import make_lambdacs
from .builders.b_builder import make_b2xlnu
from .builders import sl_line_prefilter

all_lines = {}


@register_line_builder(all_lines)
def lbtolcmunu_lctopkpi_line(name="Hlt2LbToLcMuNu_LcToPKPiLine", prescale=1):
    pvs = make_pvs()
    protons = make_protons()
    kaons = make_kaons()
    pions = make_pions()
    muons = make_bachelor_muons()
    lcs = make_lambdacs(protons, kaons, pions, pvs)
    lbs = make_b2xlnu([lcs, muons], ["[Lambda_b0 -> Lambda_c+ mu-]cc"])

    return HltLine(
        name=name,
        algs=sl_line_prefilter() + [lbs],
        prescale=prescale,
    )
