###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of exclusive B+->psiX HLT2 lines
"""

from __future__ import absolute_import, division, print_function
from PyConf import configurable

from Moore.config import HltLine, register_line_builder

#get onia builders
from .builders.prefilters import make_prefilters
from .builders import dimuon, charged_hadrons, b_hadrons

all_lines = {}

######################
# B+ -> J/psiK+     #
######################


@register_line_builder(all_lines)
@configurable
def BuToJpsiKp_JpsiToMuMu_line(name='Hlt2_onia_BpToJpsiKp_JpsiToMuMu_Line',
                               prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ line"""
    jpsi = dimuon.make_jpsi()
    kaons = charged_hadrons.make_detached_kaons()
    Bu2JpsiK = b_hadrons.make_onia_Bu(
        particles=[jpsi, kaons], descriptors=['[B+ -> J/psi(1S) K+]cc'])
    return HltLine(
        name=name, algs=make_prefilters() + [Bu2JpsiK], prescale=prescale)


######################
# B+ -> psi(2S)K+     #
######################


@register_line_builder(all_lines)
@configurable
def BuToPsi2SKp_Psi2SToMuMu_line(name='Hlt2_onia_BpToPsi2SKp_Psi2SToMuMu_Line',
                                 prescale=1):
    """B+ --> psi(2S)(-> mu+ mu-)  K+ line"""
    psi = dimuon.make_psi2s()
    kaons = charged_hadrons.make_detached_kaons()
    Bu2psiK = b_hadrons.make_onia_Bu(
        particles=[psi, kaons], descriptors=['[B+ -> psi(2S) K+]cc'])
    return HltLine(
        name=name, algs=make_prefilters() + [Bu2psiK], prescale=prescale)


######################
# B+ -> J/psi pi+    #
######################


@register_line_builder(all_lines)
@configurable
def BuToJpsiPip_JpsiToMuMu_line(name='Hlt2_onia_BpToJpsiPip_JpsiToMuMu_Line',
                                prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  pi+ line"""
    jpsi = dimuon.make_jpsi()
    pions = charged_hadrons.make_detached_pions()
    Bu2JpsiPi = b_hadrons.make_onia_Bu(
        particles=[jpsi, pions], descriptors=['[B+ -> J/psi(1S) pi+]cc'])
    return HltLine(
        name=name, algs=make_prefilters() + [Bu2JpsiPi], prescale=prescale)


######################
# B+ -> psi(2S) pi+    #
######################


@register_line_builder(all_lines)
@configurable
def BuToPsi2SPip_Psi2SToMuMu_line(
        name='Hlt2_onia_BpToPsi2SPip_Psi2SToMuMu_Line', prescale=1):
    """B+ --> psi(2S)(-> mu+ mu-)  pi+ line"""
    psi = dimuon.make_psi2s()
    pions = charged_hadrons.make_detached_pions()
    Bu2psiPi = b_hadrons.make_onia_Bu(
        particles=[psi, pions], descriptors=['[B+ -> psi(2S) pi+]cc'])
    return HltLine(
        name=name, algs=make_prefilters() + [Bu2psiPi], prescale=prescale)
