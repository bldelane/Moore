###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of exclusive Bc+->psiX HLT2 lines
"""

from __future__ import absolute_import, division, print_function
from PyConf import configurable

from Moore.config import HltLine, register_line_builder

#get onia builders
from .builders.prefilters import make_prefilters
from .builders import dimuon, charged_hadrons, b_hadrons

all_lines = {}

######################
# Bc+ -> J/psipi+    #
######################


@register_line_builder(all_lines)
@configurable
def BcToJpsiPip_JpsiToMuMu_line(name='Hlt2_onia_BcToJpsiPip_JpsiToMuMu_Line',
                                prescale=1):
    """Bc+ --> Jpsi(-> mu+ mu-)  pi+ line"""
    jpsi = dimuon.make_jpsi()
    pions = charged_hadrons.make_prompt_pions()
    Bc2JpsiPi = b_hadrons.make_onia_Bc(
        particles=[jpsi, pions], descriptors=['[B_c+ -> J/psi(1S) pi+]cc'])
    return HltLine(
        name=name, algs=make_prefilters() + [Bc2JpsiPi], prescale=prescale)


######################
# Bc+ -> psi(2S)pi+  #
######################


@register_line_builder(all_lines)
@configurable
def BcToPsi2SPip_Psi2SToMuMu_line(
        name='Hlt2_onia_BcToPsi2SPip_Psi2SToMuMu_Line', prescale=1):
    """Bc+ --> psi(2S)(-> mu+ mu-)  pi+ line"""
    psi = dimuon.make_psi2s()
    pions = charged_hadrons.make_prompt_pions()
    Bc2psiPi = b_hadrons.make_onia_Bc(
        particles=[psi, pions], descriptors=['[B_c+ -> psi(2S) pi+]cc'])
    return HltLine(
        name=name, algs=make_prefilters() + [Bc2psiPi], prescale=prescale)
