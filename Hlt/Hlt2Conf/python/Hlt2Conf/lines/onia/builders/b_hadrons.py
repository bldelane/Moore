###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q b-hadrons
"""
from GaudiKernel.SystemOfUnits import MeV, picosecond

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from PyConf import configurable

####################################
# b-hadron makers                  #
####################################


@configurable
def make_onia_b_hadron(particles,
                       descriptors,
                       name="onia_b_hadron",
                       am_min=5100 * MeV,
                       am_max=5550 * MeV,
                       m_min=5140 * MeV,
                       m_max=5510 * MeV,
                       vtx_chi2pdof_max=20,
                       bpvltime_min=0.2 * picosecond):

    combination_code = require_all('in_range({am_min}, AM, {am_max})').format(
        am_min=am_min, am_max=am_max)

    vertex_code = require_all('in_range({m_min}, M , {m_max})',
                              'VFASPF(VCHI2PDOF) < {vtx_chi2pdof_max}',
                              'BPVLTIME(25) > {bpvltime_min}').format(
                                  m_min=m_min,
                                  m_max=m_max,
                                  vtx_chi2pdof_max=vtx_chi2pdof_max,
                                  bpvltime_min=bpvltime_min)

    return ParticleCombinerWithPVs(
        name=name,
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


####################################
# B0/B0s                           #
####################################


@configurable
def make_onia_Bds(particles, descriptors, name="onia_Bds"):
    """
    Return B&Q B0 and Bs with charged final state particles
    """
    return make_onia_b_hadron(particles, descriptors, name=name)


@configurable
def make_onia_withNeutrals_Bds(particles,
                               descriptors,
                               name="onia_withNeutrals_Bds",
                               am_min=4500 * MeV,
                               am_max=6000 * MeV,
                               m_min=4550 * MeV,
                               m_max=5950 * MeV,
                               bpvltime_min=0.3 * picosecond):
    """
    Return B&Q B0 and Bs with final state involving neutrals
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min)


####################################
# B+                               #
####################################


@configurable
def make_onia_Bu(particles, descriptors, name="onia_Bu"):
    """
    Return B&Q B+.
    """
    return make_onia_b_hadron(particles, descriptors, name=name)


@configurable
def make_onia_withNeutrals_Bu(particles,
                              descriptors,
                              name="onia_withNeutrals_Bu",
                              am_min=4500 * MeV,
                              am_max=6000 * MeV,
                              m_min=4550 * MeV,
                              m_max=5950 * MeV,
                              bpvltime_min=0.3 * picosecond):
    """
    Return B&Q B+ with final state involving neutrals
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min)


####################################
# Bc+                              #
####################################


@configurable
def make_onia_Bc(particles,
                 descriptors,
                 name="onia_Bc",
                 am_min=6050 * MeV,
                 am_max=6555 * MeV,
                 m_min=6090 * MeV,
                 m_max=6510 * MeV,
                 bpvltime_min=0.1 * picosecond):
    """
    Return B&Q Bc+.
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min)


@configurable
def make_onia_withNeutrals_Bc(particles,
                              descriptors,
                              name="onia_withNeutrals_Bc",
                              am_min=4500 * MeV,
                              am_max=6750 * MeV,
                              m_min=4550 * MeV,
                              m_max=6700 * MeV,
                              bpvltime_min=0.3 * picosecond):
    """
    Return B&Q Bc with final state involving neutrals
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min)


####################################
# Lb                               #
####################################


@configurable
def make_onia_Lb(particles,
                 descriptors,
                 name="onia_Lb",
                 am_min=5350 * MeV,
                 am_max=5850 * MeV,
                 m_min=5390 * MeV,
                 m_max=5810 * MeV):
    """
    Return B&Q Lb.
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


@configurable
def make_onia_withNeutrals_Lb(particles,
                              descriptors,
                              name="onia_withNeutrals_Bds",
                              am_min=5000 * MeV,
                              am_max=6500 * MeV,
                              m_min=5050 * MeV,
                              m_max=6450 * MeV,
                              bpvltime_min=0.3 * picosecond):
    """
    Return B&Q Lb with final state involving neutrals
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min)


####################################
# Xib,Sigmab,Omegab                #
####################################


@configurable
def make_onia_b_baryons(particles,
                        descriptors,
                        name="onia_b_baryons",
                        am_min=5350 * MeV,
                        am_max=6460 * MeV,
                        m_min=5390 * MeV,
                        m_max=6410 * MeV):
    """
    Return B&Q Xib, Sigmab and Omegab.
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max)


####################################
# Xibc & di-baryons                #
####################################


@configurable
def make_onia_Xibc(particles,
                   descriptors,
                   name="onia_Xibc",
                   am_min=6250 * MeV,
                   am_max=8000 * MeV,
                   m_min=6290 * MeV,
                   m_max=7950 * MeV,
                   bpvltime_min=0.1 * picosecond):
    """
    Return B&Q Xibc.
    """
    return make_onia_b_hadron(
        particles,
        descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min)
