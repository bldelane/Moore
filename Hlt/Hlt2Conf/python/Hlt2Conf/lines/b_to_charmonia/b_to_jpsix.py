###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B -> J/psi X HLT2 lines.
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder

from PyConf import configurable
from Hlt2Conf.standard_particles import make_mass_constrained_jpsi2mumu
from Hlt2Conf.lines.b_to_charmonia.prefilters import b2cc_prefilters
from Hlt2Conf.lines.b_to_charmonia.builders import b_builder
from Hlt2Conf.lines.b_to_charmonia.builders import basic_builder

all_lines = {}


@register_line_builder(all_lines)
@configurable
def BsToJpsiPhi_detachedline(name='Hlt2B2CC_BsToJpsiPhi_DetachedLine',
                             prescale=1):
    phi = basic_builder.make_selected_phi()
    jpsi = make_mass_constrained_jpsi2mumu()
    b2jpsiphi = b_builder.make_Bs2JpsiPhi_detached(
        particles=[jpsi, phi], descriptors=['B_s0 -> J/psi(1S) phi(1020)'])

    return HltLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + [b2jpsiphi])


@register_line_builder(all_lines)
@configurable
def BsToJpsiPhi_line(name='Hlt2B2CC_BsToJpsiPhi_Line', prescale=0.1):
    phi = basic_builder.make_selected_phi()
    jpsi = make_mass_constrained_jpsi2mumu()
    b2jpsiphi = b_builder.make_Bs2JpsiPhi(
        particles=[jpsi, phi], descriptors=['B_s0 -> J/psi(1S) phi(1020)'])

    return HltLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + [b2jpsiphi])


@register_line_builder(all_lines)
@configurable
def BsToJpsieePhi_detached_line(
        name='Hlt2B2CC_BsToJpsiPhi_JpsiToEE_DetachedLine', prescale=1):
    phi = basic_builder.make_selected_phi_bs2jpsieephi()
    jpsi = basic_builder.make_selected_jpsi2ee_detached()
    b2jpsiphi = b_builder.make_Bs2JpsieePhi_detached(
        particles=[jpsi, phi], descriptors=['B_s0 -> J/psi(1S) phi(1020)'])

    return HltLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + [b2jpsiphi])


@register_line_builder(all_lines)
@configurable
def BsToJpsieePhi_line(name='Hlt2B2CC_BsToJpsiPhi_JpsiToEE_Line',
                       prescale=0.1):
    phi = basic_builder.make_selected_phi_bs2jpsieephi()
    jpsi = basic_builder.make_selected_jpsi2ee()
    b2jpsiphi = b_builder.make_Bs2JpsieePhi(
        particles=[jpsi, phi], descriptors=['B_s0 -> J/psi(1S) phi(1020)'])

    return HltLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + [b2jpsiphi])


@register_line_builder(all_lines)
@configurable
def BdToJpsieeKstar_detached_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToEE_DetachedLine', prescale=1):
    jpsi = basic_builder.make_selected_jpsi_bd2jpsieekstar_detached()
    kstar = basic_builder.make_selected_kstar_bd2jpsieekstar_detached()
    b2jpsikstar = b_builder.make_Bd2JpsieeKstar_detached(
        particles=[jpsi, kstar], descriptors=['[B0 -> J/psi(1S) K*(892)0]cc'])

    return HltLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + [b2jpsikstar])


@register_line_builder(all_lines)
@configurable
def BdToJpsieeKstar_line(name='Hlt2B2CC_BdToJpsiKstar_JpsiToEE_Line',
                         prescale=0.05):
    kstar = basic_builder.make_selected_kstar2kpi()
    jpsi = basic_builder.make_selected_jpsi_bd2jpsieekstar()
    b2jpsikstar = b_builder.make_Bd2JpsieeKstar(
        particles=[jpsi, kstar], descriptors=['[B0 -> J/psi(1S) K*(892)0]cc'])

    return HltLine(
        name=name, prescale=prescale, algs=b2cc_prefilters() + [b2jpsikstar])
