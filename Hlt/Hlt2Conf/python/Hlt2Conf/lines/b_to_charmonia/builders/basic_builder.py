###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2CC basic objects: pions, kaons, ...
"""
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV, MeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs
from PyConf import configurable
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_long_electrons_no_brem)

####################################
# Track selections                 #
####################################


@configurable
def make_selected_particles(make_particles=make_has_rich_long_pions,
                            pvs=make_pvs,
                            tr_chi2pdof_max=0,
                            mipchi2_min=0,
                            pt_min=250 * MeV,
                            p_min=2 * GeV,
                            pid=None):

    code = require_all('PT > {pt_min}', 'P > {p_min}',
                       'TRCHI2DOF < {tr_chi2pdof_max}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           pt_min=pt_min,
                           p_min=p_min,
                           tr_chi2pdof_max=tr_chi2pdof_max,
                           mipchi2_min=mipchi2_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    return ParticleFilterWithPVs(make_particles(), pvs(), Code=code)


@configurable
def make_pions(pid=5, tr_chi2pdof_max=0, pt=200 * MeV, p=2 * GeV):
    """Return pions filtered by thresholds common to B2CC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_pions,
        tr_chi2pdof_max=tr_chi2pdof_max,
        pt_min=pt,
        p_min=p,
        pid='PIDK < ' + str(pid))


@configurable
def make_kaons(pid=-3, tr_chi2pdof_max=5, pt=200 * MeV, p=2 * GeV):
    """Return kaons filtered by thresholds common to B2CC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_kaons,
        tr_chi2pdof_max=tr_chi2pdof_max,
        pt_min=pt,
        p_min=p,
        pid='PIDK > ' + str(pid))


@configurable
def make_electrons(pid=-3, tr_chi2pdof_max=3, pt=250 * MeV, mipchi2_min=0):
    """Return electrons filtered by thresholds common to B2CC decay product Jpsi->ee selections."""
    return make_selected_particles(
        make_particles=make_long_electrons_no_brem,
        tr_chi2pdof_max=tr_chi2pdof_max,
        pt_min=pt,
        mipchi2_min=mipchi2_min,
        pid='PIDe > ' + str(pid))


####################################
# 2-body decays                    #
####################################


@configurable
def make_selected_phi(name="B2CC_Phi_Filter",
                      make_pvs=make_pvs,
                      am_min=980 * MeV,
                      am_max=1060 * MeV,
                      pt=500. * MeV,
                      adoca_chi2=30,
                      vchi2=10):
    """
    Filter phi candiates for B2CC.  Default cuts refered to Bs2JpsiPhi.
    """
    kaons = make_kaons(pid=0)
    descriptors = ['phi(1020) -> K+ K-']
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       adoca_chi2=adoca_chi2)

    vertex_code = require_all("(VFASPF(VCHI2) < {vchi2})", "PT > {pt}").format(
        vchi2=vchi2, pt=pt)

    return ParticleCombinerWithPVs(
        particles=[kaons],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_selected_phi_bs2jpsieephi(make_pvs=make_pvs,
                                   am_min=990 * MeV,
                                   am_max=1050 * MeV,
                                   am_min_vtx=990 * MeV,
                                   am_max_vtx=1050 * MeV,
                                   vchi2pdof=4,
                                   pt=1000. * MeV,
                                   adoca_chi2=30,
                                   tr_chi2pdof=1.5,
                                   pid_k=1,
                                   pt_k=400. * MeV,
                                   p_k=4. * GeV):
    """
    Filter phi candiates for B2CC.  Default cuts refered to Bs2JpsieePhi.
    """
    kaons = make_kaons(pid=pid_k, tr_chi2pdof_max=tr_chi2pdof, pt=pt_k, p=p_k)

    descriptors = ['phi(1020) -> K- K+', '[phi(1020) -> K+ K+]cc']

    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       adoca_chi2=adoca_chi2)
    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vchi2pdof}",
                              "PT > {pt}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vchi2pdof=vchi2pdof,
                                  pt=pt)

    return ParticleCombinerWithPVs(
        particles=[kaons],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_selected_jpsi2ee(make_pvs=make_pvs,
                          am_min=2200 * MeV,
                          am_max=3400 * MeV,
                          am_min_vtx=2200 * MeV,
                          am_max_vtx=3400 * MeV,
                          vchi2pdof=4,
                          pt=1000. * MeV,
                          adoca_chi2=30,
                          tr_chi2pdof=2.,
                          pt_e=500 * MeV,
                          pid_e=0.5,
                          mipchi2_e=0):
    """
    Filter jpsi(ee) candiates for B2CC.  Default cuts refered to Bs2JpsieePhi.
    """
    electrons = make_electrons(
        pid=pid_e, tr_chi2pdof_max=tr_chi2pdof, pt=pt_e, mipchi2_min=mipchi2_e)

    descriptors = ['J/psi(1S) -> e- e+', '[J/psi(1S) -> e+ e+]cc']

    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       adoca_chi2=adoca_chi2)
    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vchi2pdof}",
                              "PT > {pt}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vchi2pdof=vchi2pdof,
                                  pt=pt)

    return ParticleCombinerWithPVs(
        particles=[electrons],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_selected_jpsi2ee_detached(am_min=2200 * MeV,
                                   am_max=3400 * MeV,
                                   am_min_vtx=2200 * MeV,
                                   am_max_vtx=3400 * MeV,
                                   vchi2pdof=4,
                                   pt=1000. * MeV,
                                   adoca_chi2=30,
                                   tr_chi2pdof=1.5,
                                   pt_e=500 * MeV,
                                   pid_e=1,
                                   mipchi2_e=0):
    """
    Filter jpsi(ee) candiates for B2CC, Bd2JpsieeKstar Detached.
    """
    return make_selected_jpsi2ee(
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        vchi2pdof=vchi2pdof,
        pt=pt,
        adoca_chi2=adoca_chi2,
        tr_chi2pdof=tr_chi2pdof,
        pt_e=pt_e,
        pid_e=pid_e,
        mipchi2_e=mipchi2_e)


@configurable
def make_selected_jpsi_bd2jpsieekstar(am_min=2250 * MeV,
                                      am_max=3400 * MeV,
                                      am_min_vtx=2250 * MeV,
                                      am_max_vtx=3400 * MeV,
                                      vchi2pdof=4,
                                      pt=1000. * MeV,
                                      adoca_chi2=30,
                                      tr_chi2pdof=1.5,
                                      pt_e=500 * MeV,
                                      pid_e=1,
                                      mipchi2_e=0):
    """
    Filter jpsi(ee) candiates for B2CC, Bd2JpsieeKstar.
    """
    return make_selected_jpsi2ee(
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        vchi2pdof=vchi2pdof,
        pt=pt,
        adoca_chi2=adoca_chi2,
        tr_chi2pdof=tr_chi2pdof,
        pt_e=pt_e,
        pid_e=pid_e,
        mipchi2_e=mipchi2_e)


@configurable
def make_selected_jpsi_bd2jpsieekstar_detached(am_min=2400 * MeV,
                                               am_max=3400 * MeV,
                                               am_min_vtx=2400 * MeV,
                                               am_max_vtx=3400 * MeV,
                                               vchi2pdof=4,
                                               pt=1000. * MeV,
                                               adoca_chi2=30,
                                               tr_chi2pdof=1.5,
                                               pt_e=500 * MeV,
                                               pid_e=1,
                                               mipchi2_e=0):
    """
    Filter jpsi(ee) candiates for B2CC, Bd2JpsieeKstar Detached.
    """
    return make_selected_jpsi2ee(
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        vchi2pdof=vchi2pdof,
        pt=pt,
        adoca_chi2=adoca_chi2,
        tr_chi2pdof=tr_chi2pdof,
        pt_e=pt_e,
        pid_e=pid_e,
        mipchi2_e=mipchi2_e)


@configurable
def make_selected_kstar2kpi(make_pvs=make_pvs,
                            am_min=792 * MeV,
                            am_max=992 * MeV,
                            am_min_vtx=792 * MeV,
                            am_max_vtx=992 * MeV,
                            vchi2pdof=4,
                            pt=1000. * MeV,
                            adoca_chi2=30,
                            pid_k=1,
                            tr_chi2pdof_k=1.5,
                            pt_k=400. * MeV,
                            p_k=4. * GeV,
                            pid_pi=3,
                            tr_chi2pdof_pi=1.5,
                            pt_pi=300. * MeV,
                            p_pi=3. * GeV):
    """
    Filter kstar->kpi candiates for B2CC.  Default cuts refered to Bs2JpsieeKstar.
    """
    kaons = make_kaons(
        pid=pid_k, tr_chi2pdof_max=tr_chi2pdof_k, pt=pt_k, p=p_k)
    pions = make_pions(
        pid=pid_pi, tr_chi2pdof_max=tr_chi2pdof_pi, pt=pt_pi, p=p_pi)

    descriptors = ['[K*(892)0 -> pi- K+]cc', '[K*(892)0 -> pi+ K+]cc']

    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       adoca_chi2=adoca_chi2)
    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vchi2pdof}",
                              "PT > {pt}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vchi2pdof=vchi2pdof,
                                  pt=pt)

    return ParticleCombinerWithPVs(
        particles=[pions, kaons],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_selected_kstar_bd2jpsieekstar_detached(am_min=842 * MeV,
                                                am_max=942 * MeV,
                                                am_min_vtx=842 * MeV,
                                                am_max_vtx=942 * MeV,
                                                vchi2pdof=4,
                                                pt=1000. * MeV,
                                                adoca_chi2=30,
                                                pid_pi=3,
                                                tr_chi2pdof_pi=1.5,
                                                pt_pi=300. * MeV,
                                                p_pi=3. * GeV,
                                                pid_k=1,
                                                tr_chi2pdof_k=1.5,
                                                pt_k=400. * MeV,
                                                p_k=4. * GeV):
    """
    Filter kstar->kpi candiates for B2CC.  Default cuts refered to Bd2JpsieeKstar detached.
    """
    return make_selected_kstar2kpi(
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        vchi2pdof=vchi2pdof,
        pt=pt,
        adoca_chi2=adoca_chi2,
        pid_pi=pid_pi,
        tr_chi2pdof_pi=tr_chi2pdof_pi,
        pt_pi=pt_pi,
        p_pi=p_pi,
        pid_k=pid_k,
        tr_chi2pdof_k=tr_chi2pdof_k,
        pt_k=pt_k,
        p_k=p_k)
