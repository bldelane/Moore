###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Bu/Bd/Bs/... meson decays that are shared between many
B2CC selections, and therefore are defined centrally.
"""
from GaudiKernel.SystemOfUnits import MeV, picosecond

from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs


@configurable
def make_B2JpsiX(particles,
                 descriptors,
                 name="B2CC_BToJpsiX_Combiner",
                 am_min=3600 * MeV,
                 am_max=6000 * MeV,
                 am_min_vtx=3600 * MeV,
                 am_max_vtx=6000 * MeV,
                 lifetime=-5.0 * picosecond,
                 dira=-10.0,
                 vtx_chi2pdof=10):
    """
    A generic B->Jpsi X decay maker.
    """
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)
    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "BPVLTIME() > {lifetime}", "BPVDIRA() > {dira}",
                              "VFASPF(VCHI2PDOF) < {vtx_chi2pdof}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  lifetime=lifetime,
                                  dira=dira,
                                  vtx_chi2pdof=vtx_chi2pdof)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_Bs2JpsiPhi(particles,
                    descriptors,
                    name="B2CC_BToJpsiX_Combiner",
                    am_min=5050 * MeV,
                    am_max=5650 * MeV,
                    am_min_vtx=5050 * MeV,
                    am_max_vtx=5650 * MeV,
                    vtx_chi2pdof=20):
    """
    A generic Bs->Jpsi Phi decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        vtx_chi2pdof=vtx_chi2pdof)


@configurable
def make_Bs2JpsiPhi_detached(particles,
                             descriptors,
                             name="B2CC_BToJpsiX_Combiner",
                             am_min=5050 * MeV,
                             am_max=5650 * MeV,
                             am_min_vtx=5050 * MeV,
                             am_max_vtx=5650 * MeV,
                             lifetime=0.3 * picosecond,
                             vtx_chi2pdof=20):
    """
    A generic Bs->Jpsi Phi detached decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        lifetime=lifetime,
        vtx_chi2pdof=vtx_chi2pdof)


@configurable
def make_Bs2JpsieePhi_detached(particles,
                               descriptors,
                               name="B2CC_BsToJpsieePhi_Detached_Combiner",
                               am_min=4300 * MeV,
                               am_max=5800 * MeV,
                               am_min_vtx=4300 * MeV,
                               am_max_vtx=5800 * MeV,
                               lifetime=0.3 * picosecond,
                               vtx_chi2pdof=10):
    """
    A generic Bs->Jpsi(ee) Phi decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        lifetime=lifetime,
        vtx_chi2pdof=vtx_chi2pdof)


@configurable
def make_Bs2JpsieePhi(particles,
                      descriptors,
                      name="B2CC_BsToJpsieePhi_Combiner",
                      am_min=4000 * MeV,
                      am_max=5900 * MeV,
                      am_min_vtx=4000 * MeV,
                      am_max_vtx=5900 * MeV,
                      dira=0.9995,
                      vtx_chi2pdof=10):
    """
    A generic Bs->Jpsi(ee) Phi decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        dira=dira,
        vtx_chi2pdof=vtx_chi2pdof)


@configurable
def make_Bd2JpsieeKstar_detached(particles,
                                 descriptors,
                                 name="B2CC_BdToJpsieeKstar_Detached_Combiner",
                                 am_min=4300 * MeV,
                                 am_max=5800 * MeV,
                                 am_min_vtx=4300 * MeV,
                                 am_max_vtx=5800 * MeV,
                                 lifetime=0.3 * picosecond,
                                 vtx_chi2pdof=10):
    """
    A generic B0->Jpsi(ee) Kstar decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        lifetime=lifetime,
        vtx_chi2pdof=vtx_chi2pdof)


@configurable
def make_Bd2JpsieeKstar(particles,
                        descriptors,
                        name="B2CC_BdToJpsieeKstar_Combiner",
                        am_min=4000 * MeV,
                        am_max=5900 * MeV,
                        am_min_vtx=4000 * MeV,
                        am_max_vtx=5900 * MeV,
                        dira=0.9995,
                        vtx_chi2pdof=10):
    """
    A generic B0->Jpsi(ee) Kstar decay maker.
    """
    return make_B2JpsiX(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        dira=dira,
        vtx_chi2pdof=vtx_chi2pdof)
