###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the QEE Hlt2 lines
"""

from . import SingleHighPtMuon
from . import DiBJet

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(SingleHighPtMuon.all_lines)
all_lines.update(DiBJet.all_lines)
# all_lines.update(<module.all_lines>) # Add other lines here
