###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Some generic lines

mainly to test consistency combiners and filters and estimate timing
"""

from __future__ import absolute_import, division, print_function
import math

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction, make_tracks

from ..algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs, ParticleFilter, N3BodyCombinerWithPVs
from PyConf.Algorithms import (
    ChargedBasicsFilter, LHCb__Converters__RangeToVectorParticle as
    RangeToVector, LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex as
    Vertex_v2_to_v1,
    LHCb__Converters__RecVertices__LHCbRecVerticesToVectorV2RecVertex as
    Vertex_v1_to_v2, LHCb__Converters__Track__v2__fromV1TrackV2Track as
    v2TrackConverter, LHCb__VectorToSelectionParticle as VectorToSelection,
    ParticleSelectionFilter, ThreeBodyCombiner, ThOrCombiner__3ChargedBasics,
    ThOrCombiner__3Particle, ThOrCombinerSSE__3ChargedBasics,
    ThOrCombinerScalar__3ChargedBasics, MakeZip__ChargedBasics_as_Particles)

from PyConf import configurable
from ..standard_particles import (
    make_has_rich_long_cb_pions, make_has_rich_long_cb_kaons,
    make_has_rich_long_pions, make_has_rich_long_kaons)

from Functors.math import in_range
from Functors import (ALL, SUM, DOCACHI2, P, PT, MINIPCHI2, FILTER, ISMUON,
                      PID_K, COMPOSITEMASS, CHI2DOF, MAX, BPVFDCHI2, BPVDIRA)

combiner_lines = {}
filter_lines = {}


def make_v2_pvs():
    v2_tracks = v2TrackConverter(
        InputTracksName=make_tracks()).OutputTracksName
    v2pvs = Vertex_v1_to_v2(
        InputVertices=make_pvs(), InputTracks=v2_tracks).OutputVertices
    return v2pvs


def make_pvs_from_v2_pvs():
    v1pvs = Vertex_v2_to_v1(
        InputVerticesName=make_v2_pvs(), InputTracksName=make_tracks())
    return v1pvs


@configurable
def filter_particles(particles,
                     pvs,
                     trchi2_max=3,
                     mipchi2_min=4,
                     pt_min=250 * MeV,
                     p_min=2 * GeV,
                     pid=None,
                     name=None,
                     filterAlg=None,
                     backend='LoKi'):
    """Return maker for particles filtered by thresholds common to charm decay product selections.

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """
    assert backend in {'LoKi', 'ThOr'}

    def loki_thor(loki, thor):
        return loki.format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min) if backend == 'LoKi' else thor

    cuts = [
        loki_thor('PT > {pt_min}', PT > pt_min),
        loki_thor('P > {p_min}', P > p_min),
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        loki_thor('MIPCHI2DV(PRIMARY) > {mipchi2_min}',
                  MINIPCHI2(Vertices=pvs) > mipchi2_min),
    ]
    if pid is not None: cuts.append(pid)
    if backend == 'ThOr':
        code = cuts[0]
        for other_cut in cuts[1:]:
            code = code & other_cut
        code = FILTER(code)
        if filterAlg is None: filterAlg = ChargedBasicsFilter
        return filterAlg(name=name, Input=particles, Cut=code).Output
    else:
        code = require_all(*cuts)
        return ParticleFilterWithPVs(
            particles,
            pvs,
            name=name,
            Code=code,
            ParticleCombiners={"": "ParticleVertexFitter"},
            DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
        )


@configurable
def select_pions(particles, pid=(PID_K < 5)):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return filter_particles(
        particles, make_v2_pvs(), pid=pid, name="ThOrPionsCB", backend='ThOr')


@configurable
def select_kaons(particles, pid=(PID_K > 5)):
    """Return maker for kaons filtered by thresholds common to charm decay product selections."""
    return filter_particles(
        particles, make_v2_pvs(), pid=pid, name="ThOrKaonsCB", backend='ThOr')


@configurable
def select_LHCb_pions(particles, pid=(PID_K < 5)):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    v = RangeToVector(Input=particles).Output
    s = VectorToSelection(Input=v).Output
    return filter_particles(
        s,
        make_v2_pvs(),
        pid=pid,
        filterAlg=ParticleSelectionFilter,
        name="ThOrPions",
        backend='ThOr')


@configurable
def select_ISMUON_pions(particles):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return ChargedBasicsFilter(
        Input=particles, Cut=FILTER(ISMUON), name="ThOrISMUONPionsCB")


@configurable
def select_LHCb_ISMUON_pions(particles):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    v = RangeToVector(Input=particles).Output
    s = VectorToSelection(Input=v).Output
    return ParticleSelectionFilter(
        Input=s, Cut=FILTER(ISMUON), name="ThOrISMUONPions")


@configurable
def select_legacy_ismuon_pions(particles):
    return ParticleFilter(
        particles,
        name="LoKiISMUONPions",
        Code="ISMUON",
        ParticleCombiners={"": "ParticleVertexFitter"},
        DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
    )


@configurable
def combine_dplus_hhh(particles,
                      pvs,
                      decay_descriptor='[D+ -> K- pi+ pi+]cc',
                      combiner='ThOrCombiner',
                      backend='Best',
                      make_pvs=make_v2_pvs,
                      am_min=1789 * MeV,
                      am_max=1949 * MeV,
                      docachi2_max=16,
                      amaxchild_pt_min=800 * MeV,
                      trk_2of3_pt_min=400 * MeV,
                      aptsum_min=1500 * MeV,
                      amaxchild_ipchi2_min=9,
                      trk_2of3_ipchi2_min=4,
                      vchi2pdof_max=6,
                      bpvvdchi2_min=16,
                      bpvltime_min=0.4 * picosecond,
                      acos_bpvdira_max=10. * mrad):
    '''Combining using various combiner implementations but notionally the same cuts.'''
    functor_backend = combiner[:4]
    assert functor_backend in {'LoKi', 'ThOr'}

    cos_bpvdira_min = math.cos(acos_bpvdira_max)

    def loki_thor(loki, thor):
        if functor_backend == 'LoKi':
            return loki.format(
                am_min=am_min,
                am_max=am_max,
                amaxchild_pt_min=amaxchild_pt_min,
                docachi2_max=docachi2_max,
                trk_2of3_pt_min=trk_2of3_pt_min,
                aptsum_min=aptsum_min,
                amaxchild_ipchi2_min=amaxchild_ipchi2_min,
                trk_2of3_ipchi2_min=trk_2of3_ipchi2_min,
                vchi2pdof_max=vchi2pdof_max,
                bpvvdchi2_min=bpvvdchi2_min,
                bpvltime_min=bpvltime_min,
                cos_bpvdira_min=cos_bpvdira_min)
        else:
            return thor

    def req_all(cuts):
        if functor_backend == 'LoKi':
            return require_all(*cuts)
        else:
            code = cuts[0]
            for other_code in cuts[1:]:
                code = code & other_code
            return code

    combination12_code = req_all([
        loki_thor('ACHI2DOCA(1, 2) < {docachi2_max}',
                  DOCACHI2(1, 2) < docachi2_max),
        loki_thor("AMAXCHILD(PT) > {trk_2of3_pt_min}",
                  MAX(PT) > trk_2of3_pt_min),
    ])
    combination_code = req_all([
        loki_thor("in_range({am_min},  AM, {am_max})",
                  in_range(am_min, COMPOSITEMASS, am_max)),
        loki_thor("AMAXCHILD(PT) > {amaxchild_pt_min}",
                  MAX(PT) > amaxchild_pt_min),
        loki_thor("ANUM( PT > {trk_2of3_pt_min} ) > 1",
                  SUM(PT > trk_2of3_pt_min) > 1),
        loki_thor("(APT1 + APT2 + APT3) > {aptsum_min}",
                  SUM(PT) > aptsum_min),
        loki_thor("ACHI2DOCA(1, 3) < {docachi2_max}",
                  DOCACHI2(1, 3) < docachi2_max),
        loki_thor("ACHI2DOCA(2, 3) < {docachi2_max}",
                  DOCACHI2(2, 3) < docachi2_max),
        loki_thor("ANUM(MIPCHI2DV(PRIMARY) > {trk_2of3_ipchi2_min}) > 1",
                  SUM(MINIPCHI2(Vertices=pvs) > trk_2of3_ipchi2_min) > 1),
        loki_thor("AMAXCHILD(MIPCHI2DV(PRIMARY)) > {amaxchild_ipchi2_min}",
                  MAX(MINIPCHI2(Vertices=pvs)) > amaxchild_ipchi2_min),
    ])
    vertex_code = req_all([
        loki_thor("CHI2VXNDOF < {vchi2pdof_max}", CHI2DOF < vchi2pdof_max),
        # This next one doesn't really have a ThOr equivalent
        loki_thor("BPVVALID()", ALL),
        loki_thor("BPVVDCHI2() > {bpvvdchi2_min}",
                  BPVFDCHI2(Vertices=pvs) > bpvvdchi2_min),
        loki_thor("BPVDIRA() > {cos_bpvdira_min}",
                  BPVDIRA(Vertices=pvs) > cos_bpvdira_min),
        # This doesn't have a ThOr impl: "BPVLTIME() > {bpvltime_min}",
    ])
    if combiner == 'ThOrCombiner':
        assert len(particles) == 3
        alg = {
            'Best': ThOrCombiner__3ChargedBasics,
            'SSE': ThOrCombinerSSE__3ChargedBasics,
            'Scalar': ThOrCombinerScalar__3ChargedBasics,
            'Particle': ThOrCombiner__3Particle,
        }[backend]
        return alg(
            Input0=particles[0],
            Input1=particles[1],
            Input2=particles[2],
            DecayDescriptor=decay_descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            VertexCut=vertex_code).Output
    elif combiner == 'ThOrLHCbParticle':
        return ThreeBodyCombiner(
            Inputs=particles,
            DecayDescriptors=[decay_descriptor],
            Comb12Cut=combination12_code,
            Comb123Cut=combination_code,
            ParticleCombiner="ParticleVertexFitter",
            DistanceCalculator="LoKi::TrgDistanceCalculator",
            MotherCut=vertex_code).OutputParticles
    elif combiner in {'LoKiCombineParticles', 'LoKiNBody'}:
        alg, kwargs = {
            'LoKiCombineParticles': (ParticleCombinerWithPVs, {
                'CombinationCut':
                req_all([combination12_code, combination_code]),
            }),
            'LoKiNBody': (N3BodyCombinerWithPVs, {
                'CombinationCut': combination_code,
                'Combination12Cut': combination12_code
            })
        }[combiner]
        return alg(
            particles=particles,
            pvs=pvs,
            DecayDescriptors=[decay_descriptor],
            MotherCut=vertex_code,
            ParticleCombiners={"": "ParticleVertexFitter"},
            DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
            **kwargs)


@configurable
def make_pions(particles, pvs, pid='PIDK < 5'):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return filter_particles(particles, pvs, pid=pid)


@configurable
def make_kaons(particles, pvs, pid='PIDK > 5'):
    """Return maker for kaons filtered by thresholds common to charm decay product selections."""
    return filter_particles(particles, pvs, pid=pid)


@register_line_builder(filter_lines)
def HLT_thor_cb_muon_filter_line(name="Hlt2_ThOrCBFilterISMUONLine"):
    cbpions = select_ISMUON_pions(make_has_rich_long_cb_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), cbpions])


@register_line_builder(filter_lines)
def HLT_loki_muon_filter_line(name="Hlt2_LoKiFilterISMUONLine"):
    lokipions = select_legacy_ismuon_pions(make_has_rich_long_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), lokipions])


@register_line_builder(filter_lines)
def HLT_thor_muon_filter_line(name="Hlt2_ThOrFilterISMUONLine"):
    thorpions = select_LHCb_ISMUON_pions(make_has_rich_long_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), thorpions])


# MORE COMPLICATED FILTERS
@register_line_builder(filter_lines)
def HLT_thor_cb_complex_filter_line(name="Hlt2_ThOrCBFilterCOMPLICATEDLine"):
    cbpions = select_pions(make_has_rich_long_cb_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), cbpions])


@register_line_builder(filter_lines)
def HLT_loki_complex_filter_line(name="Hlt2_LoKiFilterCOMPLICATEDLine"):
    lokipions = make_pions(make_has_rich_long_pions(), make_pvs())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), lokipions])


@register_line_builder(filter_lines)
def HLT_thor_complex_filter_line(name="Hlt2_ThOrFilterCOMPLICATEDLine"):
    thorpions = select_LHCb_pions(make_has_rich_long_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), thorpions])


### ThOr combiner lines
@register_line_builder(combiner_lines)
def HLT_ThOr_dplus_hhh_line(name="Hlt2_ThOr_Dp2KmPipPipLine"):
    kaons = select_kaons(make_has_rich_long_cb_kaons())
    pions = select_pions(make_has_rich_long_cb_pions())
    dplus = combine_dplus_hhh(
        pvs=make_v2_pvs(),
        particles=[kaons, pions, pions],
        combiner='ThOrCombiner')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), dplus])


@register_line_builder(combiner_lines)
def HLT_ThOrSSE_dplus_hhh_line(name="Hlt2_ThOrSSE_Dp2KmPipPipLine"):
    kaons = select_kaons(make_has_rich_long_cb_kaons())
    pions = select_pions(make_has_rich_long_cb_pions())
    dplus = combine_dplus_hhh(
        pvs=make_v2_pvs(),
        particles=[kaons, pions, pions],
        combiner='ThOrCombiner',
        backend='SSE')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), dplus])


@register_line_builder(combiner_lines)
def HLT_ThOrScalar_dplus_hhh_line(name="Hlt2_ThOrScalar_Dp2KmPipPipLine"):
    kaons = select_kaons(make_has_rich_long_cb_kaons())
    pions = select_pions(make_has_rich_long_cb_pions())
    dplus = combine_dplus_hhh(
        pvs=make_v2_pvs(),
        particles=[kaons, pions, pions],
        combiner='ThOrCombiner',
        backend='Scalar')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), dplus])


@register_line_builder(combiner_lines)
def HLT_ThOrVariant_dplus_hhh_line(name="Hlt2_ThOrVariant_Dp2KmPipPipLine"):
    kaons = select_kaons(make_has_rich_long_cb_kaons())
    pions = select_pions(make_has_rich_long_cb_pions())
    pions_as_particles = MakeZip__ChargedBasics_as_Particles(
        Input1=pions).Output
    kaons_as_particles = MakeZip__ChargedBasics_as_Particles(
        Input1=kaons).Output
    dplus = combine_dplus_hhh(
        pvs=make_v2_pvs(),
        particles=[kaons_as_particles, pions_as_particles, pions_as_particles],
        combiner='ThOrCombiner',
        backend='Particle')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), dplus])


### Shiny combiner lines
@register_line_builder(combiner_lines)
def HLT_Shiny_dplus_hhh_line(name="Hlt2_Shiny_Dp2KmPipPipLine"):
    kaons = make_kaons(make_has_rich_long_kaons(), make_pvs())
    pions = make_pions(make_has_rich_long_pions(), make_pvs())
    dplus = combine_dplus_hhh(
        pvs=make_v2_pvs(),
        particles=[kaons, pions],
        combiner='ThOrLHCbParticle')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), dplus])


# lhcb particle combiner lines
@register_line_builder(combiner_lines)
def dplus2Kpipi_line(name='Hlt2CharmHadDToKmPipPipLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dplus = combine_dplus_hhh(
        particles=[kaons, pions], pvs=pvs, combiner='LoKiCombineParticles')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dplus],
        prescale=prescale,
    )


@register_line_builder(combiner_lines)
@configurable
def dplus2Kpipi_line_n3(name='Hlt2CharmHadDToKmPipPipN3Line', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dplus = combine_dplus_hhh(
        particles=[kaons, pions], pvs=pvs, combiner='LoKiNBody')
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dplus],
        prescale=prescale,
    )
