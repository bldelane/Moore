from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Configurables import HltGenConfig
from Configurables import GaudiSequencer
from Configurables import DeterministicPrescaler
from Configurables import ConfigCDBAccessSvc
from Configurables import ApplicationMgr
from Configurables import LHCbApp
from Configurables import CondDB
from Moore.config import setup_ann_service, get_allen_hlt1_decision_ids

app = LHCbApp()
app.EvtMax = -1
app.DataType = 'Upgrade'
app.Simulation = True

CondDB().Upgrade = True

# Location of TCK database
TCKData = 'TCKData'
filename = os.path.join(TCKData, 'config.cdb')
if not os.path.exists(TCKData):
    os.makedirs(TCKData)
if os.path.exists(filename):
    os.remove(filename)

# TCK access service
accessSvc = ConfigCDBAccessSvc(File=filename, Mode='ReadWrite')

# Sequence, actually only a prescaler
seq = GaudiSequencer("TestSequence")
prescaler = DeterministicPrescaler(
    "TestScaler", SeedName="TestScaler", AcceptFraction=0.1)
seq.Members = [prescaler]

hlt1_decision_ids = get_allen_hlt1_decision_ids()
setup_ann_service(hlt1_decision_ids, {})

# Algorithm to generate the TCK
gen_tck = HltGenConfig(
    ConfigTop=[seq.getName()],
    ConfigSvc=['ToolSvc', 'HltANNSvc'],
    ConfigAccessSvc=accessSvc.getName(),
    HltType="Allen_Test",
    MooreRelease="Allen_v1r0",
    Label="Test")

# make sure gen is the very first Top algorithm
ApplicationMgr().TopAlg = [gen_tck.getFullName(), seq]

# Instantiate AppMgr and run some events
from GaudiPython.Bindings import AppMgr

gaudi = AppMgr()
TES = gaudi.evtSvc()

gaudi.initialize()
gaudi.start()
gaudi.stop()
gaudi.finalize()
gaudi.exit()

print('PASSED')
