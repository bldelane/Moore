###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Compare decisions from a log file to those stored in a MDF.

Takes three outputs from a previously-ran job: the options dump, the log
file, and the MDF. The decisions printed in the form of counters of the RunAllen algorithm are
compared to those taken from the DecReports found in the MDF. Any difference
between the two is considered a failure. The options dump is used to
configure the HltANNSvc for the job ran by this script.
"""
from __future__ import print_function
import argparse
from collections import defaultdict
from Configurables import (ApplicationMgr, HistogramPersistencySvc,
                           IODataManager, LHCbApp, ConfigCDBAccessSvc)
from DAQSys.Decoders import DecoderDB
from GaudiConf import IOHelper
import GaudiPython


def get_counts_from_log(f):
    """Return the decisions of each line as extracted from a log file."""
    counts = {}
    with open(f) as f:
        for line in (l for l in f if "Selected by" in l):
            columns = line.split()
            hlt_line = columns[2].replace('"', '')
            count = int(columns[6])
            counts[hlt_line] = count
        f.seek(0)
        for line in (l for l in f if "LAZY_AND: allen" in l):
            columns = line.split()
            hlt_line = columns[1]
            count = int(columns[3].replace("Sum=", ""))
            counts[hlt_line] = count
    return counts


parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", help="Input MDF file")
parser.add_argument("--input-log", help="Input log file")
args = parser.parse_args()

# Configure basic application with inputs
LHCbApp(DataType="Upgrade", Simulation=True)
IOHelper("MDF").inputFiles([args.input_mdf])
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)
# Decode Hlt DecReports
ApplicationMgr(
    TopAlg=[DecoderDB["HltDecReportsDecoder/Hlt1DecReportsDecoder"].setup()])

ConfigCDBAccessSvc().File = "TCKData/config.cdb"

# Set up counters for recording decisions from MDF
counts_from_mdf = defaultdict(int)
# Extract counters from log file of the previous job
counts_from_log = get_counts_from_log(args.input_log)

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtSvc()
gaudi.run(1)

error = False
while TES["/Event"]:
    decs = TES["/Event/Hlt1/DecReports"]
    if not decs:
        print("DecReports TES location not found")
        error = True
        break

    triggered = False
    for key in decs.decisionNames():
        report = decs.decReport(key)
        counts_from_mdf[key] += int(report.decision())
        if report.decision():
            triggered = True

    gaudi.run(1)

for key in counts_from_mdf.keys():
    line_name = key
    if line_name not in counts_from_log.keys():
        error = True
        print("Test ERROR: Line {} missing".format(line_name))
    else:
        if counts_from_mdf[key] != counts_from_log[line_name]:
            error = True
            print("Test ERROR: Counts of {} wrong, log = {}, mdf = {}".format(
                key, counts_from_log[line_name], counts_from_mdf[key]))
        else:
            print("Counts of {}, log = {}, mdf = {}".format(
                key, counts_from_log[line_name], counts_from_mdf[key]))

if error:
    exit("Test failed")  # exit with a non-zero code
