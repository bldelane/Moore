###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define routing bits

Routing bits are used to decide where to send events, to Hlt2 only, or as well to calibration and monitoring tasks.
Each bit is set according to trigger line decisions.

"""


def get_default_routing_bits(decision_names):
    """ Define default routing bits via regular expressions. This is currently a placeholder to test the functionality."""
    import re
    routingBits_regex = {
        33: r"Hlt1TrackMVALineDecision",
        35: r"Hlt1TrackMuonMVALineDecision",
        36: r"Hlt1TwoTrackMVALineDecision",
        37: r".*TrackMVA.*LineDecision",
    }
    routingBits = {}
    for bit in routingBits_regex:
        matches = []
        for name in decision_names:
            if re.match(routingBits_regex[bit], name):
                matches.append(name)
        routingBits[bit] = matches

    return routingBits
