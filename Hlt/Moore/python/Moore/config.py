###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import
import re, os, json, logging, inspect
from collections import namedtuple
from Configurables import (ApplicationMgr, DumpUTGeometry, DumpFTGeometry,
                           DumpMuonTable, DumpMuonGeometry, DumpVPGeometry,
                           DumpMagneticField, DumpBeamline, DumpUTLookupTables,
                           AllenUpdater)
from PyConf import configurable
from PyConf.Algorithms import (
    bankKiller, ExecutionReportsWriter, HltDecReportsWriter,
    HltSelReportsWriter, Hlt__RoutingBitsWriter, HltLumiWriter,
    Gaudi__Hive__FetchLeavesFromFile, CopyInputStream)
from PyConf.components import force_location, setup_component
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import (ROOT_KEY, ApplicationOptions, output_writers,
                                make_odin, default_raw_event)

# "forward" some useful functions from PyConf.application
from PyConf.application import configure_input, configure

from .lines import HltLine, SpruceLine, HltLuminosityLine  # noqa: forward import
from .persistence import persist_line_outputs, format_output_location
from .routingbits import get_default_routing_bits
from .selreports import make_selreports
from RecoConf.hlt1_allen import (
    SEQUENCE as ALLEN_SEQUENCE,
    call_allen,
)

log = logging.getLogger(__name__)

# TODO move the options global to a separate module such that functions
#      defined here cannot access it
# FIXME _enabled is a workaround for using ConfigurableUser
#: Global ApplicationOptions instance holding the options for Moore
options = ApplicationOptions(_enabled=False)


class Reconstruction(namedtuple('Reconstruction', ['node'])):  # noqa
    """Immutable object fully qualifiying the output of a reconstruction data flow with prefilters.

    Attributes:
        node (CompositeNode): the control flow of the reconstruction.

    """
    __slots__ = ()  # do not add __dict__ (preserve immutability)

    def __new__(cls, name, data_producers, filters=None):
        """Initialize a Reconstruction from name, data_producers and filters.

        Creates two control flow `CompositeNode` with the given `data`
        combined with `NONLAZY_OR` to execute  and a `CompositeNode`.

        Args:
            name (str): name of the reconstruction
            data_producers (list): iterable list of algorithms to produce data
            filters (list): list of filters to apply before running reconstruction
        """
        data_producers_node = CompositeNode(
            name + "_data",
            data_producers,
            combine_logic=NodeLogic.NONLAZY_OR,
            force_order=True)
        if filters is None:
            filters = []
        control_flow = filters + [data_producers_node]
        cf_node = CompositeNode(
            name + "_decision",
            control_flow,
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True)

        return super(Reconstruction, cls).__new__(cls, cf_node)

    @property
    def name(self):
        """Reconstruction name"""
        return self.node.name


def _build_decision_ids(decision_names):
    """Return a dict of decision names to integer IDs.

    Decision report IDs must not be zero. This method generates IDs starting
    from 1. It does not ensure that a decision ID was not used in a previous
    call, as IDs can be duplicated between different keys of the ANN service.

    Args:
        decision_names (list of str)

    Returns:
        decision_ids (dict of str to int): Mapping from decision name to ID.
    """
    return {name: idx for idx, name in enumerate(decision_names, 1)}


def setup_ann_service(hlt1_decision_ids, hlt2_decision_ids):
    """Configure the ANN service.

    The ANN service is responsible for mapping strings to numbers and
    vice-versa. Such functionality is used when serialising and deserialising
    raw banks, for example when reading and writing decision report banks.

    Args:
        hlt1_decision_ids (dict of str to int): Mapping from HLT line
            decision name to ID. Can be empty if not running HLT1 or not
            reading HLT1 reports.
        hlt2_decision_ids (dict of str to int): Mapping from HLT line decision
            name to ID. Can be empty if not running HLT2.
    """
    hlt_ann_svc = setup_component(
        "HltANNSvc",
        Hlt1SelectionID=hlt1_decision_ids,
        Hlt2SelectionID=hlt2_decision_ids,
    )

    if hlt_ann_svc.getFullName() not in ApplicationMgr().ExtSvc:
        ApplicationMgr().ExtSvc += [hlt_ann_svc.getFullName()]

    return hlt_ann_svc


@configurable
def report_writers_node(lines,
                        data_type,
                        process,
                        kill_existing,
                        associate_mc=False,
                        get_routing_bits=get_default_routing_bits):
    """Return the control flow node and locations to persist of the default reports writers.

    Args:
        lines (list of DecisionLine): control flow nodes of lines
        data_type (str)
        process (str): "hlt1", "hlt2" or "spruce"
        kill_existing (bool): If True, remove any existing HLT banks before
            creating new ones.
        associate_mc (bool): If True, run MC association algorithms and
            persist the result.
        get_routing_bits (method): Maker returning dict of int to str, which
            defines regular expressions of line names that result in an event
            being assigned a given routing bit.

    Returns:
        node (CompositeNode): Output creation control flow.
        locations_to_persist (list of str): Locations the application should
            write out.
        barrier_algorithms (list of Algorithm): Algorithms which must be
            declared to the scheduler (HLTControlFlowMgr) as barriers.
    """
    # Hack to support writing of objects created in HLT2 jobs
    locations_to_persist = []
    # Hack to support defining barrier algorithms to the scheduler
    barrier_algorithms = []

    algs = []
    # We will write the reports to raw banks at these locations
    report_banks_to_write = ['HltDecReports', 'HltSelReports']
    routing_bits_banks_to_write = ['HltRoutingBits']
    lumi_banks_to_write = ['HltLumiSummary']
    reports_raw_event = default_raw_event(report_banks_to_write)
    routing_bits_raw_event = default_raw_event(routing_bits_banks_to_write)
    lumi_raw_event = default_raw_event(lumi_banks_to_write)
    # TODO we probably shouldn't write in Trigger/RawEvent when
    # running on reconstructed data. Instead, we should create a
    # RawEvent at DAQ/RawEvent, which would make HltDecReportsWriter
    # happy, or make raw events/banks const and adapt everything.
    if process != "spruce":
        if kill_existing:
            all_banks_to_write = (report_banks_to_write + lumi_banks_to_write +
                                  routing_bits_banks_to_write)
            algs.append(
                bankKiller(
                    RawEventLocations=[reports_raw_event, lumi_raw_event],
                    BankTypes=all_banks_to_write))

        erw = ExecutionReportsWriter(
            Persist=[line.name for line in lines],
            ANNSvcKey="Hlt1SelectionID"
            if process == "hlt1" else "Hlt2SelectionID",
        )
        drw = HltDecReportsWriter(
            InputHltDecReportsLocation=erw.DecReportsLocation,
            outputs=dict(
                OutputRawEventLocation=force_location(reports_raw_event.
                                                      location)),
        )
        algs.extend([erw, drw])
        locations_to_persist.append(drw.OutputRawEventLocation)

    lines_with_output = [l for l in lines if l.produces_output]
    lumi_lines = [
        l for l in lines_with_output if isinstance(l, HltLuminosityLine)
    ]
    physics_lines = [l for l in lines_with_output if l not in lumi_lines]

    if process == "hlt1":
        srm = make_selreports(physics_lines, erw)
        algs.append(srm)
        # The SelReports maker must be a barrier as its inputs are conditional
        # on line decisions (if a line does not fire, its outputs will not be
        # available to make SelReports with)
        barrier_algorithms.append(srm)
        srw = HltSelReportsWriter(
            DecReports=erw.DecReportsLocation,
            SelReports=srm.SelReports,
            ObjectSummaries=srm.ObjectSummaries,
            # Algorithm uses this property as a both input and output
            RawEvent=reports_raw_event)
        algs.append(srw)
        locations_to_persist.append(reports_raw_event)
    if process == "hlt2":
        line_output_cf, line_output_locations = persist_line_outputs(
            physics_lines, data_type, erw.DecReportsLocation, associate_mc)
        algs.append(line_output_cf)
        locations_to_persist.extend(line_output_locations)

    elif process == "spruce":
        algs.append(
            bankKiller(
                RawEventLocations=[default_raw_event(['DstData'])],
                BankTypes=['DstData']))

        Spruce_erw = ExecutionReportsWriter(
            Persist=[line.name for line in lines],
            ANNSvcKey="Hlt2SelectionID",
            outputs={
                'DecReportsLocation':
                force_location("/Event/Spruce/DecReports")
            },
            PrintFreq=10,
        )

        line_output_cf, line_output_locations = persist_line_outputs(
            physics_lines,
            data_type,
            Spruce_erw.DecReportsLocation,
            associate_mc=False,
            stream="/Event/Spruce",
            out_stream="/Event/Spruce/HLT2")
        algs.append(Spruce_erw)
        algs.append(line_output_cf)
        locations_to_persist.extend(line_output_locations)
        locations_to_persist.append(Spruce_erw.outputs['DecReportsLocation'])

    # We can handle multiple lines by using a LumiCounterMerger algorithm, but
    # for now simplify the logic by assuming there's a single line
    assert len(lumi_lines) <= 1, 'Found multiple lumi lines'
    if len(lumi_lines) == 1:
        lumi_line, = lumi_lines
        lumi_encoder = HltLumiWriter(
            InputBank=lumi_line.output_producer,
            outputs=dict(
                RawEventLocation=force_location(lumi_raw_event.location)),
        )
        algs.append(lumi_encoder)
        locations_to_persist.append(lumi_encoder.RawEventLocation)

    if process != "spruce":
        rbw = Hlt__RoutingBitsWriter(
            RoutingBits=get_routing_bits(
                [line.decision_name for line in lines]),
            DecReports=erw.DecReportsLocation,
            ODIN=make_odin(),
            outputs=dict(
                RawEventLocation=force_location(routing_bits_raw_event.
                                                location)),
        )
        algs.append(rbw)
        locations_to_persist.append(rbw.RawEventLocation)

    node = CompositeNode(
        'report_writers',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=algs,
        force_order=True)

    # Transform to a list of unique str
    locations_to_persist = list(
        set(map(format_output_location, locations_to_persist)))

    return node, locations_to_persist, barrier_algorithms


def root_writer(path, locations=None):
    """Return algorithms for writing ROOT/DST files.

    The writer will write not only the locations specified in `locations`,
    but also the locations collected by `Gaudi::Hive::FetchLeavesFromFile`.
    By scheduling the latter as the very first producer, one can copy all
    input file locations to the output.

    Args:
        path (str): Path the output file should be written to.
        locations (list of str): TES locations created by the application to write.

    Returns:
        (collector, writer): The `collector` must be explicitly scheduled
            before any other producer algorithms. The `writer` is typically
            scheduled last.
    """
    collector = Gaudi__Hive__FetchLeavesFromFile()
    writer = CopyInputStream(
        InputFileLeavesLocation=collector,
        OptItemList=locations or [],
        Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
            path),
    )
    return collector, writer


def moore_output_writers(path, output_type, locations=None):
    """Return algorithms for writing output.

    These algorithms are not functional and must be explicitly scheduled,
    typically at the very end of the control flow.

    Args:
      path (str): Path the output file should be written to.
      output_type (str): One of `MDF_KEY` or `ROOT_KEY`.
      locations (list of str): TES locations to write when using ROOT writers.
        Ignored when writing MDF files.

    Returns:
      (pre_algs, post_algs): Lists of configured algorithms that should be
        scheduled at the very beginning and the very end of the control flow,
        respectively.
    """
    pre_algs, post_algs = [], []
    if output_type == ROOT_KEY:
        pre, post = root_writer(path, locations)
        pre_algs.append(pre)
        post_algs.append(post)
    else:
        # Defer to PyConf, which does not specify pre-algs, for other types
        post_algs += output_writers(path, output_type, locations)
    return pre_algs, post_algs


def moore_control_flow(options, lines, process, allen_hlt1):
    """Return the Moore application control flow node.

    Combines the lines with `NONLAZY_OR` logic in a global decision
    control flow node. This is `LAZY_AND`-ed with the output control
    flow, which consists of Moore-specific report makers/writers and
    generic persistency.

    Args:
        options (ApplicationOptions): holder of application options
        lines: control flow nodes of lines
        process (str): "hlt1", "hlt2" or "spruce"
        allen_hlt1: configure Allen HLt1 lines in HltANNSvc (when processing Hlt2)

    Returns:
        node (CompositeNode): Application control flow.
        barrier_algorithms (list of Algorithm): Algorithms which must be
            declared to the scheduler (HLTControlFlowMgr) as barriers.
    """
    options.finalize()

    # Sort the lines by name for consistency between runs
    lines = sorted(lines, key=lambda line: line.name)

    ann_config = dict(hlt1_decision_ids={}, hlt2_decision_ids={})
    if allen_hlt1:
        ann_config["hlt1_decision_ids"] = get_allen_hlt1_decision_ids()
    key = ("hlt1" if process == "hlt1" else "hlt2") + "_decision_ids"
    ann_config[key] = _build_decision_ids([l.decision_name for l in lines])
    setup_ann_service(**ann_config)

    # TODO pass kill_existing from options
    rw_node, rw_outputs, barriers = report_writers_node(
        lines,
        options.data_type,
        process,
        kill_existing=True,
        # Can only run association when we have access to the linker tables
        associate_mc=options.simulation and options.input_type == "ROOT",
    )
    writer_setup = []
    writers = [rw_node]
    if options.output_file:
        pre, post = moore_output_writers(
            options.output_file,
            options.output_type,
            rw_outputs,
        )
        writer_setup += pre
        writers += post

    dec = CompositeNode(
        'hlt_decision',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[line.node for line in lines],
        force_order=False)

    node = CompositeNode(
        'moore',
        combine_logic=NodeLogic.LAZY_AND,
        children=writer_setup + [dec] + writers,
        force_order=True)
    return node, barriers


def run_moore(options, make_lines=None, public_tools=[], allen_hlt1=False):
    """Configure Moore's entire control and data flow.

    Convenience function that configures all services, creates the
    standard Moore control flow and builds the the data flow (by
    calling the global lines maker).

    Args:
        options (ApplicationOptions): holder of application options
        make_lines: function returning a list of `DecisionLine` objects
        public_tools (list): list of public `Tool` instances to configure
        allen_hlt1: Configure Allen Hlt1 lines when processing Hlt2 on Allen filtered Hlt1 sample, Moore Hlt1 lines are confiugred when allen_hlt1=False

    """
    # First call configure_input for its important side effect of
    # changing the default values of default_raw_event's arguments.
    # The latter is the deepest part of the call stack of make_lines.
    config = configure_input(options)
    # Then create the data (and control) flow for all lines.
    lines = (make_lines or options.lines_maker)()

    # Determine whether Hlt1, Hlt2 or Spruce is being processed
    hlt1 = all(l.name.startswith('Hlt1') for l in lines)
    hlt2 = all(l.name.startswith('Hlt2') for l in lines)
    spruce = all(l.name.startswith('Spruce') for l in lines)
    assert hlt1 ^ hlt2 ^ spruce, 'Expected exclusively all Hlt1, all Hlt2 or all Spruce lines'
    assert not (
        hlt1 & allen_hlt1
    ), 'allen_hlt1 can only be configured when processing Hlt2 on Allen filtered Hlt1 samples'

    if hlt1:
        process = "hlt1"
    if hlt2:
        process = "hlt2"
    elif spruce:
        process = "spruce"

    # Combine all lines and output in a global control flow.
    top_cf_node, barriers = moore_control_flow(options, lines, process,
                                               allen_hlt1)
    config.update(
        configure(
            options,
            top_cf_node,
            public_tools=public_tools,
            barrier_algorithms=barriers))
    # TODO pass config to gaudi explicitly when that is supported
    return config


def setup_allen_non_event_data_service(dump_binaries=False):
    """Setup Allen non-event data

    An ExtSvc is added to the ApplicationMgr to provide the Allen non-event data (geometries etc.)
    """
    producers = [
        p(DumpToFile=dump_binaries)
        for p in (DumpVPGeometry, DumpUTGeometry, DumpFTGeometry,
                  DumpMuonGeometry, DumpMuonTable, DumpMagneticField,
                  DumpBeamline, DumpUTLookupTables)
    ]
    ApplicationMgr().ExtSvc += [
        AllenUpdater(OutputLevel=2),
    ] + producers


def get_allen_hlt1_decision_ids():
    """Read Allen HLT1 decision IDs using the JSON sequence file.

    """
    config_file_path = ALLEN_SEQUENCE
    try:
        with open(os.path.expandvars(config_file_path)) as config_file:
            config = json.load(config_file)
    except:
        log.error(
            "Could not find Allen sequence file %s; make sure you have built Allen",
            config_file_path)
        raise

    # Line names are stored in the JSON as a single string separated by commas
    active_lines = str(config["gather_selections"]["names_of_active_lines"])
    decision_names = [name + "Decision" for name in active_lines.split(",")]
    return _build_decision_ids(decision_names)


def allen_control_flow(options):
    options.finalize()

    setup_allen_non_event_data_service()

    # Write DecReports raw banks
    call_allen_alg = call_allen(filter_hlt1=True, tck=options.tck)
    allen_dec_reports = call_allen_alg.DecReportsLocation

    # We will write the reports to raw banks at these locations
    algs = []
    locations_to_persist = []
    report_banks_to_write = ['HltDecReports']
    reports_raw_event = default_raw_event(report_banks_to_write)

    # TODO we probably shouldn't write in Trigger/RawEvent when
    # running on reconstructed data. Instead, we should create a
    # RawEvent at DAQ/RawEvent, which would make HltDecReportsWriter
    # happy, or make raw events/banks const and adapt everything.
    kill_existing = True
    if kill_existing:
        all_banks_to_write = (report_banks_to_write)
        algs.append(
            bankKiller(
                RawEventLocations=[reports_raw_event],
                BankTypes=all_banks_to_write))

    drw = HltDecReportsWriter(
        InputHltDecReportsLocation=allen_dec_reports,
        outputs=dict(
            OutputRawEventLocation=force_location(reports_raw_event.location)),
    )

    algs.extend([drw])
    locations_to_persist.append(drw.OutputRawEventLocation)

    decision_ids = get_allen_hlt1_decision_ids()
    setup_ann_service(hlt1_decision_ids=decision_ids, hlt2_decision_ids={})

    locations_to_persist.append(reports_raw_event)

    report_writers_node = CompositeNode(
        'report_writers',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=algs,
        force_order=True)

    # Transform to a list of unique str
    locations_to_persist = list(
        set(map(format_output_location, locations_to_persist)))

    writers = [report_writers_node]
    if options.output_file:
        writers.extend(
            output_writers(options.output_file, options.output_type,
                           locations_to_persist))

    return CompositeNode(
        'allen',
        combine_logic=NodeLogic.LAZY_AND,
        # TODO we are not running the writer setup algs here
        children=[call_allen_alg] + writers,
        force_order=True)


def run_allen(options):
    """Configure Allen within Mooore.

    Convenience function that sets up an Allen node and sets up services

    Args:
        options (ApplicationOptions): holder of application options

    """
    config = configure_input(options)

    top_cf_node = allen_control_flow(options)

    config.update(configure(options, top_cf_node))
    # TODO pass config to gaudi explicitly when that is supported
    return config


def run_allen_reconstruction(options,
                             make_reconstruction,
                             dump_binaries=False,
                             public_tools=[]):
    """Configure the Allen reconstruction data flow

    Convenience function that configures all services and creates a data flow.

    Args:
        options (ApplicationOptions): holder of application options
        make_reconstruction: function returning a single CompositeNode object
        public_tools (list): list of public `Tool` instances to configure

    """

    setup_allen_non_event_data_service(dump_binaries=dump_binaries)
    return run_reconstruction(
        options, make_reconstruction, public_tools=public_tools)


def run_reconstruction(options, make_reconstruction, public_tools=[]):
    """Configure the reconstruction data flow with a simple control flow.

    Convenience function that configures all services and creates a data flow.

    Args:
        options (ApplicationOptions): holder of application options
        make_reconstruction: function returning a single CompositeNode object
        public_tools (list): list of public `Tool` instances to configure

    """

    config = configure_input(options)
    reconstruction = make_reconstruction()
    config.update(
        configure(options, reconstruction.node, public_tools=public_tools))
    # TODO pass config to gaudi explicitly when that is supported
    return config


#: Regular expression (compiled) defining the valid line names
HLT_LINE_NAME_PATTERN = re.compile(r'^(Hlt[12]|Spruce)[A-Za-z0-9_]+Line$')


def valid_name(name):
    """Return True if name follows the HLT line name conventions."""
    try:
        return HLT_LINE_NAME_PATTERN.match(name) is not None
    except TypeError:
        return False


def _get_param_default(function, name):
    """Return the default value of a function parameter.

    Raises TypeError if ``function`` has no default keyword parameter
    called ``name``.
    """
    try:
        try:
            sig = inspect.signature(function)
            p = sig.parameters[name]
            if p.default == p.empty:
                raise ValueError()  # just forward to raise below
            return p.default
        except AttributeError:  # Python 2 compatibility
            spec = inspect.getargspec(function)
            i = spec.args.index(name)  # ValueError if not found
            return spec.defaults[i - len(
                spec.args)]  # IndexError if not keyword
    except (KeyError, ValueError, IndexError):
        raise TypeError('{!r} has no keyword parameter {!r}'.format(
            function, name))


def add_line_to_registry(registry, name, maker):
    """Add a line maker to a registry, ensuring no name collisions."""
    if name in registry:
        raise ValueError('{} already names an HLT line maker: '
                         '{}'.format(name, registry[name]))
    registry[name] = maker


def register_line_builder(registry):
    """Decorator to register a named HLT line.

    The decorated function must have keyword parameter `name`. Its
    default value is used as the key in `registry`, under which the
    line builder (maker) is registered.

    Usage:

        >>> from PyConf.tonic import configurable
        ...
        >>> all_lines = {}
        >>> @register_line_builder(all_lines)
        ... @configurable
        ... def the_line_definition(name='Hlt2TheNameOfTheLine'):
        ...     # ...
        ...     return DecisionLine(name=name, algs=[])  # filled with control flow
        ...
        >>> 'Hlt2TheNameOfTheLine' in all_lines
        True

    """

    def wrapper(wrapped):
        name = _get_param_default(wrapped, 'name')
        if not valid_name(name):
            raise ValueError('{!r} is not a valid HLT line name'.format(name))
        add_line_to_registry(registry, name, wrapped)
        # TODO return a wrapped function that checks the return type is DecisionLine
        return wrapped

    return wrapper
