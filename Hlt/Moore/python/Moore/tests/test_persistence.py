###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.persistence import format_output_location
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer


def test_format_output_location():
    """The argument should be appended with `#1` if not already present."""
    assert format_output_location("/Event/Tracks") == "/Event/Tracks#1"
    # Should not append depth specifier if not already present
    assert format_output_location("/Event/Tracks#1") == "/Event/Tracks#1"
    assert format_output_location("/Event/Tracks#93") == "/Event/Tracks#93"
    assert format_output_location("/Event/Tracks#*") == "/Event/Tracks#*"
    # Should accept a DataHandle object as an argument
    producer = Gaudi__Examples__IntDataProducer()
    assert format_output_location(producer.OutputLocation) == "{}#1".format(
        producer.OutputLocation.location)
