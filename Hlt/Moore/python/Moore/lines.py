###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import
import logging

from Configurables import LumiCounterMerger
from PyConf.Algorithms import DeterministicPrescaler
from PyConf.application import make_odin
from PyConf.components import Algorithm
from PyConf.control_flow import CompositeNode, NodeLogic

from Moore.selreports import (
    UnconvertableAlgorithmError,
    convert_output as convert_output_for_selreports,
)
from RecoConf.reconstruction_objects import make_pvs

from .persistence.particle_moving import CopyParticles, dvalgorithm_locations, is_particle_producer
from .persistence.persistreco import persistreco_line_outputs

#: Ending common to all line decision names
DECISION_SUFFIX = "Decision"

log = logging.getLogger(__name__)


def _producer(datahandle_or_producer):
    try:
        return datahandle_or_producer.producer
    except AttributeError:
        return datahandle_or_producer


def _node_basics(node):
    """Return the flattened list of all non-node descendents of `node`."""
    basics = []
    for child in node.children:
        if isinstance(child, CompositeNode):
            basics += _node_basics(child)
        else:
            basics.append(child)
    return basics


def _remove_decision_output(algs, decision_node):
    """Return `algs` with any children of `decision_node` removed.

    The original order of `algs` may not be reflected in the returned list.
    """
    decision_algs = _node_basics(decision_node)
    return list(set(map(_producer, algs)) - set(decision_algs))


class DecisionLine(object):
    """Object fully qualifying an HLT line.

    The control flow of the line, a `PyConf.control_flow.CompositeNode`, is
    exposed as the `node` property. It will run the given `algs` prepended
    with a `DeterministicPrescaler` (even if the prescale is 1), all combined
    with `PyConf.control_flow.NodeLogic.LAZY_AND` logic.

    The attributes of this object should be considered immutable.

    Args:
        name (str): name of the line
        algs (iterable of `Algorithm`): control flow of the line
        prescale (float): accept fraction of the prescaler

    Attributes:
        name (str): name of the line
        node (`PyConf.control_flow.CompositeNode`): full control flow of the
            line; if this CF node is positive, the line is said to have
            'fired'
        output_producer (`Algorithm`): the last algorithm in the line's
            control flow. On a positive CF decision the application may wish
            to use this output, e.g. to fill selection reports or luminosity
            banks
    """

    def __init__(self, name, algs, prescale=1.0):
        # Line names ending with "Decision" would be confusing, so forbid it
        if name.endswith(DECISION_SUFFIX):
            raise ValueError("line name ({}) should not end with {}".format(
                name, DECISION_SUFFIX))
        self.name = name
        self.node = self._decision_node(self.name, algs, prescale)
        self.output_producer = self._output_producer(self.node)

    @property
    def decision_name(self):
        """Decision name."""
        return self.name + DECISION_SUFFIX

    @property
    def produces_output(self):
        """Return True if this line produces output."""
        return self.output_producer is not None

    @staticmethod
    def _decision_node(name, algs, prescale):
        """Return a `CompositeNode` that evaluates this line's decision."""
        prescaler = DeterministicPrescaler(
            AcceptFraction=prescale,
            SeedName=name + "Prescaler",
            ODINLocation=make_odin())
        node = CompositeNode(name, (prescaler, ) + tuple(algs))
        return node

    @staticmethod
    def _output_producer(node):
        """Return the producer that defines the 'output data' of this line.

        The producer is defined as the last child in the control flow node,
        i.e. the last item passed as the `algs` argument to the
        `DecisionLine` constructor.

        How the output data of the producer is used depends on the application.

        If the producer creates no output, None is returned.
        """
        last = node.children[-1]
        # Could in principle have control node here; will deal with this use
        # case if it arises
        assert isinstance(last, Algorithm), last
        # If the last algorithm produces nothing, there is no 'producer'
        return last if last.outputs else None


class HltLuminosityLine(DecisionLine):
    """Object fully qualifying an HLT luminosity line.

    A luminosity line collects information from luminometers, sub-detectors
    which provide time-stable measurements of quantities correlated to the
    instantaneous luminosity delivered to the experiment.

    A luminosity line will rate-limit itself by filtering on the value of the
    `LHCb::ODIN::Lumi` bit in the event type field of the ODIN raw bank.

    The counters computed by the algorithms of this line must be in a format
    that can be consumed by the creator the luminosity raw bank. To ensure
    this, the constructor asserts that the output producer of the line is of
    type `LumiCounterMerger`.
    """

    def __init__(self, name, algs, prescale=1.):
        super(HltLuminosityLine, self).__init__(name, algs, prescale)
        assert self.output_producer.type == LumiCounterMerger


class Hlt1Line(DecisionLine):
    """Object fully qualifying an HLT1 line.

    Extends `DecisionLine` with control flow that ensures additional objects
    are created for later persistence on a positive decision.

    Args:
        name (str): name of the line; must begin with `Hlt1`.
        algs (iterable of `Algorithm`): control flow of the line
        prescale (float): accept fraction of the prescaler

    Attributes:
        objects_to_persist (list of DataHandle): Objects which this lines
            requests to be persisted on a positive decision. The control flow
            of the line will guarantee that these objects will exist in that
            case.
    """

    def __init__(self, name, algs, prescale=1.):
        super(Hlt1Line, self).__init__(name, algs, prescale)
        assert self.name.startswith("Hlt1")
        # The line guarantees that these objects will be present in the TES if
        # this line made a positive decision
        self.objects_to_persist = []
        if self.produces_output:
            output_node, self.objects_to_persist = self._output_node(
                self.node, self.output_producer)
            # Wrap the decision and output nodes
            if output_node.children:
                self.node = CompositeNode(self.name + "DecisionWithOutput",
                                          (self.node, output_node))

    @staticmethod
    def _output_node(decision_node, output_producer):
        try:
            algs = [convert_output_for_selreports(output_producer)]
            objects_to_persist = list(algs[-1].outputs.values())
        except UnconvertableAlgorithmError as e:
            log.warning(
                "Cannot convert output objects for line {}; SelReports will be unavailable: {}"
                .format(decision_node.name, e))
            algs = []
            objects_to_persist = []

        output_node = CompositeNode(
            decision_node.name + "Output",
            # Algorithms already in the decision CF don't need to be in the
            # output CF. Although one can imagine a scheduler that allows this
            # type of duplication, it is currently not permitted under an
            # ordered control flow node (see LHCb#108)
            _remove_decision_output(algs, decision_node),
            combine_logic=NodeLogic.NONLAZY_OR,
            force_order=False)

        return output_node, objects_to_persist


class Hlt2Line(DecisionLine):
    """Object fully qualifying an HLT2 line.

    Extends `DecisionLine` with control flow that ensures additional objects
    are created for later persistence on a positive decision. This supports
    extra outputs (TurboSP) and reconstruction persistence
    (Turbo++/PersistReco).

    Args:
        name (str): name of the line; must begin with `Hlt2`.
        algs (iterable of `Algorithm`): control flow of the line
        prescale (float): accept fraction of the prescaler
        extra_outputs (iterable of 2-tuple): List of `(name, DataHandle)` pairs.
        persistreco (bool): If True, request HLT2 reconstruction persistence.

    Attributes:
        objects_to_persist (list of DataHandle): Objects which this lines
            requests to be persisted on a positive decision. The control flow
            of the line will guarantee that these objects will exist in that
            case.
        extra_outputs (iterable of 2-tuple): List of `(name, DataHandle)`
            pairs which this line requests are persisted on a positive
            decision. The name is just an identifier, which can be used by
            the application to construct a TES path when persisting each
            extra output
        persistreco (bool): If True, this line requests HLT2 reconstruction
            persistence on a positive decision. The CF of the line (the
            `node` attribute) will ensure that the reconstruction algorithms
            that produce the reconstruction objects will run when the line
            fires
    """

    _NAME_PREFIX = "Hlt2"

    def __init__(self,
                 name,
                 algs,
                 prescale=1.,
                 extra_outputs=None,
                 persistreco=False):
        super(Hlt2Line, self).__init__(name, algs, prescale)
        if not self.name.startswith(self._NAME_PREFIX):
            raise ValueError("name {!r} does not start with {!r}".format(
                name, self._NAME_PREFIX))
        self.extra_outputs = frozenset(extra_outputs or [])
        self.persistreco = persistreco
        # The line guarantees that these objects will be present in the TES if
        # this line made a positive decision
        self.objects_to_persist = []
        if self.produces_output:
            output_node, self.objects_to_persist = self._output_node(
                self.node, self.output_producer, self.extra_outputs,
                self.persistreco)
            # Wrap the decision and output nodes
            if output_node.children:
                self.node = CompositeNode(self.name + "DecisionWithOutput",
                                          (self.node, output_node))

    @staticmethod
    def _output_node(decision_node, output_producer, extra_outputs,
                     persistreco):
        # Build list of 2-tuple of (output path components, output producer)
        all_outputs = [(output_producer, (decision_node.name, ))]
        for prefix, output in extra_outputs:
            all_outputs.append((output, (decision_node.name, prefix)))
        algs, objects_to_persist = Hlt2Line._line_outputs(
            all_outputs, persistreco)

        # Run all output producers in a separate CF node, and record all of
        # their outputs so Moore can persist them
        output_node = CompositeNode(
            decision_node.name + "Output",
            # Algorithms already in the decision CF don't need to be in the
            # output CF. Although one can imagine a scheduler that allows this
            # type of duplication, it is currently not permitted under an
            # ordered control flow node (see LHCb#108)
            _remove_decision_output(algs, decision_node),
            combine_logic=NodeLogic.NONLAZY_OR,
            force_order=False)

        return output_node, objects_to_persist

    @staticmethod
    def _line_outputs(outputs_locations, persistreco):
        """Return output-producing algorithms and their outputs.

        Args:
            outputs_locations (list of 2-tuple): Each entry is a 2-tuple of
                a DataHandle and location components tuple. The semantics are
                that this line wishes to have the object represented by the
                DataHandle to be persisted at the TES location represented by
                the location tuple (an N-tuple of str).
            persistreco (bool): If True, this line requests the full HLT2
                reconstruction be persisted along with its other outputs.

        Returns:
            algs (list of Algorithm): Output data producer algorithms which
                this line's CF must run on a positive decision, in order to
                produce the output objects it has requested.
            objects_to_persist (list of DataHandle): Output objects to be
                persisted on a positive decision.

        Note:
            There is an implicit understanding between the logic here and
            that implemented in `Moore.persistence`. The latter can only
            support persisting certain types of objects, and the logic here
            performs some basic configuration-time checking to ensure lines
            do not request objects which Moore does not know how to persist.

            As of this writing, line outputs and extra outputs can only be
            LHCb::Particle producers. If other objects are to be supported,
            this method must be updated alongside the cloner algorithms
            configured in `Moore.persistence.cloning`.
        """
        pvs = make_pvs()
        algs = []
        objects_to_persist = []
        for output, location_components in outputs_locations:
            # Branch on the C++ type of the outputs of the producer
            # For legacy LHCb::Particle outputs, we must navigate back to the
            # producer to determine whether we should run the LHCb::Particle
            # moving algorithm
            producer = _producer(output)
            if is_particle_producer(producer):
                mover = CopyParticles(producer, pvs, location_components)
                algs.append(mover)
                objects_to_persist += dvalgorithm_locations(mover)
            else:
                log.warning(
                    "Unsupported line output type {} (for location {}); this will not be persisted"
                    .format(output, location_components))

        # All Turbo lines (lines producing output) should always get PVs
        if objects_to_persist:
            algs.append(pvs)
            objects_to_persist.append(pvs)

        if persistreco:
            pr_objs = list(persistreco_line_outputs().values())
            algs += pr_objs
            objects_to_persist += pr_objs

        return algs, objects_to_persist

    @staticmethod
    def referenced_locations(datahandle):
        """Return the set of all locations referenced by datahandle.

        The set includes the location of `datahandle` itself.

        Note:
            This method exists because `DataHandle.producer.all_inputs`
            assumes all algorithm outputs are discoverable by PyConf. This is
            not the case for DVAlgorithm descendents, and so this method
            ensures the extra locations produced by such algorithms are
            included. Once we no longer use DVAlgorithm, we can use
            `all_inputs` everywhere and remove this method.
        """
        data = set([datahandle])
        for dh in datahandle.producer.all_inputs:
            try:
                # DVAlgorithms do not expose all output locations
                data.update(dvalgorithm_locations(dh.producer))
            except AssertionError:
                if is_particle_producer(dh.producer):
                    # The producer is not a DVAlgorithm but does create legacy
                    # Particle objects, which often have hidden dependencies to
                    # the other outputs of the producer. To be safe we include
                    # all of those outputs like in the DVAlgorithm case
                    data.update(dh.producer.outputs.values())
                else:
                    data.update([dh])
        return data


class SpruceLine(Hlt2Line):
    """Object fully qualifying a Sprucing line.

    A variant of `Hlt2Line`.
    """

    _NAME_PREFIX = "Spruce"


def HltLine(name, *args, **kwargs):
    """Dispatch to the appropriate line constructor based on `name`.

    Deprecated:
        Use `DecisionLine`, `Hlt2Line` or `SpruceLine` as appropriate, directly.
        See Moore#239.
    """
    if name.startswith("Hlt1"):
        line = Hlt1Line(name, *args, **kwargs)
    elif name.startswith("Hlt2"):
        line = Hlt2Line(name, *args, **kwargs)
    else:
        raise ValueError("use e.g. SpruceLine, DecisionLine, ... directly")
    return line
