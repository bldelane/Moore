###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for persisting the entire HLT2 reconstruction.

The `persistreco_line_outputs` method defines what we mean by "PersistReco".
The data handles returned by that method point to the containers that will be
persisted if a PersistReco-enabled HLT2 line fires.

Some objects which are anyhow persisted as part of the usual line output, such
as primary vertices, are treated different if the line has PersistReco enabled.
See the cloning configuration for those differences.
"""
from RecoConf.reconstruction_objects import reconstruction as _reconstruction


def persistreco_line_outputs():
    """Return a dict of data handles that define reconstruction to be persisted."""
    objs = _reconstruction()
    return dict(
        ChargedProtos=objs["ChargedProtos"],
        NeutralProtos=objs["NeutralProtos"],
        Tracks=objs["Tracks"],
        PVs=objs["PVs"],
        CaloElectrons=objs["CaloElectrons"],
        CaloPhotons=objs["CaloPhotons"],
        CaloMergedPi0s=objs["CaloMergedPi0s"],
        CaloSplitPhotons=objs["CaloSplitPhotons"],
        MuonPIDs=objs["MuonPIDs"],
        RichPIDs=objs["RichPIDs"],
    )
