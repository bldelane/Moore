###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for persisting HLT2 objects in output ROOT/MDF files."""
from __future__ import absolute_import
import itertools
import logging
from pprint import pformat
import re

from Configurables import HltANNSvc, HltLinePersistenceSvc

from Hlt2Conf.data_from_file import unpacked_mc_locations, unpacked_reco_locations
from PyConf import configurable
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.components import get_output
from GaudiConf.PersistRecoConf import PersistRecoPacking

from .cloning import clone_line_outputs
from .location_prefix import prefix
from .packing import pack_stream_objects, pack_stream_mc
from .persistreco import persistreco_line_outputs
from .serialisation import serialise_packed_containers
from .truth_matching import truth_matching, CHARGED_PP2MC_LOC, NEUTRAL_PP2MC_LOC

log = logging.getLogger(__name__)

#: TES prefix under which all persisted objects will be stored
DEFAULT_OUTPUT_PREFIX = "/Event/HLT2"


def format_output_location(l):
    """Return the TES location `l` formatted for an output writer.

    Args:
        l (str or DataHandle)
    """
    try:
        l = l.location
    except AttributeError:
        pass
    # Ensure the location ends with the depth specification `#N` or `#*`
    if not re.match(r".*#(\d+|\*)", l):
        l = "{}#1".format(l)
    return l


def _referenced_locations(lines):
    """Return the set of all locations referenced by each line.

    To serialise the data we must know the all location strings referenced by
    the line output objects. This includes locations of objects all the way
    back up the data flow tree.

    Args:
        lines (list of Hlt2Line)

    Returns:
        all_locs (dict of str to list of str)
    """
    all_locs = {}
    for l in lines:
        # Include the locations of the line outputs themselves
        line_locs = set()
        # Gather locations referenced from higher up the data flow tree
        for dh in l.objects_to_persist:
            line_locs.update(l.referenced_locations(dh))
        # Convert DataHandle to str, for configuring the ANN service
        all_locs[l.decision_name] = [dh.location for dh in line_locs]
    return all_locs


def _register_to_hltannsvc(locations):
    """Register the packed object locations in the HltANNSvc.
    """
    log.debug('Registering locations to HltANNSvc: %s', pformat(locations))
    # TODO: We configure the ANNSvc elsewhere in Moore,
    # but we only want to do it once, so must merge the
    # configuration here with the other
    locs_ids = {loc: i + 1 for i, loc in enumerate(locations)}
    HltANNSvc(
        allowUndefined=False,
        PackedObjectLocations=locs_ids,
    )


@configurable
def persist_line_outputs(lines,
                         data_type,
                         dec_reports,
                         associate_mc,
                         out_stream=DEFAULT_OUTPUT_PREFIX,
                         stream=DEFAULT_OUTPUT_PREFIX,
                         clone_mc=True):
    """Return CF node and output locations of the HLT2 line persistence."""
    cf = []
    protoparticle_relations = []
    if associate_mc:
        truth_matching_cf, proto_rels = truth_matching(lines)
        protoparticle_relations += proto_rels
        cf.append(truth_matching_cf)

    # The persistence service is used by the cloners to get the list of
    # locations to clone given a positive line decision
    persistence_svc = HltLinePersistenceSvc(
        Locations={
            l.decision_name: [dh.location for dh in l.objects_to_persist]
            for l in lines
        },
        TurboPPLines=[l.decision_name for l in lines if l.persistreco],
    )
    log.debug('line_locations: %s', pformat(persistence_svc.Locations))

    # Locations are lost at this point, as the cloners can copy locations not
    # directly referenced by line outputs
    output_cloner_cf, output_cloner_locations = clone_line_outputs(
        persistence_svc, dec_reports, protoparticle_relations, stream,
        clone_mc)
    log.debug('output_cloner_locations: %s', pformat(output_cloner_locations))
    cf.append(output_cloner_cf)

    prdict = persistreco_line_outputs()
    prpacking = PersistRecoPacking(
        stream=out_stream,
        inputs={
            name: prefix(get_output(v).location, stream)
            for name, v in prdict.items()
        },
        data_type=data_type,
    )

    packer_cf, packer_locations, container_map = pack_stream_objects(
        stream, prpacking)
    cf.append(packer_cf)

    if clone_mc:
        mc_packer_cf, packer_mc_locations = pack_stream_mc(stream)
        cf.append(mc_packer_cf)
    else:
        packer_mc_locations = []
    log.debug('packer_locations: %s', pformat(packer_locations))
    log.debug('packer_mc_locations: %s', pformat(packer_mc_locations))
    log.debug('input to ouput container_map: %s', pformat(container_map))

    serialisation_cf, output_raw_location = serialise_packed_containers(
        packer_locations, container_map)
    log.debug('output_raw_location: %s', pformat(output_raw_location))
    cf.append(serialisation_cf)

    # replace locations that are mapped to new locations
    persisted_line_locs = [
        container_map.get(l, l) for l in output_cloner_locations
    ]
    # These locations may not be exposed in the line dataflow tree
    fixed_locs = [
        CHARGED_PP2MC_LOC,
        NEUTRAL_PP2MC_LOC,
        # Must hard-code this location for the real-time reconstruction
        # See Moore#242
        "MuonIDAlgLite/MuonTrackLocation",
    ]
    # Gather all possible locations which might be referenced...
    registered_locs = list(
        itertools.chain(
            fixed_locs,
            persisted_line_locs,
            packer_locations,
            unpacked_reco_locations().values(),
            unpacked_mc_locations().values(),
            itertools.chain(*_referenced_locations(lines).values()),
        ))
    # ...including all prefixed (post-cloning) locations
    registered_locs += [prefix(l, tes_prefix=stream) for l in registered_locs]
    _register_to_hltannsvc(list(set(registered_locs)))

    log.debug('registered_locs: %s', pformat(registered_locs))

    control_flow_node = CompositeNode(
        "hlt2_line_output_persistence",
        combine_logic=NodeLogic.NONLAZY_OR,
        children=cf,
        force_order=True)
    # special locations (if clone_mc=True)
    # /Event/HLT2/pSim/MCParticles and /Event/HLT2/pSim/MCVertices
    # are not serialised to avoid complication and to save space in the clone_mc=False case
    output_packed_locations = list(
        map(format_output_location, packer_mc_locations))

    return control_flow_node, output_packed_locations + [output_raw_location]
