###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration       #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import os

from PyConf.Algorithms import (
    PackMCParticle,
    PackMCVertex,
    PackParticlesAndVertices,
)
from PyConf.components import get_output
from PyConf.control_flow import CompositeNode, NodeLogic

log = logging.getLogger(__name__)


def pack_stream_objects(stream, prpacking):
    """Return a list of packers that will produce all packed output.

    Args:
        stream (str): TES root containing objects to be packed.
        prpacking (PersistRecoPacking object): PersistRecoPacking object that describes packing configuration.

    Returns:
        algs (list): Algorithms to run the packing.
        outputs (list of str): Locations that should be persisted, in the
        specification used by ROOT output writers (e.g. OutputStream).
    """

    persistreco_packers = prpacking.packers()
    container_map = prpacking.inputToPackedLocationMap()

    # This list was taken by scanning the PackParticlesAndVertices source code
    # to see what containers it creates; it is a hack around
    # PackParticlesAndVertices not declaring its output locations
    psandvs_containers = [
        os.path.join(stream, p) for p in [
            'pPhys/Particles',
            'pPhys/Vertices',
            'pPhys/RecVertices',
            'pPhys/FlavourTags',
            'pPhys/Relations',
            'pPhys/PartToRelatedInfoRelations',
            'pPhys/P2IntRelations',
            'pPhys/PP2MCPRelations',
            'pRec/ProtoP/Custom',
            'pRec/Muon/CustomPIDs',
            'pRec/Rich/CustomPIDs',
            'pRec/Track/Custom',
        ]
    ]

    packer = PackParticlesAndVertices(
        InputStream=stream,
        EnableCheck=False,
        # Always create all output containers
        AlwaysCreateContainers=psandvs_containers,
        AlwaysCreateOutput=True,
        DeleteInput=False,
        # Veto the containers already packed by PersistRecoPacking
        VetoedContainers=list(prpacking.inputs.values()),
    )

    packers_cf = CompositeNode(
        "packers",
        children=[packer] + persistreco_packers,
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=True,
    )
    packers_output_locations = (packer.properties["AlwaysCreateContainers"] + \
      [get_output(p.outputs["OutputName"]).location for p in persistreco_packers])

    return packers_cf, packers_output_locations, container_map


def pack_stream_mc(stream):
    """Return a list of packers that will produce mc packed output.

    Args:
        stream (str): TES root containing objects to be packed.

    Returns:
        algs (list): Algorithms to run the packing.
        outputs (list of str): Locations that should be persisted, in the
        specification used by ROOT output writers (e.g. OutputStream).
    """

    mc_packers = []
    mc_packers += [
        PackMCParticle(
            InputName=os.path.join(
                stream,
                PackMCParticle.getDefaultProperties()["InputName"]),
            OutputName=os.path.join(
                stream,
                PackMCParticle.getDefaultProperties()["OutputName"]),
        ),
        PackMCVertex(
            InputName=os.path.join(
                stream,
                PackMCVertex.getDefaultProperties()["InputName"]),
            OutputName=os.path.join(
                stream,
                PackMCVertex.getDefaultProperties()["OutputName"]),
        ),
    ]

    mc_packers_cf = CompositeNode(
        "mc_packers",
        children=mc_packers,
    )
    packers_mc_locations = [p.properties["OutputName"] for p in mc_packers]

    return mc_packers_cf, packers_mc_locations
