###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Logic for cloning HLT2 line outputs under ``/Event/HLT2``.

This is largely a port of the Run 2 logic, up until the serialisation of the
packed containers into raw banks. The serialisation is skipped as we assume
that HLT2 is always writing a DST, and so we can write packed containers
directly.

The Run 2 logic is defined in the Hlt project. Reading the `_persistRecoSeq`
method in the `HltAfterburner.py` file is a good place to start if you want to
understand more.
"""
import logging

from PyConf.Algorithms import (
    CopyLinePersistenceLocations,
    CopyParticle2PVRelationsFromLinePersistenceLocations,
    CopyProtoParticle2MCRelations,
    CopySignalMCParticles,
)
from PyConf.Tools import (
    CaloClusterCloner,
    CaloHypoCloner,
    ProtoParticleCloner,
    ParticleCloner,
)
from PyConf.control_flow import CompositeNode, NodeLogic

from .location_prefix import prefix

log = logging.getLogger(__name__)


def _mc_cloners(stream, protoparticle_relations):
    """Copy signal and associated MC particles to microDST-like locations.

    In the Stripping, the microDST machinery only saves a subset of the
    input MC particles and vertices. This subset contains two possibly
    overlapping contributions:

    1. The MCParticle objects in the signal decay tree; and
    2. The MCParticle objects which can be associated to the reconstructed
    objects selected by at least one stripping line.

    To mimic this behaviour in Tesla, we run almost the same set of
    microDST algorithms, using the relations tables Tesla makes to find the
    MC particles that have been associated to the reconstruction.

    Args:
        stream (str): TES root under which objects will be cloned.
        protoparticle_relations (list of DataHandle): ProtoParticle-to-MCParticle relations.
    """
    # Algorithm to clone the existing relations tables and the MC particles
    # and vertices these tables point to from their original location to
    # the same location prefixed by /Event/Turbo
    copy_pp2mcp = CopyProtoParticle2MCRelations(
        InputLocations=protoparticle_relations,
        OutputPrefix=stream,
        # Don't use the assumed copy of the input ProtoParticle objects, as
        # Tesla doesn't need to copy them (they're already under /Event/Turbo)
        # UseOriginalFrom=True,
    )

    # Algorithm to clone all MC particles and vertices that are associated
    # to the simulated signal process (using LHCb::MCParticle::fromSignal)
    copy_signal_mcp = CopySignalMCParticles(
        OutputPrefix=stream,
        # Don't try to copy the associated reconstruction, as other cloners
        # will have already taken care of it
        SaveAssociatedRecoInfo=False,
    )

    return [copy_pp2mcp, copy_signal_mcp]


def clone_line_outputs(persistence_svc,
                       dec_reports,
                       protoparticle_relations,
                       stream,
                       clone_mc=True):
    """Return a list of cloners that will copy line outputs under `stream`.

    Args:
        persistence_svc (Configurable): Instance of ILinePersistenceSvc Configurable.
        dec_reports (DataHandle): HLT2 decision reports.
        protoparticle_relations (list of DataHandle): ProtoParticle-to-MCParticle relations.
        stream (str): TES root under which objects will be cloned.
        clone_mc (bool): If True, clone MC objects referred to by `protoparticle_relations`.
    """
    line_names = list(persistence_svc.Locations.keys())
    input_locations = set()
    for key in persistence_svc.Locations:
        input_locations.update(persistence_svc.Locations[key])

    # TODO ideally we configure the cluster and digit/ADC cloning here, but
    # running HLT2 from the Brunel reconstruction means we don't actually have
    # access to these, only the CaloHypo objects are available, so we have to
    # ban cluster cloning altogether
    # We only support running on Brunel output at the moment, so hard-code this
    # path choice
    calo_hack = True
    if calo_hack:
        # The cluster cloner should never run given the configuration of the
        # hypo cloner below, but let's be sure
        cluster_cloner = CaloClusterCloner(
            CloneEntriesNeuP=False,
            CloneEntriesChP=False,
            CloneEntriesAlways=False,
        )
        hypo_cloner = CaloHypoCloner(
            CloneClustersNeuP=False,
            CloneClustersChP=False,
            CloneDigitsNeuP=False,
            CloneDigitsChP=False,
            CloneClustersAlways=False,
            CloneDigitsAlways=False,
        )
    else:
        # Always clone all information associated to CALO objects
        # This may end up being impossible due to bandwidth constraints, but
        # let's enable it for now and profile later
        cluster_cloner = CaloClusterCloner(CloneEntriesAlways=True)
        hypo_cloner = CaloHypoCloner(
            CloneClustersAlways=True,
            CloneDigitsAlways=True,
        )

    protoparticle_cloner = ProtoParticleCloner(
        # If we clone the MC objects, the relations tables cloning will be
        # handled by `_mc_cloners`, otherwise this algorithm will take care of
        # cloning the tables
        PP2MCPRelations=protoparticle_relations if not clone_mc else [],
        # Clone clusters associated to tracks
        # TODO The TrackClonerWithClusters tool does not support the Run 3
        # detector
        # ICloneTrack=TrackClonerWithClusters(
        #     CloneAncestors=False
        # )
    )

    particle_cloner = ParticleCloner(ICloneProtoParticle=protoparticle_cloner)

    # Because this algorithm isn't functional, the scheduler won't try to
    # create its inputs (the outputs to be persisted requested by each line)
    # If this was functional, it would have to be a barrier algorithm because
    # we wouldn't want the scheduler to unconditionally run the producers of
    # the input (they should only be run if the line's decision CF was
    # positive)
    # The cloner will emit a warning at runtime if a line requests a location
    # to be cloned which does not hold a supported type
    container_cloner = CopyLinePersistenceLocations(
        OutputPrefix=stream,
        LinesToCopy=line_names,
        ILinePersistenceSvc=persistence_svc.getFullName(),
        Hlt2DecReportsLocation=dec_reports,
        ICloneCaloHypo=hypo_cloner,
        ICloneCaloCluster=cluster_cloner,
        ICloneProtoParticle=protoparticle_cloner,
        ICloneParticle=particle_cloner,
    )

    p2pv_cloner = CopyParticle2PVRelationsFromLinePersistenceLocations(
        # Clone PVs, but not the tracks they reference
        ClonerType='VertexBaseFromRecVertexClonerNoTracks',
        OutputPrefix=stream,
        LinesToCopy=line_names,
        ILinePersistenceSvc=persistence_svc.getFullName(),
        Hlt2DecReportsLocation=dec_reports,
    )

    algs = [container_cloner, p2pv_cloner]
    # If there are relations tables, we must run the makers by hand because the
    # consumer (the ProtoParticle cloner) is not functional
    if protoparticle_relations:
        algs = [rel.producer for rel in protoparticle_relations] + algs

    if clone_mc:
        algs += _mc_cloners(stream, protoparticle_relations)

    cloners_cf = CompositeNode(
        "cloners",
        children=algs,
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=True,
    )
    # We assume that every location the cloners retrieve from the
    # HltPersistenceSvc are cloned under `stream`
    cloners_output_locations = list(
        map(lambda loc: prefix(loc, stream), input_locations))

    return cloners_cf, cloners_output_locations
