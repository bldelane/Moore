###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import os
import re

from PyConf.Algorithms import FilterDesktop
from PyConf.components import force_location
from PyConf.dataflow import UntypedDataHandle

from .location_prefix import prefix

#: Final component of a TES location holding Particle objects
PARTICLES_LOCATION_SUFFIX = "Particles"
# Container type we match against to define 'a container of Particle objects'
PARTICLE_CONTAINER_T = "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >"
# Set of locations we've forced to specific values; must keep track of these to
# ensure we never force the same location twice
_FORCED_LOCATIONS = set()

log = logging.getLogger(__name__)


def dvalgorithm_locations(producer):
    """Return a list of all DVAlgorithm output DataHandles.

    Assumes `producer` represents a DVAlgorithm.
    """
    # Check that the outputs match the output API defined by
    # Hlt2Conf.algorithms
    output_keys = set(producer.outputs)
    assert output_keys in [{"Particles"},
                           {"Particles", "Particle2VertexRelations"}]
    particle_loc = producer.outputs["Particles"].location
    particles_suffix = '/{}'.format(PARTICLES_LOCATION_SUFFIX)
    assert particle_loc.endswith(particles_suffix), particle_loc

    # Algorithms that create '/Particles' locations can also create some
    # additional locations under the same parent path; we want those too
    sneaky_suffixes = [
        'decayVertices', '_RefitPVs', 'Particle2VertexRelations'
    ]

    locations = []
    for suffix in [PARTICLES_LOCATION_SUFFIX] + sneaky_suffixes:
        loc = re.sub(particles_suffix + '$', '/' + suffix, particle_loc)
        # The 'key' of the data handle is taken as `suffix`, but the
        # configurable does not expose these outputs as configurable
        # properties. Because of this, PyConf will be unable to infer the C++
        # type of these output objects, so we explicitly instantiate an untyped
        # data handle
        dh = UntypedDataHandle(producer, suffix, force_location(prefix(loc)))
        locations.append(dh)

    return locations


def _line_output_mover_output_transform(Particles):
    """Map arguments to FilterDesktop output properties."""
    return dict(Output=Particles, ExtraOutputs=[Particles])


def _line_output_mover_input_transform(InputParticles, PrimaryVertices):
    """Map arguments to FilterDesktop input properties."""
    return dict(
        Inputs=[InputParticles],
        InputPrimaryVertices=PrimaryVertices,
        ExtraInputs=[InputParticles, PrimaryVertices],
    )


def CopyParticles(particle_producer, pvs, path_components):
    """Return an algorithm that copies Particle objects.

    Args:
        particle_producer (Algorithm): Producer of particle container to be
        copied. Assumed to inherit from DVCommonBase.
        pvs (DataHandle): Primary vertices used to generated P2PV relations
        tables for the copied Particle objects.
        path_components (list of str): Definition of TES path, relative to
        '/Event', under which to store copied Particle objects.
    """
    # Path components should not contain slashes
    assert all('/' not in pc for pc in path_components)
    # We will add the suffix in this method to ensure consistency
    assert path_components[-1] != PARTICLES_LOCATION_SUFFIX

    output_loc = os.path.join(
        os.path.join("/Event", *path_components), PARTICLES_LOCATION_SUFFIX)
    # Keep track of forced output locations to make sure we don't write to the
    # same place twice
    assert output_loc not in _FORCED_LOCATIONS, "Duplicate CopyParticles instantiation"
    _FORCED_LOCATIONS.add(output_loc)

    # Define a FilterDesktop derivative whose output is forced to go to a
    # specific location
    return FilterDesktop(
        input_transform=_line_output_mover_input_transform,
        output_transform=_line_output_mover_output_transform,
        outputs=dict(Particles=force_location(output_loc)),
        InputParticles=particle_producer.Particles,
        PrimaryVertices=pvs,
        # FilterDesktop doesn't create 'new' particles by
        # default, but pointers to the inputs passing the
        # filters. We override that default behaviour here
        CloneFilteredParticles=True,
        # Ensure that P2PV relations are made; we'll persist these later
        Code='BPVVALID() | ALL',
        # Use 'default' DVAlgorithm behaviour for consistency when we read
        # back the data offline
        WriteP2PVRelations=True,
        # Allow DVCommonBase to ensure `/Particles` suffix to the locations
        ModifyLocations=True,
    )


def is_particle_producer(producer):
    """Return True if `producer` outputs an `LHCb::Particle` container.

    If an output cannot be introspected to have `PARTICLE_CONTAINER_T` type,
    a fallback is used to detect a common type of particle producer:
    descendents of `DVCommonBase`. In this case the output property is called
    `Particles` and output type cannot be deduced by the Python configuration
    framework, so is marked as "unknown_t".

    Note:
        The `Particles` property name is a convention used with Moore and
        Hlt2Conf. It is the argument name in output transforms used to
        decorate various `DVCommonBase` subclasses; see `Hlt2Conf.algorithms`
        for examples.

        If a `DVCommonBase` descendent does not follow these conventions, it
        will not be recognised by this method as a `LHCb::Particle` producer.
    """
    # First, check if the producer has outputs of the correct C++ type
    if PARTICLE_CONTAINER_T in {dh.type for dh in producer.outputs.values()}:
        return True
    # Otherwise, use a heuristic to determine if producer derives from
    # DVCommonBase, in which case it is a Particle producer
    try:
        particles = producer.Particles
        return particles.type == "unknown_t"
    except AttributeError:
        return False
