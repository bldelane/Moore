###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for ProtoParticle-to-MCParticle association.

See the `truth_matching` method for more details on the API, and Moore#197
for general discussion on truth matching in Moore.
"""
from __future__ import absolute_import
import logging

from PyConf.Algorithms import (
    ChargedProtoParticleAssociator,
    MergeLinksByKeysToVector,
    MergeRelationsTablesPP2MCP,
    NeutralProtoParticleAssociator,
)
from PyConf.components import force_location
from PyConf.control_flow import CompositeNode, NodeLogic

from Hlt2Conf.data_from_file import brunel_links, mc_unpackers
from RecoConf import mc_checking

log = logging.getLogger(__name__)
#: TES location for charged ProtoParticle-to-MCParticle relations
CHARGED_PP2MC_LOC = "Relations/ChargedPP2MCP"
#: TES location for neutral ProtoParticle-to-MCParticle relations
NEUTRAL_PP2MC_LOC = "Relations/NeutralPP2MCP"
# Container type we match against to define 'a container of ProtoParticle objects'
PROTOPARTICLE_CONTAINER_T = "KeyedContainer<LHCb::ProtoParticle,Containers::KeyedObjectManager<Containers::hashmap> >"
# Container type we match against to define 'a container of Track objects'
TRACK_CONTAINER_T = "KeyedContainer<LHCb::Event::v1::Track,Containers::KeyedObjectManager<Containers::hashmap> >"
# Container type we match against to define 'a container of CaloCluster objects'
CALOCLUSTER_CONTAINER_T = "KeyedContainer<LHCb::CaloCluster,Containers::KeyedObjectManager<Containers::hashmap> >"


def _collect_dependencies(root, type_name):
    """Return all inputs to `root` with the given type name.

    The full data dependency tree of `root` is searched.
    """
    matches = set()
    # Depth-first search from the root
    frontier = [root]
    to_list = lambda x: x if isinstance(x, list) else [x]
    while frontier:
        alg = frontier.pop()
        for input_list in map(to_list, alg.inputs.values()):
            for input_handle in input_list:
                if input_handle.type == type_name:
                    matches.add(input_handle)
                    continue
                frontier.append(input_handle.producer)
    return matches


def _find_protoparticles(lines):
    """Return all ProtoParticle containers referenced by lines.

    The data dependency tree for line candidates is traversed using
    `_collect_dependencies` to find ProtoParticle containers, and these are
    sorted into categories:

    1. Charged (they refer to a Track object)
    2. Neutral (they refer to a CaloHypo object)
    3. Charged from Brunel (they refer to a fixed PackedProto location)
    4. Neutral from Brunel (they refer to a fixed PackedProto location)

    Args:
        lines (list of Hlt2Line)

    Returns:
        (charged, neutral), (charged_brunel, neutral_brunel):
            Each a set of DataHandle objects that match the respective
            category.
    """
    protos = {
        dh
        for line in lines
        for output in line.objects_to_persist for dh in _collect_dependencies(
            output.producer, PROTOPARTICLE_CONTAINER_T)
    }
    charged = set()
    neutral = set()
    charged_brunel = set()
    neutral_brunel = set()
    # Heuristically determine whether a given container of ProtoParticles was
    # created from the raw event or was taken from the input file. We must
    # truth-match differently between the two cases.
    for dh in protos:
        alg = dh.producer
        prop = alg.inputs.get("InputName")
        # If there is a packed container location in the data dependency tree,
        # assume that these protos come from the input file (i.e. from Brunel)
        if prop is not None and (prop.location.startswith("/Event/pRec") or
                                 prop.location.startswith("/Event/HLT2/pRec")):

            # We happen to know that Brunel outputs charged and neutral protos
            # to so-named locations
            if "Charged" in prop.location:
                charged_brunel.add(dh)
            else:
                neutral_brunel.add(dh)
        else:
            # Assume charged ProtoParticle container if the maker has CALO
            # clusters in its dependencies
            if _collect_dependencies(dh.producer, CALOCLUSTER_CONTAINER_T):
                neutral.add(dh)
            else:
                charged.add(dh)
    return (charged, neutral), (charged_brunel, neutral_brunel)


def _match_charged(charged, charged_brunel, mc_particles):
    brunel_links_from_file = brunel_links()
    relations = []

    # Track association from file
    track_links_brunel = brunel_links_from_file["Tracks"]
    assoc_tracks_brunel = MergeLinksByKeysToVector(
        InputLinksByKeys=[track_links_brunel])

    # Need these links for performing track association from raw
    links_to_lhcbids = mc_checking.make_links_lhcbids_mcparticles_tracking_system(
    )

    # Track association from raw
    for dh in charged:
        tracks = _collect_dependencies(dh.producer, TRACK_CONTAINER_T)
        assert len(tracks) == 1, "Unexpected multiple track dependencies"
        tracks = dict(v1=tracks.pop())
        assoc_tracks = mc_checking.make_links_tracks_mcparticles(
            InputTracks=tracks,
            LinksToLHCbIDs=links_to_lhcbids,
        )
        assoc_tracks_merged = MergeLinksByKeysToVector(
            InputLinksByKeys=[assoc_tracks])
        assoc_charged = ChargedProtoParticleAssociator(
            InputProtoParticles=dh,
            InputMCParticles=mc_particles,
            InputAssociations=assoc_tracks_merged,
        )
        relations.append(assoc_charged.OutputTable)

    # Track association from file
    for dh in charged_brunel:
        assoc_charged = ChargedProtoParticleAssociator(
            InputProtoParticles=dh,
            InputMCParticles=mc_particles,
            InputAssociations=assoc_tracks_brunel,
        )
        relations.append(assoc_charged.OutputTable)

    return relations


def _match_neutral(neutral, neutral_brunel, mc_particles):
    brunel_links_from_file = brunel_links()
    relations = []

    # CaloHypo association from raw
    # This isn't supported yet as we cannot produce CaloHypo-to-ProtoParticle
    # tables
    assert not neutral

    # CaloHypo association from file
    if neutral_brunel:
        # We only expect a single neutral container from Brunel
        assert len(
            neutral_brunel
        ) == 1, "Unexpected multiple neutral ProtoParticle containers"
        neutral_brunel = neutral_brunel.pop()
        hypo_types_brunel = [
            "CaloMergedPi0s", "CaloPhotons", "CaloSplitPhotons"
        ]
        hypo_links_brunel = [
            brunel_links_from_file[t] for t in hypo_types_brunel
        ]
        assoc_hypos_brunel = MergeLinksByKeysToVector(
            InputLinksByKeys=hypo_links_brunel)
        assoc_neutral_brunel = NeutralProtoParticleAssociator(
            InputProtoParticles=neutral_brunel,
            InputMCParticles=mc_particles,
            InputAssociations=assoc_hypos_brunel,
        )
        relations.append(assoc_neutral_brunel.OutputTable)

    return relations


def truth_matching(lines):
    """Return relations tables for the objects produced by the lines.

    Associates ProtoParticle objects selected by lines to MCParticle objects.

    If the line outputs refer to the reconstruction from the file (produced
    by Brunel), the associations are created using the LinksByKey objects
    from the file (produced by Brunel).

    If the line outputs refer to the Moore reconstruction, the associations
    are created from the ground up using the lower-level associators already
    in Moore.

    However the relations are made, they are first made per ProtoParticle
    container. In order to provide a fixed number of output tables in
    well-defined locations, the relations tables we make in Moore are merged
    into two tables: one for charged protos, and one for neutral. Those
    locations are returned by this method.

    Args:
        lines (list of Hlt2Line)

    Returns:
        matching_cf (CompositeNode): Algorithms needed to run the matching.
        outputs (list of str): ProtoParticle-to-MCParticle relations tables.

    Note:
        Associating neutral objects created from the Moore reconstruction is
        currently not supported. This method will raise an `AssertionError`
        if it finds line outputs that refer to neutral Moore reconstruction.
    """
    (charged, neutral), (charged_brunel,
                         neutral_brunel) = _find_protoparticles(lines)
    if neutral:
        log.warning(
            "Neutral truth-matching not supported for lines using reconstruction-from-raw; skipping"
        )
        neutral = set()

    mc_objs = mc_unpackers()
    mc_particles = mc_objs["MCParticles"]
    mc_vertices = mc_objs["MCVertices"]

    # TODO factor out configuration common to RecoConf.mc_checking and use that
    # rather than duplicate it here
    charged_relations = _match_charged(charged, charged_brunel, mc_particles)

    neutral_relations = _match_neutral(neutral, neutral_brunel, mc_particles)

    # These algorithms produce the relations tables we 'export', so we force
    # their locations to a fixed location
    charged_merged = MergeRelationsTablesPP2MCP(
        InputRelationsTables=charged_relations,
        outputs=dict(Output=force_location(CHARGED_PP2MC_LOC)),
    )
    neutral_merged = MergeRelationsTablesPP2MCP(
        InputRelationsTables=neutral_relations,
        outputs=dict(Output=force_location(NEUTRAL_PP2MC_LOC)),
    )
    protoparticle_relations = [charged_merged.Output, neutral_merged.Output]

    # Have to run the MC unpackers by hand for now; if the unpackers become
    # functional we can remove the need for this CF node
    matching_cf = CompositeNode(
        "truth_matching",
        children=[mc_particles, mc_vertices],
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=True,
    )

    return matching_cf, protoparticle_relations
