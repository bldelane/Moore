###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for serialising packed containers to raw banks.

As the output format when running online, MDF, only supports writing a raw
event comprised of raw banks, serialisation is a necessary step in persisting
HLT2 output online. For the sake of uniformity we also perform the
serialisation when writing out ROOT files, e.g. during simulation productions.
"""
from PyConf.application import default_raw_event
from PyConf.Algorithms import HltPackedDataWriter
from PyConf.control_flow import CompositeNode


def serialise_packed_containers(packed_locations, container_map):
    """Return CF node that serialises a set of packed containers to a raw bank.

    Args:
        packer_locations (list of str): Packed object containers to serialise.
        container_map (dict): map of internal locations to user-defined locations.

    Returns:
        serialisation_node (CompositeNode).
    """
    persistreco_raw_event = default_raw_event(['DstData'])
    output_raw_location = persistreco_raw_event.location
    bank_writer = HltPackedDataWriter(
        PackedContainers=packed_locations,
        ContainerMap=container_map,
        OutputRawEventLocation=output_raw_location,
    )

    serialisation_cf = CompositeNode(
        "serialisation",
        children=[bank_writer],
    )

    return serialisation_cf, output_raw_location
