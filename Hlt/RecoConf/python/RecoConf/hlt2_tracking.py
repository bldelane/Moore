###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define the HLT2 track reconstruction outputs for use by lines."""

from PyConf import configurable
from functools import partial

from RecoConf.hlt1_tracking import (
    make_VPClus_hits, make_PrStoreUTHit_hits, make_PrStorePrUTHits_hits,
    make_PrStoreFTHit_hits, make_PrStoreSciFiHits_hits,
    make_VeloClusterTrackingSIMD_hits, get_global_ut_hits_tool,
    get_track_master_fitter, make_hlt1_tracks, make_hlt1_fitted_tracks,
    get_global_materiallocator)

from PyConf.Algorithms import (
    PrForwardTrackingVelo, PrForwardTracking, PrHybridSeeding, PrMatchNN,
    PrLongLivedTracking, TrackBestTrackCreator, TracksFTConverter,
    TracksMatchConverter, TracksDownstreamConverter, TracksVPConverter,
    PrResidualUTHits, PrResidualPrUTHits, PrResidualSeeding,
    PrResidualVeloTracks, PrResidualSciFiHits,
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
    LHCb__Converters__Track__PrSeeding__fromTrackv2PrSeedingTracks as
    SeedConverter, MakeZipContainer__Track_v2, TrackContainerSplitter,
    TrackCloneKiller, TrackContainersMerger, TrackEventFitter,
    TracksToSelection, TrackSelectionMerger, TrackSelectionToContainer)

from PyConf.Tools import PrAddUTHitsTool, UpgradeGhostId, LoKi__Hybrid__TrackSelector as LoKiTrackSelector


@configurable
def make_PrForwardTracking_tracks(input_tracks,
                                  make_ft_hits=make_PrStoreSciFiHits_hits,
                                  ut_hits_tool=get_global_ut_hits_tool,
                                  momentum_window=False):
    """Makes forward tracks with PrForwardTracking (long tracks from upstream seeds).

       Args:
           input_tracks (dict): upstream tracks, needs ``'Pr'`` tracks, e.g. from  `all_upstream_track_types`.
           make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreSciFiHits_hits`.
           ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool`.
           momentum_window: switch for momentum guided search window.

       Returns:
           DataHandle: PrForwardTracking's OutputName.

       Note:
           PrForwardTracking's default does not use a momentum estimate, this is overwritten here.
    """
    return PrForwardTracking(
        SciFiHits=make_ft_hits(),
        InputTracks=input_tracks["Pr"],
        AddUTHitsToolName=ut_hits_tool(),
        UseMomentumSearchWindow=momentum_window,
    ).OutputTracks


@configurable
def make_PrForwardTrackingVelo_tracks(input_tracks,
                                      make_ft_hits=make_PrStoreSciFiHits_hits,
                                      ut_hits_tool=get_global_ut_hits_tool):
    """Makes forward tracks for HLT2 (long tracks from Velo seeds) with PrForwardTrackingVelo.

       Args:
           input_tracks (dict): velo tracks, needs ``'Pr'`` tracks, e.g. from  `all_velo_track_types <RecoConf.hlt1_tracking.all_velo_track_types>`.
           make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreSciFiHits_hits <RecoConf.hlt1_tracking.make_PrStoreSciFiHits_hits>`.
           ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool <RecoConf.hlt1_tracking.get_global_ut_hits_tool>`.

       Returns:
           DataHandle: PrForwardTrackingVelo's OutputName.

       Note:
           PrForwardTrackingVelo's defaults have been overridden in this maker with ``UseMomentumEstimate=False``.
    """
    return PrForwardTrackingVelo(
        InputTracks=input_tracks["Pr"],
        SciFiHits=make_ft_hits(),
        AddUTHitsToolName=ut_hits_tool()).OutputTracks


@configurable
def all_hlt2_forward_track_types(
        input_tracks, make_forward_tracks=make_PrForwardTrackingVelo_tracks):
    """Helper function to get all types of HLT2 forward tracks.

    Args:
        input_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types <RecoConf.hlt1_tracking.all_velo_track_types>`.
        make_forward_tracks (DataHandle): maker of forward tracks, defaults to `make_PrForwardTrackingVelo_tracks`.

    Returns:
        A dict mapping Pr, v1, v2 and v2Zip HLT2 forward tracks to ``'Pr'``, ``'v1'``, ``'v2'`` and ``'v2Zip'`` respectively.
    """
    forward_tracks_pr = make_forward_tracks(input_tracks=input_tracks)
    forward_tracks_v2 = TracksFTConverter(
        TracksUTLocation=input_tracks["v2"],
        TracksFTLocation=forward_tracks_pr).OutputTracksLocation
    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName
    forward_tracks_v2_zip = MakeZipContainer__Track_v2(
        Input=forward_tracks_v2).OutputSelection
    return {
        "Pr": forward_tracks_pr,
        "v2": forward_tracks_v2,
        "v1": forward_tracks_v1,
        "v2Zip": forward_tracks_v2_zip,
    }


@configurable
def make_PrHybridSeeding_tracks(make_ft_hits=make_PrStoreFTHit_hits):
    """Makes seed tracks with PrHybridSeeding [1]_.

    Args:
        make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreFTHit_hits <RecoConf.hlt1_tracking.make_PrStoreFTHit_hits>`.

    Returns:
        A dict mapping v1 and v2 SciFi seeding tracks to ``'v1'`` and ``'v2'`` respectively.

    .. [1] https://cds.cern.ch/record/2027531/

    """
    scifi_tracks_v2 = PrHybridSeeding(FTHitsLocation=make_ft_hits()).OutputName
    scifi_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=scifi_tracks_v2).OutputTracksName
    scifi_tracks_pr = SeedConverter(
        InputTracks=scifi_tracks_v2,
        InputSciFiHits=make_PrStoreSciFiHits_hits()).OutputTracks

    return {
        "v2": scifi_tracks_v2,
        "v1": scifi_tracks_v1,
        "Pr": scifi_tracks_pr
    }


@configurable
def make_PrMatchNN_tracks(velo_tracks,
                          scifi_tracks,
                          ut_hits_tool=get_global_ut_hits_tool):
    """Makes long tracks from SciFi seed tracks, velo tracks and UT hits using PrMatchNN.

    Args:
        velo_tracks (dict): velo tracks, needs ``'v2'`` tracks, e.g. from `all_velo_track_types <RecoConf.hlt1_tracking.all_velo_track_types>`.
        scifi_tracks (dict): SciFi seeding tracks, needs ``'v2'`` tracks, e.g. from `make_PrHybridSeeding_tracks`.
        ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool <RecoConf.hlt1_tracking.get_global_ut_hits_tool>`.

    Returns:
        A dict mapping v1 and v2 long tracks from track-matching to ``'v1'`` and ``'v2'`` respectively.
    """

    match_tracks_pr = PrMatchNN(
        VeloInput=velo_tracks["Pr"],
        SeedInput=scifi_tracks["Pr"],
        AddUTHitsToolName=ut_hits_tool()).MatchOutput

    match_tracks_v2 = TracksMatchConverter(
        TracksSeedLocation=scifi_tracks["v2"],
        TracksVeloLocation=velo_tracks["v2"],
        TracksMatchLocation=match_tracks_pr).OutputTracksLocation

    match_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=match_tracks_v2).OutputTracksName

    return {
        "v2": match_tracks_v2,
        "v1": match_tracks_v1,
        "Pr": match_tracks_pr
    }


@configurable
def make_PrLongLivedTracking_tracks(scifi_tracks,
                                    make_ut_hits=make_PrStorePrUTHits_hits):
    """Makes downstream tracks from SciFi seed tracks and UT hits using PrLongLivedTracking.

    Args:
        scifi_tracks (dict): SciFi seeding tracks, needs ``'Pr'`` tracks, e.g. from `make_PrHybridSeeding_tracks`.
        ut_hits_tool: tool providing UT hits, defaults to `get_global_ut_hits_tool <RecoConf.hlt1_tracking.get_global_ut_hits_tool>`.

    Returns:
        A dict mapping v1 and v2 downstream tracks to ``'v1'`` and ``'v2'`` respectively.
    """
    downstream_tracks = PrLongLivedTracking(
        InputLocation=scifi_tracks["Pr"], UTHits=make_ut_hits()).OutputLocation

    downstream_tracks_v2 = TracksDownstreamConverter(
        TracksFTLocation=scifi_tracks["v2"],
        TracksDownstreamLocation=downstream_tracks).OutputTracksLocation

    downstream_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=downstream_tracks_v2).OutputTracksName

    return {
        "Pr": downstream_tracks,
        "v2": downstream_tracks_v2,
        "v1": downstream_tracks_v1
    }


@configurable
def get_UpgradeGhostId_tool(velo_hits=make_VPClus_hits,
                            ut_hits=make_PrStoreUTHit_hits):
    """Returns instance of UpgradeGhostId given VP and UT hits.

    Args:
        velo_hits (DataHandle): maker of velo hits, defaults to `make_VPClus_hits <RecoConf.hlt1_tracking.make_VPClus_hits>`.
        ut_hits (DataHandle): maker of UT hits, defaults to `make_PrStoreUTHit_hits <RecoConf.hlt1_tracking.make_PrStoreUTHit_hits>`.

    Returns:
        Instance of UpgradeGhostId
    """
    return UpgradeGhostId(
        VPClusterLocation=velo_hits(), UTClusterLocation=ut_hits())


def get_default_hlt2_tracks():
    """Function to get default set of tracks reconstructed in HLT2, which are later used as input to `make_TrackBestTrackCreator_tracks`.

    Returns:
        A dict mapping all types of velo, upstream, HLT1 forward fitted, HLT2 forward, SciFi seeding, downstream and matched long tracks to ``'Velo'``, ``'Upstream'``, ``'ForwardFastFitted'``, ``'Forward'``, ``'Seed'``, ``'Downstream'`` and ``'Match'``` respectively.
    """
    hlt1_tracks = make_hlt1_tracks()
    fitted_hlt1_tracks = make_hlt1_fitted_tracks(hlt1_tracks)
    hlt2_forward_tracks = all_hlt2_forward_track_types(hlt1_tracks["Velo"])
    scifi_tracks = make_PrHybridSeeding_tracks()
    downstream_tracks = make_PrLongLivedTracking_tracks(scifi_tracks)
    match_tracks = make_PrMatchNN_tracks(hlt1_tracks["Velo"], scifi_tracks)
    return {
        "Velo": hlt1_tracks["Velo"],
        "Upstream": hlt1_tracks["Upstream"],
        "ForwardFastFitted": fitted_hlt1_tracks,
        "Forward": hlt2_forward_tracks,
        "Seed": scifi_tracks,
        "Downstream": downstream_tracks,
        "Match": match_tracks
    }


def get_default_track_list_for_light_reco():
    """Function to set the default order of tracks which are used as input for the track fit.

    Returns:
        A list of strings.
    """
    return ["Forward", "Match", "Downstream"]


def get_default_track_list_for_TrackBestTrackCreator():
    """Function to set the default order of tracks which are used as input to `make_TrackBestTrackCreator_tracks`.

    Returns:
        A list of strings.
    """
    #TODO: the list has a random order, pls fix
    return [
        "Velo", "ForwardFastFitted", "Upstream", "Match", "Seed", "Forward",
        "Downstream"
    ]


def get_default_out_track_types_for_light_reco():
    """Returns default output track types of light reco sequence.

    Returns:
        Dict with lists of strings.
    """
    return {
        "Best": ["BestLong", "BestDownstream"],
        "Unfitted4Calo": ["SeedDecloned"]
    }


def get_default_tracks_for_calo(tracks, light_reco=False):
    """Returns default tracks used as input for calo.

    Returns:
        A list of strings.
    """
    if light_reco:
        track_dict = get_default_out_track_types_for_light_reco()
        track_list = []
        for key in ["Best", "Unfitted4Calo"]:
            track_list += track_dict[key]
        trackSelections = [tracks[key]["v1"] for key in track_list]
        return TrackSelectionMerger(
            InputLocations=trackSelections).OutputLocation
    else:
        return tracks["Best"]["v1"]


@configurable
def kill_clones(inputTracks,
                referenceTracks=None,
                SkipSameContainerTracks=True,
                KeepUnFitted=True,
                UseUnFittedRef=False):
    """Perform the clone killing with respect to `referenceTracks` containers without the fit.
    """

    if referenceTracks is not None:
        clone_killer = partial(
            TrackCloneKiller,
            SkipSameContainerTracks=SkipSameContainerTracks,
            KeepUnFitted=KeepUnFitted,
            UseUnFittedRef=UseUnFittedRef)
        if len(referenceTracks) == 1:
            declonned = clone_killer(
                TracksInContainer=inputTracks,
                TracksRefContainer=referenceTracks[0]).TracksOutContainer
        else:
            selections = []
            for reference in referenceTracks:
                selections += [
                    TracksToSelection(InputLocation=reference).OutputLocation
                ]
            merged_selections = TrackSelectionMerger(
                InputLocations=selections).OutputLocation
            # to be fixed:
            merged_tracks = TrackSelectionToContainer(
                InputLocation=merged_selections).OutputLocation
            declonned = clone_killer(
                TracksInContainer=inputTracks,
                TracksRefContainer=merged_tracks).TracksOutContainer
    else:
        declonned = inputTracks

    return declonned


@configurable
def fit_and_select(inputTracks,
                   referenceTracks=None,
                   get_fitter_tool=get_track_master_fitter,
                   get_ghost_tool=get_UpgradeGhostId_tool,
                   do_not_refit=False,
                   nameSuffix=""):
    """Perform the fit and selection of tracks:
        1. Clone killing with respect to `referenceTracks` containers of (preferably) fitted tracks
        2. Fit declonned container using  `TrackEventFitter`
        3. Select tracks using `TrackBestTrackCreator` with no-fit mode
    """

    declonned = kill_clones(
        inputTracks=inputTracks, referenceTracks=referenceTracks)

    if do_not_refit:
        fitted = declonned
    else:
        fitted = TrackEventFitter(
            name="TrackEventFitter" + nameSuffix,
            TracksInContainer=declonned,
            Fitter=get_fitter_tool(MaxUpdateTransports=0)).TracksOutContainer

    best = TrackBestTrackCreator(
        name="BestTrackCreator" + nameSuffix,
        TracksInContainers=[fitted],
        GhostIdTool=get_ghost_tool(),
        DoNotRefit=True,
        AddGhostProb=True,
        FitTracks=False).TracksOutContainer

    return best


@configurable
def make_TrackBestTrackCreator_tracks(
        tracks,
        track_version,
        light_reco=False,
        get_tracklist=get_default_track_list_for_TrackBestTrackCreator,
        get_fitter_tool=get_track_master_fitter,
        get_ghost_tool=get_UpgradeGhostId_tool,
        do_not_refit=False):
    """Persists best quality tracks, calls track fitters, kills clones and adds neural-net response for fake-track (a.k.a. ghost) rejection.

    Args:
        tracks (dict): reconstructed tracks, e.g. from `get_default_hlt2_tracks`.
        track_version (string): track version of input and output tracks.
        get_tracklist (list of strings): sets the list of `tracks` which is used as input to TrackBestTrackCreator's ``TracksInContainers``. Be aware that changing the order of tracks in that list has an impact on the output and performance of TrackBestTrackCreator. Defaults to `get_default_track_list_for_TrackBestTrackCreator`
        get_fitter_tool: track fitting tool, defaults to `get_track_master_fitter <RecoConf.hlt1_tracking.get_track_master_fitter>`.
        get_ghost_tool: ghostId tool, adding a neural-net response that has been trained to reject fake (a.k.a. ghost) tracks.
        do_not_refit (bool): Wheter or not to not refit input tracks.

    Returns:
        DataHandle: Best tracks

    Note:
        TrackBestTrackCreator's defaults have been overridden in this maker with ``InitTrackStates=False, DoNotRefit=do_not_refit, FitTracks=True``
    """

    outTracks = {}
    track_list = [
        tracks[track_type][track_version] for track_type in get_tracklist()
    ]
    outTracks["Best"] = TrackBestTrackCreator(
        TracksInContainers=track_list,
        Fitter=get_fitter_tool(),
        GhostIdTool=get_ghost_tool(),
        DoNotRefit=do_not_refit,
        AddGhostProb=True,
        FitTracks=True).TracksOutContainer

    return outTracks


@configurable
def make_light_reco_best_tracks(
        tracks,
        track_version,
        fit_preselection=None,
        get_tracklist=get_default_track_list_for_light_reco,
        get_fitter_tool=get_track_master_fitter,
        get_ghost_tool=get_UpgradeGhostId_tool,
        do_not_refit=False):
    """Persists best quality tracks, calls track fitters, kills clones and adds neural-net response for fake-track (a.k.a. ghost) rejection.
       Rejects too soft tracks prior to the fit.

    Returns:
        outTracks: dictionaty of track DataHandles.
    """

    track_list = get_tracklist()
    input_tracks = {
        track_type: tracks[track_type][track_version]
        for track_type in track_list
    }
    dictTracks = {}

    selected_tracks = {}
    if fit_preselection is not None:
        selector = LoKiTrackSelector(Code=fit_preselection)
        for trType in track_list:
            splitter = TrackContainerSplitter(
                name="TrackContainerSplitter" + trType,
                TracksInContainer=input_tracks[trType],
                Selector=selector)
            selected_tracks[trType] = splitter.PassedContainer
            dictTracks["Soft" + trType] = splitter.RejectedContainer

        tracks_to_merge = [dictTracks["SoftForward"], dictTracks["SoftMatch"]]
        dictTracks["SoftLong"] = TrackContainersMerger(
            name="TrackContainersMergerSoftLong",
            InputLocations=tracks_to_merge).OutputLocation
    else:
        selected_tracks = {
            trType: input_tracks[trType]
            for trType in track_list
        }

    dictTracks["BestForward"] = fit_and_select(
        inputTracks=selected_tracks["Forward"],
        do_not_refit=do_not_refit,
        nameSuffix="Forward")

    dictTracks["BestMatch"] = fit_and_select(
        inputTracks=selected_tracks["Match"],
        referenceTracks=[dictTracks["BestForward"]],
        do_not_refit=do_not_refit,
        nameSuffix="Match")
    #  all of these gives a very tiny more ~0.01% long tracks wrt to number of long tracks from TrackBestTrackCreator(Match, Forward)

    tracks_to_merge = [dictTracks["BestMatch"], dictTracks["BestForward"]]
    dictTracks["BestLong"] = TrackContainersMerger(
        name="TrackContainersMergerLong",
        InputLocations=tracks_to_merge).OutputLocation
    dictTracks["BestDownstream"] = fit_and_select(
        inputTracks=selected_tracks["Downstream"],
        referenceTracks=[dictTracks["BestLong"]],
        do_not_refit=do_not_refit,
        nameSuffix="Downstream")
    #  this gives ~0.5% less downstream tracks wrt to number of downstream tracks from TrackBestTrackCreator(Long, Downstream)

    seed_reference_keys = ["BestLong", "BestDownstream"]
    dictTracks["SeedDecloned"] = kill_clones(
        inputTracks=tracks["Seed"][track_version],
        referenceTracks=[dictTracks[key] for key in seed_reference_keys])

    out_track_dict = get_default_out_track_types_for_light_reco()
    outTrackTypes = out_track_dict["Best"] + out_track_dict["Unfitted4Calo"]
    outTracks = {
        track_type: dictTracks[track_type]
        for track_type in outTrackTypes
    }
    return outTracks


@configurable
def make_pr_kf_light_reco_best_tracks(tracks,
                                      fast_reco,
                                      get_ghost_tool=get_UpgradeGhostId_tool,
                                      fit_forward_first=True):
    """
    Preselect forward,match, and downstream tracks
    Fit forward tracks -> TrackBestTrackCreator (TBTC) for ghost rejection
    Clone kill match Tracks with respect to forward -> Fit -> TBTC for ghost rejection
    Merge match and forward -> BestLong
    Clone kill downstream with respect to BestLong -> Fit -> TBTC for ghost rejection

    Returns:
        DataHandles: BestLong, BestDownstream
    """

    from PyConf.Algorithms import (PrCloneKillerLong, PrCloneKillerDown,
                                   PrKalmanFilter, PrKalmanFilter_Downstream)
    from PyConf.Tools import TrackMasterExtrapolator

    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ut_hits = make_PrStorePrUTHits_hits()
    ft_hits = make_PrStoreSciFiHits_hits()

    if fit_forward_first:
        first, name_1 = tracks['Forward']['Pr'], "Forward"
        second, name_2 = tracks['Match']['Pr'], "Match"

        first_ut_hits = tracks['UTHitsRes'] if fast_reco else ut_hits
        first_ft_hits = tracks['FTHitsRes'] if fast_reco else ft_hits

        second_ut_hits = ut_hits
        second_ft_hits = ft_hits

    else:
        first, name_1 = tracks['Match']['Pr'], "Match"
        second, name_2 = tracks['Forward']['Pr'], "Forward"

        second_ut_hits = tracks['UTHitsRes'] if fast_reco else ut_hits
        second_ft_hits = tracks['FTHitsRes'] if fast_reco else ft_hits

        first_ut_hits = ut_hits
        first_ft_hits = ft_hits

    max_chi2 = 2.8

    kf_template = partial(
        PrKalmanFilter,
        MaxChi2=max_chi2,
        MaxChi2PreOutlierRemoval=20,
        HitsVP=vp_hits,
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()))

    tbtc_template = partial(
        TrackBestTrackCreator,
        MaxChi2DoF=max_chi2,
        GhostIdTool=get_ghost_tool(),
        DoNotRefit=True,
        AddGhostProb=True,
        FitTracks=False)

    fitted_1 = kf_template(
        name="PrKalmanFilter" + name_1,
        Input=first,
        HitsUT=first_ut_hits,
        HitsFT=first_ft_hits).Output

    best_1 = tbtc_template(
        name="TBTC_" + name_1,
        TracksInContainers=[fitted_1]).TracksOutContainer

    declonned_2 = PrCloneKillerLong(
        name="CloneKiller" + name_2,
        TracksInContainer=second,
        TracksRefContainer=best_1,
    ).TracksOutContainer

    fitted_2 = kf_template(
        name="PrKalmanFilter" + name_2,
        Input=declonned_2,
        HitsUT=second_ut_hits,
        HitsFT=second_ft_hits).Output

    best_2 = tbtc_template(
        name="TBTC" + name_2, TracksInContainers=[fitted_2]).TracksOutContainer

    best_long = TrackContainersMerger(
        InputLocations=[best_1, best_2]).OutputLocation

    declonned_down = PrCloneKillerDown(
        TracksInContainer=tracks['Downstream']['Pr'],
        TracksRefContainer=best_long,
    ).TracksOutContainer

    fitted_down = PrKalmanFilter_Downstream(
        Input=declonned_down,
        MaxChi2=max_chi2,
        MaxChi2PreOutlierRemoval=20,
        HitsVP=vp_hits,
        HitsUT=tracks['UTHitsRes'] if fast_reco else ut_hits,
        HitsFT=ft_hits,
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator())).Output

    best_down = tbtc_template(
        name="TBTC_down", TracksInContainers=[fitted_down]).TracksOutContainer

    best_seed = kill_clones(
        inputTracks=tracks["Seed"]["v1"],
        referenceTracks=[best_long, best_down])

    return {
        'BestLong': best_long,
        'BestDownstream': best_down,
        'SeedDecloned': best_seed
    }


@configurable
def make_hlt2_tracks(light_reco=False, fast_reco=False, use_pr_kf=False):
    """Function to get all types of tracks reconstructed in HLT2

    Returns:
        A dict mapping all types of velo, upstream, HLT1 forward fitted, HLT2 forward, SciFi seeding, downstream, matched long and best tracks to ``'Velo'``, ``'Upstream'``, ``'ForwardFastFitted'``, ``'Forward'``, ``'Seed'``, ``'Downstream'``, ``'Match'`` and ``'Best'`` respectively.
    """

    if fast_reco:
        track_dict = get_fast_hlt2_tracks()
    else:
        track_dict = get_default_hlt2_tracks()

    track_version = "v1"
    if light_reco:
        if use_pr_kf:
            # it's currently hardcoded but let's make sure that everyone knows we only return v1 for now
            assert track_version == 'v1'
            track_containers = make_pr_kf_light_reco_best_tracks(
                tracks=track_dict, fast_reco=fast_reco)
        else:
            track_containers = make_light_reco_best_tracks(
                tracks=track_dict, track_version=track_version)
    else:
        track_containers = make_TrackBestTrackCreator_tracks(
            tracks=track_dict, track_version=track_version)

    for trType in track_containers.keys():
        track_dict[trType] = {track_version: track_containers[trType]}

    return track_dict


def make_ReduceVeloTracks_fromMatch(input_tracks, velotracks):
    """
    Function to remove Velo tracks used by PrMatchNN algorithm and create a new velo track container for the residual tracks, which are later used as input to other algorithm PrForwardTracking
    """
    velo_tracks = PrResidualVeloTracks(
        TracksLocation=input_tracks["Pr"],
        VeloTrackLocation=velotracks["Pr"]).VeloTrackOutput
    v2_tracks = TracksVPConverter(
        TracksLocation=velo_tracks).OutputTracksLocation
    return {"Pr": velo_tracks, "v2": v2_tracks}


def make_ReduceSeedTracks_fromMatch(match_tracks, seed_tracks):
    """
    Function to remove Seed tracks used by PrMatchNN algorithm and create a new SeedTracks container for the residual tracks, which are later used as input to other algorithm PrLongLivedTracking
    """
    residual_seed_v2 = PrResidualSeeding(
        MatchTracksLocation=match_tracks["Pr"],
        SeedTracksLocation=seed_tracks["v2"]).SeedTracksOutput

    residual_seed_pr = SeedConverter(
        InputTracks=residual_seed_v2,
        InputSciFiHits=make_PrStoreSciFiHits_hits()).OutputTracks

    return {"Pr": residual_seed_pr, "v2": residual_seed_v2}


@configurable
def make_ReduceSciFiHits_fromMatch(input_tracks,
                                   make_ft_hits=make_PrStoreSciFiHits_hits):
    """
    Function to remove SciFi hits used by PrMatchNN algorithm and create a new SciFiHits container for the residual SciFi hits, which are later used as input to other algorithm PrForwardTracking
    """
    return PrResidualSciFiHits(
        TracksLocation=input_tracks["Pr"],
        SciFiHitsLocation=make_ft_hits()).SciFiHitsOutput


@configurable
def make_ReduceUTHits_fromMatch(match_tracks,
                                make_ut_hits=make_PrStoreUTHit_hits):
    """
    Function to remove UTHits used by Long tracks and create a new UTHits container for the residual UTHits, which are later used as input to PrLongLivedTracking or PrForwardTracking
    """
    return PrResidualUTHits(
        TracksLocation=match_tracks["Pr"],
        UTHitsLocation=make_ut_hits()).UTHitsOutput


@configurable
def make_ReducePrUTHits_fromMatch(input_tracks,
                                  make_ut_hits=make_PrStorePrUTHits_hits):
    """
    Function to remove PrUTHits used by Long tracks and create a new PrUTHits container for the residual PrUTHits, which are later used as input to PrLongLivedTracking or PrForwardTracking
    """
    return PrResidualPrUTHits(
        TracksLocation=input_tracks["Pr"],
        PrUTHitsLocation=make_ut_hits()).PrUTHitsOutput


@configurable
def get_residual_ut_hits_tool(match_tracks,
                              ut_hits_tool=PrAddUTHitsTool,
                              make_ut_hits=make_ReducePrUTHits_fromMatch):
    return ut_hits_tool(UTHitsLocation=make_ut_hits(match_tracks))


def get_fast_hlt2_tracks():
    """Function to get fast set of tracks reconstructed in HLT2, where the PrForwardTracking uses the residual VeloTracks and SciFi hits from ``'Match'`` tracks, the PrLonglivedTracking uses the residual SeedTracks and UTHits from ``'Match'`` tracks. This is expected to a significant speedup for track reconstruction with a moderate loss of efficiency.

    Returns:
        A dict mapping all types of velo, upstream, HLT1 forward fitted, HLT2 forward, SciFi seeding, downstream and matched long tracks to ``'Velo'``, ``'Upstream'``, ``'ForwardFastFitted'``, ``'Forward'``, ``'Seed'``, ``'Downstream'`` and ``'Match'``` respectively.
    """
    hlt1_tracks = make_hlt1_tracks()
    fitted_hlt1_tracks = make_hlt1_fitted_tracks(hlt1_tracks)
    scifi_tracks = make_PrHybridSeeding_tracks()
    match_tracks = make_PrMatchNN_tracks(hlt1_tracks["Velo"], scifi_tracks)
    residual_velo = make_ReduceVeloTracks_fromMatch(match_tracks,
                                                    hlt1_tracks["Velo"])

    make_residual_fthits = partial(make_ReduceSciFiHits_fromMatch,
                                   match_tracks)
    make_residual_pruthits = partial(get_residual_ut_hits_tool, match_tracks)
    make_residual_forward = partial(
        make_PrForwardTrackingVelo_tracks,
        make_ft_hits=make_residual_fthits,
        ut_hits_tool=make_residual_pruthits)
    hlt2_forward_tracks = all_hlt2_forward_track_types(
        residual_velo, make_forward_tracks=make_residual_forward)
    scifi_tracks_reduce = make_ReduceSeedTracks_fromMatch(
        match_tracks, scifi_tracks)
    make_residual_uthits = partial(make_ReducePrUTHits_fromMatch, match_tracks)
    downstream_tracks = make_PrLongLivedTracking_tracks(
        scifi_tracks_reduce, make_ut_hits=make_residual_uthits)

    return {
        "Velo": hlt1_tracks["Velo"],
        "Upstream": hlt1_tracks["Upstream"],
        "ForwardFastFitted": fitted_hlt1_tracks,
        "Forward": hlt2_forward_tracks,
        "Seed": scifi_tracks,
        "Downstream": downstream_tracks,
        "Match": match_tracks,
        "UTHitsRes": make_residual_uthits(),
        "FTHitsRes": make_residual_fthits(),
    }
