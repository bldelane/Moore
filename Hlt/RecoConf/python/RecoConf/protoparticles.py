###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions to create charged and neutral protoparticles from reconstruction output."""
from PyConf import configurable
from PyConf.Algorithms import (
    FunctionalChargedProtoParticleMaker,
    FutureNeutralProtoPAlg,
)
from PyConf.Tools import (
    LoKi__Hybrid__TrackSelector as TrackSelector,
    CaloFutureElectron,
    CaloFutureHypoEstimator,
    CaloFutureHypo2CaloFuture,
    FutureGammaPi0XGBoostTool,
    FutureGammaPi0SeparationTool,
    FutureNeutralIDTool,
    ChargedProtoParticleAddEcalInfo,
    ChargedProtoParticleAddHcalInfo,
    ChargedProtoParticleAddBremInfo,
    ChargedProtoParticleAddRichInfo,
    ChargedProtoParticleAddMuonInfo,
    ChargedProtoParticleAddCombineDLLs,
    ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo,
)


def hypo_estimator(calo_pids):
    return CaloFutureHypoEstimator(
        ClusterMatchLocation=calo_pids["clusterMatch"],
        ElectronMatchLocation=calo_pids["electronMatch"],
        BremMatchLocation=calo_pids["bremMatch"],
        Hypo2Calo=CaloFutureHypo2CaloFuture(
            EcalDigitsLocation=calo_pids["digitsEcal"],
            HcalDigitsLocation=calo_pids["digitsHcal"]),
        Pi0Separation=FutureGammaPi0SeparationTool(),
        Pi0SeparationXGB=FutureGammaPi0XGBoostTool(
            DigitLocation=calo_pids["digitsEcal"]),
        NeutralID=FutureNeutralIDTool(),
        Electron=CaloFutureElectron(),
    )


@configurable
def make_neutral_protoparticles(calo_pids):
    """Create neutral ProtoParticles from Calorimeter reconstruction output.

    Args:
        calo_pids: dictionary containing all necessary inputs to create neutral ProtoParticles.

    Returns:
        DataHandle to the container of neutral ProtoParticles

    """
    neutral_protos = FutureNeutralProtoPAlg(
        CaloHypoEstimator=hypo_estimator(calo_pids),
        MergedPi0s=calo_pids["mergedPi0s"],
        Photons=calo_pids["photons"],
        SplitPhotons=calo_pids["splitPhotons"],
    )

    return neutral_protos.ProtoParticleLocation


@configurable
def make_charged_protoparticles(tracks,
                                rich_pids,
                                calo_pids,
                                muon_pids,
                                track_type=None,
                                enable_muon_id=False):
    """Create charged ProtoParticles from tracking and particle identification reconstruction outputs.

    Args:
        tracks: input tracks to create charged protos
        rich_pids: input rich_pids created from tracks
        calo_pids: input rich_pids from Calorimeter reconstruction
        muon_pids: input muon_pids created from tracks
        enable_muon_id: Running the MuonID is currently not thread-safe therefore the default is False but the option exists to run it in single threaded mode.

    Returns:
        dict with DataHandle to the container of charged ProtoParticles and the sequence of algorithms to create them.

    """

    addInfo = [
        ChargedProtoParticleAddEcalInfo(
            name='addEcalInfo',
            CaloFutureElectron=CaloFutureElectron(),
            CaloHypoEstimator=hypo_estimator(calo_pids),
            InputInEcalLocation=calo_pids["inEcal"],
            InputElectronMatchLocation=calo_pids["electronMatch"],
            InputEcalChi2Location=calo_pids["ecalChi2"],
            InputEcalELocation=calo_pids["ecalE"],
            InputClusterChi2Location=calo_pids["clusChi2"],
            InputEcalPIDeLocation=calo_pids["ecalPIDe"],
            InputEcalPIDmuLocation=calo_pids["ecalPIDmu"],
            InputClusterMatchLocation=calo_pids["clusterMatch"],
        ),
        ChargedProtoParticleAddHcalInfo(
            name='addHcalInfo',
            InputInHcalLocation=calo_pids["inHcal"],
            InputHcalELocation=calo_pids["hcalE"],
            InputHcalPIDeLocation=calo_pids["hcalPIDe"],
            InputHcalPIDmuLocation=calo_pids["hcalPIDmu"],
        ),
        ChargedProtoParticleAddBremInfo(
            name='addBremInfo',
            CaloHypoEstimator=hypo_estimator(calo_pids),
            InputInBremLocation=calo_pids["inBrem"],
            InputBremMatchLocation=calo_pids["bremMatch"],
            InputBremChi2Location=calo_pids["bremChi2"],
            InputBremPIDeLocation=calo_pids["bremPIDe"],
        ),
        ChargedProtoParticleAddRichInfo(
            name='addRichInfo', InputRichPIDLocation=rich_pids)
    ]

    # MuonChi2MatchTool is not thread-safe, so we cannot run this by default at the moment.
    if enable_muon_id:
        addInfo.append(
            ChargedProtoParticleAddMuonInfo(
                name='addMuonInfo', InputMuonPIDLocation=muon_pids))

    addInfo.append(ChargedProtoParticleAddCombineDLLs())

    # Set up ann pids
    if track_type is None:
        track_types = ["Long", "Downstream", "Upstream"]
    else:
        track_types = [track_type]

    addInfo += [
        ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo(
            name="AddANNPID" + TrackType + PIDType,
            NetworkVersion="MCUpTuneV1",
            PIDType=PIDType,
            TrackType=TrackType,
        ) for TrackType in track_types
        for PIDType in ["Electron", "Muon", "Pion", "Kaon", "Proton", "Ghost"]
    ]

    if track_type is None:
        charged_protos = FunctionalChargedProtoParticleMaker(
            Inputs=[tracks["v1"]],
            TrackSelector=TrackSelector(
                Code="TrLONG | TrDOWNSTREAM | TrUPSTREAM"),
            AddInfo=addInfo)
    else:
        charged_protos = FunctionalChargedProtoParticleMaker(
            Inputs=[tracks["v1"]], AddInfo=addInfo)

    return charged_protos.Output
