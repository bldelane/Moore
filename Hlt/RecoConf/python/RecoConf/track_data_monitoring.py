###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import (
    TrackVertexMonitor, TrackFitMatchMonitor, AlignmentOnlineMonitor,
    TrackMonitor, TrackPV2HalfAlignMonitor, DeterministicPrescaler)

from PyConf import configurable
from PyConf.application import make_odin

from PyConf.control_flow import CompositeNode, NodeLogic


@configurable
def monitor_tracking(tracks, pvs, prescale=1.0):
    prescaler = DeterministicPrescaler(
        name="TrackMonitoringPrescaler",
        AcceptFraction=prescale,
        SeedName="TrackMonitoringPrescaler",
        ODINLocation=make_odin())

    vertex_moni = TrackVertexMonitor(
        name="TrackVertexMonitor", PVContainer=pvs, TrackContainer=tracks)

    align_online_moni = AlignmentOnlineMonitor(
        name="AlignmentOnlineMonitor", TrackLocation=tracks)

    fitmatch_moni = TrackFitMatchMonitor(
        name="TrackFitMatchMonitor", TrackContainer=tracks)

    track_moni = TrackMonitor(
        name="TrackMonitor", PrimaryVertices=pvs, TracksInContainer=tracks)

    pv2half_moni = TrackPV2HalfAlignMonitor(
        name="TrackPV2HalfAlignMonitor",
        TrackContainer=tracks,
        ODINLocation=make_odin())

    return CompositeNode(
        "Monitoring", [
            prescaler, vertex_moni, align_online_moni, fitmatch_moni,
            track_moni, pv2half_moni
        ],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)
