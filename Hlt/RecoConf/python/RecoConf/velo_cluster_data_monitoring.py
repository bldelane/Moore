###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import default_raw_event, make_data_with_FetchDataFromFile
from PyConf.Algorithms import VPClusterEfficiency
from Hlt2Conf.data_from_file import boole_links_digits_mchits, mc_unpackers
from RecoConf.hlt1_tracking import make_velo_full_clusters


def monitor_velo_clusters(make_raw=default_raw_event,
                          full_cluster_location=make_velo_full_clusters):

    return VPClusterEfficiency(
        RawEventLocation=make_raw(["VP"]),
        VPClusterLocation=full_cluster_location(),
        MCHitLocation=mc_unpackers()["MCVPHits"],
        MCParticleLocation=mc_unpackers()["MCParticles"],
        VPDigit2MCHitLinksLocation=boole_links_digits_mchits()["VPDigits"],
        MCProperty=make_data_with_FetchDataFromFile("/Event/MC/TrackInfo"))
