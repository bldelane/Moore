###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV


def monitor_calo_clusters(calo):
    from PyConf.Algorithms import CaloFutureClusterMonitor
    ecal_clus_moni = CaloFutureClusterMonitor(
        name="ClusterMonitorEcalClusters",
        Input=calo["ecalClusters"],
        HistoMultiplicityMax=400,
        HistoEnergyMax=50. * GeV,
        HistoEtMax=5. * GeV,
        HistoSizeMax=30,
    )
    ecal_split_clus_moni = CaloFutureClusterMonitor(
        name="ClusterMonitorEcalSplitClusters",
        Input=calo["ecalSplitClusters"],
        HistoMultiplicityMax=50,
        HistoEnergyMax=100. * GeV,
        HistoEtMax=10. * GeV,
    )
    return ecal_clus_moni, ecal_split_clus_moni
