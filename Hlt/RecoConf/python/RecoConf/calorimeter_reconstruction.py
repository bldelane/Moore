###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import (
    CaloFutureRawToDigits, FutureCellularAutomatonAlg, CaloFutureShowerOverlap,
    CaloFutureClusterCovarianceAlg, InEcalFutureAcceptanceAlg,
    FuturePhotonMatchAlg, SelectiveTrackMatchAlg, ClassifyPhotonElectronAlg,
    CaloFutureMergedPi0, InHcalFutureAcceptanceAlg,
    FutureInBremFutureAcceptanceAlg, SelectiveElectronMatchAlg,
    FutureElectronMatchAlg, BremMatchAlgFuture, FutureTrack2EcalEAlg,
    FutureTrack2HcalEAlg, FutureClusChi22ID, FutureEcalChi22ID,
    BremChi22IDFuture, FutureEcalPIDeAlg, BremPIDeAlgFuture, FutureHcalPIDeAlg,
    FutureEcalPIDmuAlg, FutureHcalPIDmuAlg,
    LHCb__Calo__Asssociators__Hypo2Tracks as CaloHypo2Tracks,
    LHCb__Converters__Calo__Cluster__v1__fromV2 as ClusterConverter,
    LHCb__Converters__Calo__Hypo__v1__fromV2 as HypoConverter,
    LHCb__Converters__Calo__Hypo__v1__MergedPi0__fromV2 as MergedPi0Converter,
    LHCb__Converters__Calo__Hypo2TrackTable__v1__fromV2 as
    Hypo2TrackTableConverter)

from PyConf.Tools import (CaloFutureECorrection, CaloFutureSCorrection,
                          CaloFutureLCorrection, CaloFutureDigitFilterTool)
from PyConf.application import default_raw_event


def make_ecal_digits(raw):
    adc_alg = CaloFutureRawToDigits(
        name='FutureEcalZSup',
        RawEventLocation=raw,
        DetectorLocation='/dd/Structure/LHCb/DownstreamRegion/Ecal')
    return adc_alg.OutputDigitData


def make_hcal_digits(raw):
    adc_alg_Hcal = CaloFutureRawToDigits(
        name='FutureHcalZSup',
        RawEventLocation=raw,
        DetectorLocation='/dd/Structure/LHCb/DownstreamRegion/Hcal')
    return adc_alg_Hcal.OutputDigitData


def make_digits(raw):
    digitsEcal = make_ecal_digits(raw)
    digitsHcal = make_hcal_digits(raw)

    return {"digitsEcal": digitsEcal, "digitsHcal": digitsHcal}


def make_clusters(digits):
    ecalClustersRaw = FutureCellularAutomatonAlg(InputData=digits).OutputData

    ecalClustersOverlap = CaloFutureShowerOverlap(
        InputData=ecalClustersRaw, ApplyLSCorr=False).OutputData

    ecalClusters = CaloFutureClusterCovarianceAlg(
        InputData=ecalClustersOverlap).OutputData

    return ecalClusters


def make_clusters_various(digits):
    """Make various types of clusters for resolution tests.

    See Also:
        `make_calo_resolution_gamma`

    """
    # no overlap
    ecalClustersRaw = FutureCellularAutomatonAlg(InputData=digits).OutputData
    # running CaloFutureShowerOverlap with niter=0 is currently needed to produce "old" clusters
    # (output of ShowerOverlap, input for CaloFutureClusterCovarianceAlg and further algorithms)
    # from "future" clusters (output of CellularAutomaton, input for ShowerOverlap),
    # i.e. CaloFutureShowerOverlap acts just as transformer "old" clusters -> "new" clusters
    # Eventually all clusters will be "future" and this step should be dropped
    # NB Iterations=0 does not fully disable the shower overlap algorithm, there is
    # sort of 0th iteration performed anyway
    ecalClustersRawNoOverlap = CaloFutureShowerOverlap(
        InputData=ecalClustersRaw,
        Iterations=0,
        ApplyLSCorr=False,
        name='CaloFutureShowerOverlapNoOverlap').OutputData
    ecalClustersNoOverlapWithCovar = CaloFutureClusterCovarianceAlg(
        InputData=ecalClustersRawNoOverlap,
        name='CaloFutureClusterCovarianceAlgNoOverlap').OutputData

    # overlap default
    ecalClustersOverlap = CaloFutureShowerOverlap(
        InputData=ecalClustersRaw).OutputData
    ecalClustersOverlapWithCovar = CaloFutureClusterCovarianceAlg(
        InputData=ecalClustersOverlap).OutputData

    # overlap fast (with stopping criteria)
    ecalClustersOverlapFast = CaloFutureShowerOverlap(
        InputData=ecalClustersRaw,
        Iterations=-5,
        name='CaloFutureShowerOverlapFast').OutputData
    ecalClustersOverlapFastWithCovar = CaloFutureClusterCovarianceAlg(
        InputData=ecalClustersOverlapFast,
        name='CaloFutureClusterCovarianceAlgOverlapFast').OutputData

    # overlap without S, L corrections (and also with stopping criteria)
    ecalClustersOverlapNoCor = CaloFutureShowerOverlap(
        InputData=ecalClustersRaw,
        Iterations=-5,
        ApplyLSCorr=False,
        name='CaloFutureShowerOverlapNoCor').OutputData
    ecalClustersOverlapNoCorWithCovar = CaloFutureClusterCovarianceAlg(
        InputData=ecalClustersOverlapNoCor,
        name='CaloFutureClusterCovarianceAlgOverlapNoCor').OutputData

    return {
        "ecalClustersNoOverlap": ecalClustersRaw,
        "ecalClustersNoOverlapWithCovar": ecalClustersNoOverlapWithCovar,
        "ecalClustersOverlap": ecalClustersOverlap,
        "ecalClustersOverlapWithCovar": ecalClustersOverlapWithCovar,
        "ecalClustersOverlapFast": ecalClustersOverlapFast,
        "ecalClustersOverlapFastWithCovar": ecalClustersOverlapFastWithCovar,
        "ecalClustersOverlapNoCor": ecalClustersOverlapNoCor,
        "ecalClustersOverlapNoCorWithCovar": ecalClustersOverlapNoCorWithCovar,
    }


def make_raw_clusters(digits):
    return FutureCellularAutomatonAlg(InputData=digits).OutputData


@configurable
def make_photons_and_electrons(tracks, clusters, pvs,
                               selective_matching=False):
    inECALFuture = InEcalFutureAcceptanceAlg(Inputs=tracks).Output

    if selective_matching:
        clusterMatch = SelectiveTrackMatchAlg(
            InputClusters=clusters,
            InputTracks=tracks,
            Filter=inECALFuture,
            nNeighborSquares=1,
            useSpread=False).Output
        PhotonMinChi2 = 10.
        ElectrMaxChi2 = 50.
    else:
        clusterMatch = FuturePhotonMatchAlg(
            Calos=clusters, Tracks=tracks, Filter=inECALFuture).Output
        PhotonMinChi2 = 4.
        ElectrMaxChi2 = 25.

    photonElectronAlg = ClassifyPhotonElectronAlg(
        InputTable=clusterMatch,
        InputClusters=clusters,
        ElectrMaxChi2=ElectrMaxChi2,
        ElectrMinEt=50.0,
        MinDigits=2,
        PhotonMinChi2=PhotonMinChi2,
        PhotonMinEt=50.0,
        PhotonCorrection=[
            CaloFutureECorrection(
                FilterTool=CaloFutureDigitFilterTool(PrimaryVertices=pvs)),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ],
        ElectronCorrection=[
            CaloFutureECorrection(
                FilterTool=CaloFutureDigitFilterTool(PrimaryVertices=pvs)),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ])
    photons = photonElectronAlg.OutputPhotons
    electrons = photonElectronAlg.OutputElectrons

    calo2Track = CaloHypo2Tracks(
        InputTable=clusterMatch,
        InputHypos=photonElectronAlg.OutputElectrons,
        ElectrMaxChi2=ElectrMaxChi2)

    return {
        "inECAL": inECALFuture,
        "clusterMatch": clusterMatch,
        "photons": photons,
        "electrons": electrons,
        "electronTracks": calo2Track.OutputTracks,
    }


def make_acceptance(tracks):
    return {
        "inAccHcal": InHcalFutureAcceptanceAlg(Inputs=tracks).Output,
        "inAccEcal": InEcalFutureAcceptanceAlg(Inputs=tracks).Output,
        "inAccBrem": FutureInBremFutureAcceptanceAlg(Inputs=tracks).Output
    }


def make_track_energy_in_calo(tracks, digitsEcal, digitsHcal, inAccEcal,
                              inAccHcal):
    ecalE = FutureTrack2EcalEAlg(
        Inputs=tracks, Filter=inAccEcal, Digits=digitsEcal).Output

    hcalE = FutureTrack2HcalEAlg(
        Inputs=tracks, Filter=inAccHcal, Digits=digitsHcal).Output

    return {"ecalE": ecalE, "hcalE": hcalE}


def make_merged_pi0(ecalClusters, pvs):
    mergedPi0 = CaloFutureMergedPi0(
        InputData=ecalClusters,
        EtCut=1500.0,
        PhotonTools=[
            CaloFutureECorrection(
                FilterTool=CaloFutureDigitFilterTool(PrimaryVertices=pvs)),
            CaloFutureSCorrection(),
            CaloFutureLCorrection()
        ])
    return {
        "ecalSplitClusters": mergedPi0.SplitClusters,
        "mergedPi0s": mergedPi0.MergedPi0s,
    }


def make_merged_pi0_various(ecalClusters, pvs):
    """Make various types of pi0 for resolution tests.

    See Also:
        `make_calo_resolution_pi0`

    """
    corrections = [
        CaloFutureECorrection(
            FilterTool=CaloFutureDigitFilterTool(PrimaryVertices=pvs)),
        CaloFutureSCorrection(),
        CaloFutureLCorrection()
    ]

    # default
    mergedPi0 = CaloFutureMergedPi0(
        InputData=ecalClusters, EtCut=1500.0, PhotonTools=corrections)

    # no overlap
    mergedPi0NoOverlap = CaloFutureMergedPi0(
        InputData=ecalClusters,
        EtCut=1500.0,
        MaxIterations=0,
        PhotonTools=corrections)

    # overlap fast (with stopping criteria)
    mergedPi0OverlapFast = CaloFutureMergedPi0(
        InputData=ecalClusters,
        EtCut=1500.0,
        MaxIterations=-25,
        PhotonTools=corrections)

    # overlap without S, L corrections (and also with stopping criteria)
    mergedPi0OverlapNoCor = CaloFutureMergedPi0(
        InputData=ecalClusters,
        EtCut=1500.0,
        ApplyLSCorr=False,
        MaxIterations=-25,
        PhotonTools=corrections)

    return {
        #"ecalSplitClusters": mergedPi0.SplitClusters,
        "mergedPi0s": mergedPi0.MergedPi0s,
        "mergedPi0sNoOverlap": mergedPi0NoOverlap.MergedPi0s,
        "mergedPi0sOverlapFast": mergedPi0OverlapFast.MergedPi0s,
        "mergedPi0sOverlapNoCor": mergedPi0OverlapNoCor.MergedPi0s,
        #"splitPhotons": mergedPi0.SplitPhotons
    }


@configurable
def make_electron_and_brem_match(electrons,
                                 photons,
                                 tracks,
                                 clusterMatch,
                                 inECAL,
                                 inAccBrem,
                                 selective_matching=False):
    if selective_matching:
        electronMatch = SelectiveElectronMatchAlg(
            InputHypos=electrons,
            InputTracks=tracks,
            InputTable=clusterMatch,
            AddEnergy=True,
            OneSidedE=True)
    else:
        electronMatch = FutureElectronMatchAlg(
            Calos=electrons, Tracks=tracks, Filter=inECAL)

    bremMatch = BremMatchAlgFuture(
        Calos=photons, Tracks=tracks, Filter=inAccBrem)

    return {"electronMatch": electronMatch, "bremMatch": bremMatch}


def make_chi2_and_pid(tracks, clusterMatch, electronMatch, bremMatch, ecalE,
                      hcalE):
    clusChi2 = FutureClusChi22ID(
        Tracks=tracks,
        Input=clusterMatch,
    ).Output

    ecalChi2 = FutureEcalChi22ID(
        Tracks=tracks,
        Input=electronMatch,
    )

    bremChi2 = BremChi22IDFuture(
        Tracks=tracks,
        Input=bremMatch,
    )

    ecalPIDe = FutureEcalPIDeAlg(Input=ecalChi2).Output

    bremPIDe = BremPIDeAlgFuture(Input=bremChi2).Output

    hcalPIDe = FutureHcalPIDeAlg(Input=hcalE).Output

    ecalPIDmu = FutureEcalPIDmuAlg(Input=ecalE).Output

    hcalPIDmu = FutureHcalPIDmuAlg(Input=hcalE).Output

    return {
        "ecalChi2": ecalChi2,
        "clusChi2": clusChi2,
        "bremChi2": bremChi2,
        "ecalPIDe": ecalPIDe,
        "bremPIDe": bremPIDe,
        "hcalPIDe": hcalPIDe,
        "ecalPIDmu": ecalPIDmu,
        "hcalPIDmu": hcalPIDmu,
    }


def make_calo(best_tracks, pvs, make_raw=default_raw_event):
    rawEvent = make_raw(["HcalPacked", "EcalPacked"])
    rawToDigitsOutput = make_digits(rawEvent)
    digitsEcal = rawToDigitsOutput["digitsEcal"]
    digitsHcal = rawToDigitsOutput["digitsHcal"]

    ecalClusters = make_clusters(digitsEcal)
    PhElOutput = make_photons_and_electrons(best_tracks, ecalClusters, pvs)
    photons = PhElOutput["photons"]
    electrons = PhElOutput["electrons"]
    inECAL = PhElOutput["inECAL"]
    clusterMatch = PhElOutput["clusterMatch"]
    electronTracks = PhElOutput["electronTracks"]

    mergePi0Out = make_merged_pi0(ecalClusters, pvs)
    ecalSplitClusters = mergePi0Out["ecalSplitClusters"]
    mergedPi0s = mergePi0Out["mergedPi0s"]

    acc = make_acceptance(best_tracks)
    track2CaloEOut = make_track_energy_in_calo(best_tracks, digitsEcal,
                                               digitsHcal, acc["inAccEcal"],
                                               acc["inAccHcal"])
    ecalE = track2CaloEOut["ecalE"]
    hcalE = track2CaloEOut["hcalE"]

    matchAlgOut = make_electron_and_brem_match(electrons, photons, best_tracks,
                                               clusterMatch, inECAL,
                                               acc["inAccBrem"])
    electronMatch = matchAlgOut["electronMatch"]
    bremMatch = matchAlgOut["bremMatch"]

    chi2PidOut = make_chi2_and_pid(best_tracks, clusterMatch, electronMatch,
                                   bremMatch, ecalE, hcalE)
    ecalChi2 = chi2PidOut["ecalChi2"]
    ecalPIDe = chi2PidOut["ecalPIDe"]
    bremPIDe = chi2PidOut["bremPIDe"]
    hcalPIDe = chi2PidOut["hcalPIDe"]
    ecalPIDmu = chi2PidOut["ecalPIDmu"]
    hcalPIDmu = chi2PidOut["hcalPIDmu"]
    clusChi2 = chi2PidOut["clusChi2"]
    bremChi2 = chi2PidOut["bremChi2"]

    old_ecalClusters = ClusterConverter(
        InputClusters=ecalClusters).OutputClusters
    old_photons = HypoConverter(
        InputHypos=photons, InputClusters=old_ecalClusters).OutputHypos
    old_electrons = HypoConverter(
        InputHypos=electrons, InputClusters=old_ecalClusters).OutputHypos
    old_electronMatch = Hypo2TrackTableConverter(
        InputTable=electronMatch, InputHypotheses=old_electrons).OutputTable
    old_bremMatch = Hypo2TrackTableConverter(
        InputTable=bremMatch, InputHypotheses=old_photons).OutputTable
    old_ecalSplitClusters = ClusterConverter(
        InputClusters=ecalSplitClusters).OutputClusters

    convert_mergedpi0 = MergedPi0Converter(
        InputClusters=old_ecalClusters,
        InputSplitClusters=old_ecalSplitClusters,
        InputHypos=mergedPi0s)
    old_mergedPi0s = convert_mergedpi0.OutputHypos
    old_splitPhotons = convert_mergedpi0.OutputSplitPhotons

    return {
        "digitsEcal": digitsEcal,
        "digitsHcal": digitsHcal,
        "ecalClusters": ecalClusters,
        "ecalSplitClusters": ecalSplitClusters,
        "inEcal": acc["inAccEcal"],
        "inHcal": acc["inAccHcal"],
        "inBrem": acc["inAccBrem"],
        "ecalChi2": ecalChi2,
        "ecalE": ecalE,
        "hcalE": hcalE,
        "ecalPIDmu": ecalPIDmu,
        "hcalPIDe": hcalPIDe,
        "hcalPIDmu": hcalPIDmu,
        "ecalPIDe": ecalPIDe,
        "bremPIDe": bremPIDe,
        "bremChi2": bremChi2,
        "clusChi2": clusChi2,
        "electronTracks": electronTracks,
        "clusterMatch": clusterMatch,
        "electronMatch": old_electronMatch,
        "bremMatch": old_bremMatch,
        "photons": old_photons,
        "electrons": old_electrons,
        "mergedPi0s": old_mergedPi0s,
        "splitPhotons": old_splitPhotons,
    }


def make_calo_reduced(make_raw=default_raw_event):
    rawEvent = make_raw(["HcalPacked", "EcalPacked"])
    rawToDigitsOutput = make_digits(rawEvent)
    digitsEcal = rawToDigitsOutput["digitsEcal"]
    digitsHcal = rawToDigitsOutput["digitsHcal"]
    clusters = make_clusters(digitsEcal)

    return {
        "digitsEcal": digitsEcal,
        "ecalClusters": clusters,
        "digitsHcal": digitsHcal,
    }


def make_calo_resolution_gamma(best_tracks, pvs, make_raw=default_raw_event):
    rawEvent = make_raw(["HcalPacked", "EcalPacked"])
    rawToDigitsOutput = make_digits(rawEvent)
    digitsEcal = rawToDigitsOutput["digitsEcal"]
    clusters = make_clusters_various(digitsEcal)

    # default
    PhElOutput = make_photons_and_electrons(
        best_tracks, clusters["ecalClustersOverlapWithCovar"], pvs)
    photons = PhElOutput["photons"]
    electrons = PhElOutput["electrons"]

    # overlap fast
    PhElOutputOverlapFast = make_photons_and_electrons(
        best_tracks, clusters["ecalClustersOverlapFastWithCovar"], pvs)
    photonsOverlapFast = PhElOutputOverlapFast["photons"]

    # overlap no corrections
    PhElOutputOverlapNoCor = make_photons_and_electrons(
        best_tracks, clusters["ecalClustersOverlapNoCorWithCovar"], pvs)
    photonsOverlapNoCor = PhElOutputOverlapNoCor["photons"]

    # no overlap
    PhElOutputNoOverlap = make_photons_and_electrons(
        # after full migration to "future" clusters use the next line
        #best_tracks, clusters["ecalClustersNoOverlap"])
        best_tracks,
        clusters["ecalClustersNoOverlapWithCovar"],
        pvs)
    photonsNoOverlap = PhElOutputNoOverlap["photons"]

    return {
        "clusters": clusters,
        "digitsEcal": digitsEcal,
        "photons": photons,
        "photonsOverlapFast": photonsOverlapFast,
        "photonsOverlapNoCor": photonsOverlapNoCor,
        "photonsNoOverlap": photonsNoOverlap,
        "electrons": electrons,
    }


def make_calo_resolution_pi0(best_tracks, pvs, make_raw=default_raw_event):
    rawEvent = make_raw(["HcalPacked", "EcalPacked"])
    rawToDigitsOutput = make_digits(rawEvent)
    #adcEcal = rawToDigitsOutput["adcEcal"]
    digitsEcal = rawToDigitsOutput["digitsEcal"]
    #adcHcal = rawToDigitsOutput["adcHcal"]
    #digitsHcal = rawToDigitsOutput["digitsHcal"]
    clusters = make_clusters_various(digitsEcal)

    # default
    pi0 = make_merged_pi0(clusters["ecalClustersOverlapWithCovar"],
                          pvs)["mergedPi0s"]

    # overlap fast
    pi0OverlapFast = make_merged_pi0(
        clusters["ecalClustersOverlapFastWithCovar"], pvs)["mergedPi0s"]

    # overlap no corrections
    pi0OverlapNoCor = make_merged_pi0_various(
        clusters["ecalClustersOverlapNoCorWithCovar"], pvs)

    # no overlap
    pi0NoOverlap = make_merged_pi0_various(
        clusters["ecalClustersNoOverlapWithCovar"], pvs)

    # no corrections
    pi0NoCor = CaloFutureMergedPi0(
        InputData=clusters["ecalClustersOverlapWithCovar"],
        EtCut=1500.0,
        PhotonTools=[]).MergedPi0s

    return {
        "clusters": clusters,
        "digitsEcal": digitsEcal,
        "clusDef-pi0Def": pi0,
        "clusSOFast-pi0Def": pi0OverlapFast,
        "clusSONoCor-pi0Def": pi0OverlapNoCor["mergedPi0s"],
        "clusSONoCor-pi0NoSO": pi0OverlapNoCor["mergedPi0sNoOverlap"],
        "clusSONoCor-pi0SOFast": pi0OverlapNoCor["mergedPi0sOverlapFast"],
        "clusSONoCor-pi0SONoCor": pi0OverlapNoCor["mergedPi0sOverlapNoCor"],
        "clusNoSO-pi0Def": pi0NoOverlap["mergedPi0s"],
        "clusNoSO-pi0NoSO": pi0NoOverlap["mergedPi0sNoOverlap"],
        "clusDef-pi0NoCor": pi0NoCor,
    }


def make_calo_raw_ecalclusters(make_raw=default_raw_event):
    # create ecal raw event
    ecalrawEvent = make_raw(["EcalPacked"])
    ecaldigits = make_ecal_digits(ecalrawEvent)
    # create raw ECAL clusters running CellularAutomaton only
    # returns CaloClusters v2
    clusters = make_raw_clusters(ecaldigits)
    return {"ecaldigits": ecaldigits, "ecalclusters": clusters}
