###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Module to allow switching between prompt reconstruction and reconstruction from file.

The user can switch between the two options in a top level configuration file
by binding the argument from_file of the function reconstruction.

"""

from PyConf import configurable
from RecoConf.reco_objects_from_file import (
    reconstruction as reconstruction_from_file,
    upfront_reconstruction as upfront_reconstruction_from_file,
)
from RecoConf.reco_objects_for_spruce import (
    reconstruction as reconstruction_for_spruce,
    upfront_reconstruction as upfront_reconstruction_for_spruce,
)

from RecoConf.hlt2_global_reco import reconstruction as reconstruction_from_reco


@configurable
def reconstruction(from_file=True, spruce=False):
    """Return reconstruction objects.

    Note it is advised to use this function if more than one object is needed,
    rather than the accessors below as it makes the configuration slower.
    """
    if spruce:
        assert from_file, 'For sprucing, from_file must be set to True (default value)'
    if from_file:
        if spruce:
            reco = reconstruction_for_spruce()
            upfront_reconstruction = upfront_reconstruction_for_spruce()
        else:
            reco = reconstruction_from_file()
            upfront_reconstruction = upfront_reconstruction_from_file()
    else:
        reco = reconstruction_from_reco()
        upfront_reconstruction = reco["UpfrontReconstruction"]

    charged_protos = reco["ChargedProtos"]
    neutral_protos = reco["NeutralProtos"]
    best_tracks = reco["Tracks"]
    pvs = reco["PVs"]
    electrons = reco["CaloElectrons"]
    photons = reco["CaloPhotons"]
    mergedPi0s = reco["CaloMergedPi0s"]
    splitPhotons = reco["CaloSplitPhotons"]
    muon_pids = reco["MuonPIDs"]
    rich_pids = reco["RichPIDs"]

    return {
        "ChargedProtos": charged_protos,
        "NeutralProtos": neutral_protos,
        "Tracks": best_tracks,
        "PVs": pvs,
        "UpfrontReconstruction": upfront_reconstruction,
        "CaloElectrons": electrons,
        "CaloPhotons": photons,
        "CaloMergedPi0s": mergedPi0s,
        "CaloSplitPhotons": splitPhotons,
        "MuonPIDs": muon_pids,
        "RichPIDs": rich_pids,
    }


def upfront_reconstruction():
    """Return sequence to create charged ProtoParticles.

    """
    return reconstruction()["UpfrontReconstruction"]


def make_charged_protoparticles():
    """Return a DataHandle to the container of charged ProtoParticles.

    """
    return reconstruction()["ChargedProtos"]


def make_neutral_protoparticles():
    """Return a DataHandle to the container of neutral ProtoParticles.

    """
    return reconstruction()["NeutralProtos"]


def make_pvs():
    """Return a DataHandle to the container of PVs

    """
    return reconstruction()["PVs"]


def make_tracks():
    """Return a DataHandle to the container of all tracks

    """
    return reconstruction()["Tracks"]
