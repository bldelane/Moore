###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import HltPackedDataDecoder
from PyConf.application import default_raw_event
from GaudiConf.PersistRecoConf import PersistRecoPacking
from Gaudi.Configuration import VERBOSE
from Configurables import ApplicationMgr
from PyConf.components import setup_component


def upfront_reconstruction():
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be ran before all HLT2 lines.

    """

    prpacking = PersistRecoPacking(stream='/Event/HLT2', data_type='Upgrade')
    unpackers = prpacking.unpackers()
    container_map = prpacking.packedToOutputLocationMap()
    bank_location = default_raw_event(["DstData"]).location

    HltReadingSvc = setup_component(
        "HltANNSvc", instance_name="HltANNSvcReading", OutputLevel=1)

    if HltReadingSvc.getFullName() not in ApplicationMgr().ExtSvc:
        ApplicationMgr().ExtSvc += [HltReadingSvc.getFullName()]

    decoder = HltPackedDataDecoder(
        ANNSvc="HltANNSvcReading",
        RawEventLocations=[bank_location],
        OutputLevel=VERBOSE,
        ContainerMap=container_map,
    )

    algs = list(unpackers)
    algs.insert(0, decoder)
    return algs


def reconstruction():
    """Return a {name: DataHandle} dict that define the reconstruction output."""

    prpacking = PersistRecoPacking(stream='/Event/HLT2', data_type='Upgrade')
    unpackers = prpacking.unpackers_by_key()
    map = {k: v.OutputName for k, v in unpackers.items()}
    return map


def make_charged_protoparticles():
    return reconstruction()['ChargedProtos']


def make_neutral_protoparticles():
    return reconstruction()['NeutralProtos']


def make_pvs():
    return reconstruction()['PVs']


def make_tracks():
    return reconstruction()['Tracks']
