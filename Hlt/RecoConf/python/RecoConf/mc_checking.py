###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
'''
In this file, algorithms needed to run the MC reconstruction checking (PrTrackChecker) are defined.
'''
from PyConf.tonic import configurable
from PyConf.dataflow import DataHandle

from PyConf.application import (
    make_data_with_FetchDataFromFile,
    make_odin,
)

from PyConf.Algorithms import (
    VPFullCluster2MCParticleLinker, VPFullCluster2MCHitLinker,
    PrLHCbID2MCParticle, PrLHCbID2MCParticleVPUTFTMU, PrTrackAssociator,
    PrTrackChecker, PrUTHitChecker, TrackListRefiner, TrackResChecker,
    PrMultiplicityChecker, TrackIPResolutionCheckerNT, MCParticle2MCHitAlg,
    PrTrackerDumper, PVDumper, PrimaryVertexChecker,
    LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertices as
    FromVectorLHCbRecVertex)

from PyConf.Tools import (IdealStateCreator, LoKi__Hybrid__MCTool,
                          VisPrimVertTool, LoKi__Hybrid__TrackSelector as
                          LoKiTrackSelector, TrackMasterExtrapolator,
                          TrackDistanceExtraSelector)

from RecoConf.hlt1_tracking import (
    make_PrStoreUTHit_hits, make_FTRawBankDecoder_clusters,
    make_velo_full_clusters, make_VPClus_hits, make_PrStoreFTHit_hits,
    make_VeloClusterTrackingSIMD_hits, make_VeloClusterTrackingSIMD_tracks)

from Hlt2Conf.data_from_file import (boole_links_digits_mcparticles,
                                     boole_links_digits_mchits, mc_unpackers)

from RecoConf.mc_checking_categories import get_mc_categories, get_hit_type_mask, categories

from RecoConf.hlt1_muonid import make_muon_hits


def get_item(x, key):
    """Return `key` from `x` if `x` is a dict, otherwise return `x`.

    TODO: This helper function can be replaces once Moore!63 has been addressed.
    """
    if isinstance(x, DataHandle): return x
    return x[key]


@configurable
def make_links_veloclusters_mcparticles():
    return VPFullCluster2MCParticleLinker(
        ClusterLocation=make_velo_full_clusters(),
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPDigit2MCParticleLinksLocation=boole_links_digits_mcparticles()
        ["VPDigits"],
    ).OutputLocation


@configurable
def make_links_veloclusters_mchits():
    return VPFullCluster2MCHitLinker(
        ClusterLocation=make_velo_full_clusters(),
        MCHitLocation=mc_unpackers()["MCVPHits"],
        VPDigit2MCHitLinksLocation=boole_links_digits_mchits()["VPDigits"],
    ).OutputLocation


@configurable
def make_links_lhcbids_mcparticles_tracking_system():
    return PrLHCbID2MCParticle(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        UTHitsLinkLocation=boole_links_digits_mcparticles()["UTClusters"],
        FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
        FTLiteClustersLinkLocation=boole_links_digits_mcparticles()
        ["FTLiteClusters"],
    ).TargetName


@configurable
def make_links_lhcbids_mcparticles_tracking_and_muon_system():
    """Match lhcbIDs of hits in the tracking stations and muon stations to MCParticles.
    """
    return PrLHCbID2MCParticleVPUTFTMU(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPFullClustersLocation=make_velo_full_clusters(),
        VPFullClustersLinkLocation=make_links_veloclusters_mcparticles(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        UTHitsLinkLocation=boole_links_digits_mcparticles()["UTClusters"],
        FTLiteClustersLocation=make_FTRawBankDecoder_clusters(),
        FTLiteClustersLinkLocation=boole_links_digits_mcparticles()
        ["FTLiteClusters"],
        MuonHitsLocation=make_muon_hits(),
        MuonHitsLinkLocation=boole_links_digits_mcparticles()["MuonDigits"],
    ).TargetName


@configurable
def make_links_tracks_mcparticles(InputTracks, LinksToLHCbIDs):
    return PrTrackAssociator(
        SingleContainer=get_item(InputTracks, "v1"),
        LinkerLocationID=LinksToLHCbIDs,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation


@configurable
def monitor_tracking_efficiency(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
        HitTypesToCheck,
        WriteHistos=2,
):
    """ Setup tracking efficiency checker

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(
        Title=TrackType,
        HitTypesToCheck=HitTypesToCheck,
        MyCuts=MCCategories,
        WriteHistos=WriteHistos,
    )
    return PrTrackChecker(
        name=TrackType + "TrackChecker",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=LinksToLHCbIDs,
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LoKiFactory=LoKi__Hybrid__MCTool(Modules=["LoKiMC.decorators"]),
        **props)


@configurable
def monitor_tracking_efficiency_vs_multiplicity(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
):
    """ Setup tracking efficiency checker vs multiplicity

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(Title=TrackType, )
    return PrMultiplicityChecker(
        name=TrackType + "MultiplicityChecker",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        Velo_Hits=make_VeloClusterTrackingSIMD_hits(),
        Velo_Tracks=make_VeloClusterTrackingSIMD_tracks()['Pr'],
        UT_Hits=make_PrStoreUTHit_hits(),
        FT_Hits=make_PrStoreFTHit_hits(),
        **props)


@configurable
def monitor_uthit_efficiency(
        TrackType,
        InputTracks,
        LinksToTracks,
        LinksToLHCbIDs,
        MCCategories,
        WriteHistos=1,
):
    """ Setup UT hit efficiency checker

    Args:
        InputTracks (dict or DataHandle): The InputTracks can be either a dict with the key v1 or a DataHandle to LHCb::Event::v1::Tracks directly.
    """
    props = dict(
        Title=TrackType + "UTHits",
        WriteHistos=WriteHistos,
        MyCuts=MCCategories,
    )

    return PrUTHitChecker(
        name=TrackType + "UTHitsChecker",
        Tracks=get_item(InputTracks, "v1"),
        Links=LinksToTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"],
        LinkTableLocation=LinksToLHCbIDs,
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        LoKiFactory=LoKi__Hybrid__MCTool(Modules=["LoKiMC.decorators"]),
        **props)


@configurable
def get_track_checkers(
        types_and_locations,
        uthit_efficiency_types=["Forward", "Downstream", "Match"],
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system
):
    """ Setup track and UT hit efficiency checkers

    The track type has to correspond to a key mc_checking_categories.categories. This defines which MC particle categories are tested.

    Args:
        types_and_locations (dict): Types and locations of track containers.
        uthit_efficiency_types (list of str): Types for which dedicated UT hit efficiency checker is setup.
        make_links_lhcbids_mcparticles (function): Maker that returns ???
    """

    assert isinstance(
        types_and_locations,
        dict), "Please provide a dictionary of track type and tracks"

    efficiency_checkers = []
    links_to_lhcbids = make_links_lhcbids_mcparticles()
    # TODO (Python 3 compatibility) remove sort once we drop Python 2 support
    # so that we keep the more meaningful order of items as defined originally.
    for track_type, tracks in sorted(types_and_locations.items()):
        assert track_type in categories, track_type + " unknown. Please chose from " + ", ".join(
            categories)

        links_to_tracks = make_links_tracks_mcparticles(
            InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)

        pr_checker = monitor_tracking_efficiency(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type),
            HitTypesToCheck=get_hit_type_mask(track_type),
        )
        efficiency_checkers.append(pr_checker)

        if track_type not in uthit_efficiency_types:
            continue
        uthit_checker = monitor_uthit_efficiency(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type + "UTHits"),
        )
        efficiency_checkers.append(uthit_checker)
    return efficiency_checkers


@configurable
def get_track_checkers_multiplicity(
        types_and_locations,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system
):
    """ Setup track efficiency vs multiplicity checker

    The track type has to correspond to a key mc_checking_categories.categories. This defines which MC particle categories are tested.

    Args:
        types_and_locations (dict): Types and locations of track containers.
        make_links_lhcbids_mcparticles (function): Maker that returns ???
    """

    assert isinstance(
        types_and_locations,
        dict), "Please provide a dictionary of track type and tracks"

    efficiency_checkers = []
    links_to_lhcbids = make_links_lhcbids_mcparticles()
    # TODO (Python 3 compatibility) remove sort once we drop Python 2 support
    # so that we keep the more meaningful order of items as defined originally.
    for track_type, tracks in sorted(types_and_locations.items()):
        assert track_type in categories, track_type + " unknown. Please chose from " + ", ".join(
            categories)

        links_to_tracks = make_links_tracks_mcparticles(
            InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)

        pr_multiplicity_checker = monitor_tracking_efficiency_vs_multiplicity(
            TrackType=track_type,
            InputTracks=tracks,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(track_type),
        )
        efficiency_checkers.append(pr_multiplicity_checker)

    return efficiency_checkers


@configurable
def get_fitted_tracks_checkers(
        FittedTracks,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system,
        light_reco=False):

    links_to_lhcbids = make_links_lhcbids_mcparticles()

    fitted_track_types = ["BestLong", "BestDownstream"]
    links_to_tracks = {}
    for track_type in fitted_track_types:
        links_to_tracks[track_type] = make_links_tracks_mcparticles(
            InputTracks=FittedTracks[track_type],
            LinksToLHCbIDs=links_to_lhcbids)

    good_long_tracks = make_track_filter(
        InputTracks=FittedTracks["BestLong"], code="(TrGHOSTPROB < 0.5)")
    good_downstream_tracks = make_track_filter(
        InputTracks=FittedTracks["BestDownstream"], code="(TrGHOSTPROB < 0.5)")

    efficiency_checkers = []
    for tracks, tr_key, mc_key, hit_key in [
        (FittedTracks["BestLong"], "BestLong", "BestLong", "BestLong"),
        (good_long_tracks, "LongGhostFiltered", "BestLong", "BestLong"),
        (FittedTracks["BestDownstream"], "BestDownstream", "BestDownstream",
         "BestDownstream"),
        (good_downstream_tracks, "DownstreamGhostFiltered", "BestDownstream",
         "BestDownstream"),
    ]:
        checker = monitor_tracking_efficiency(
            InputTracks=tracks,
            TrackType=tr_key,
            LinksToTracks=links_to_tracks[mc_key],
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(mc_key),
            HitTypesToCheck=get_hit_type_mask(hit_key),
        )
        efficiency_checkers.append(checker)
    return efficiency_checkers


@configurable
def get_best_tracks_checkers(
        BestTracks,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system,
        include_resolution_checkers=True):

    links_to_lhcbids = make_links_lhcbids_mcparticles()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=BestTracks, LinksToLHCbIDs=links_to_lhcbids)

    long_tracks = make_track_filter(
        InputTracks=BestTracks, code="(~TrINVALID) & TrLONG ")
    downstream_tracks = make_track_filter(
        InputTracks=BestTracks, code="(~TrINVALID) & TrDOWNSTREAM ")
    good_long_tracks = make_track_filter(
        InputTracks=BestTracks,
        code="(~TrINVALID) & TrLONG & (TrGHOSTPROB < 0.5) ")
    good_downstream_tracks = make_track_filter(
        InputTracks=BestTracks,
        code="(~TrINVALID) & TrDOWNSTREAM & (TrGHOSTPROB < 0.5)")

    efficiency_checkers = []
    for tracks, tr_key, mc_key, hit_key in [
        (BestTracks, "Best", "Best", "Best"),
        (long_tracks, "BestLong", "BestLong", "BestLong"),
        (good_long_tracks, "LongGhostFiltered", "BestLong", "BestLong"),
        (downstream_tracks, "BestDownstream", "BestDownstream",
         "BestDownstream"),
        (good_downstream_tracks, "DownstreamGhostFiltered", "BestDownstream",
         "BestDownstream"),
    ]:
        checker = monitor_tracking_efficiency(
            InputTracks=tracks,
            TrackType=tr_key,
            LinksToTracks=links_to_tracks,
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(mc_key),
            HitTypesToCheck=get_hit_type_mask(hit_key),
        )
        efficiency_checkers.append(checker)

    if include_resolution_checkers:
        resolution_checker = monitor_track_resolution(BestTracks)
        efficiency_checkers.append(resolution_checker)

    return efficiency_checkers


@configurable
def get_pv_checkers(pvs, tracks, produce_ntuple=False):

    assert isinstance(
        pvs, DataHandle), "Please provide reconstructed primary verticies"

    pv_checkers = []

    pvchecker = PrimaryVertexChecker(
        produceNtuple=produce_ntuple,
        inputVerticesName=pvs,
        inputTracksName=tracks["v1"],
        MCVertexInput=mc_unpackers()["MCVertices"],
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCHeaderLocation=make_data_with_FetchDataFromFile("/Event/MC/Header"),
        MCPropertyInput=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"))

    pv_checkers.append(pvchecker)
    return pv_checkers


def make_track_filter(InputTracks, code):
    selector = LoKiTrackSelector(Code=code)
    filtered_tracks = TrackListRefiner(
        inputLocation=InputTracks["v1"], Selector=selector).outputLocation
    return filtered_tracks


def make_default_IdealStateCreator(public=False):
    mcpart = mc_unpackers()["MCParticles"]
    mcvert = mc_unpackers()["MCVertices"]
    vphits = mc_unpackers()["MCVPHits"]
    uthits = mc_unpackers()["MCUTHits"]
    fthits = mc_unpackers()["MCFTHits"]

    link_vp_hits = MCParticle2MCHitAlg(MCParticles=mcpart, MCHitPath=vphits)
    link_ut_hits = MCParticle2MCHitAlg(MCParticles=mcpart, MCHitPath=uthits)
    link_ft_hits = MCParticle2MCHitAlg(MCParticles=mcpart, MCHitPath=fthits)

    return IdealStateCreator(
        Extrapolator=TrackMasterExtrapolator(
            ExtraSelector=TrackDistanceExtraSelector(shortDist=0)),
        MCVertices=mcvert,
        VPMCHits=vphits,
        VPMCHitLinks=link_vp_hits,
        UTMCHits=uthits,
        UTMCHitLinks=link_ut_hits,
        FTMCHits=fthits,
        FTMCHitLinks=link_ft_hits,
        public=public)


@configurable
def monitor_track_resolution(InputTracks,
                             per_hit_resolutions=False,
                             split_per_type=False):
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks["v1"], LinksToLHCbIDs=links_to_lhcbids)
    mcpart = mc_unpackers()["MCParticles"]
    mchead = make_data_with_FetchDataFromFile('/Event/MC/Header')
    mcprop = make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')
    res_checker = TrackResChecker(
        TracksInContainer=InputTracks["v1"],
        MCParticleInContainer=mc_unpackers()["MCParticles"],
        LinkerInTable=links_to_tracks,
        VisPrimVertTool=VisPrimVertTool(
            public=True,
            MCHeader=mchead,
            MCParticles=mcpart,
            MCProperties=mcprop),
        StateCreator=make_default_IdealStateCreator(public=True),
        FullDetail=per_hit_resolutions,
        SplitByType=split_per_type)

    return res_checker


def monitor_IPresolution(InputTracks, InputPVs, VeloTracks):

    vertexConverter = FromVectorLHCbRecVertex(
        InputVerticesName=InputPVs,
        InputTracksName=VeloTracks).OutputVerticesName
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks, LinksToLHCbIDs=links_to_lhcbids)
    IPres_checker = TrackIPResolutionCheckerNT(
        TrackContainer=InputTracks,
        MCParticleInput=mc_unpackers()["MCParticles"],
        MCHeaderLocation=make_data_with_FetchDataFromFile("/Event/MC/Header"),
        LinkerLocation=links_to_tracks,
        PVContainer=vertexConverter,
        NTupleLUN="FILE1")
    return IPres_checker


def tracker_dumper(odin_location=make_odin,
                   root_output_dir="dump/TrackerDumper",
                   bin_output_dir="dump/MC_info/tracks",
                   dump_to_root=True,
                   dump_to_binary=False):
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()

    return PrTrackerDumper(
        MCParticlesLocation=mc_unpackers()["MCParticles"],
        VPLightClusterLocation=make_VPClus_hits(),
        FTHitsLocation=make_PrStoreFTHit_hits(),
        UTHitsLocation=make_PrStoreUTHit_hits(),
        ODINLocation=odin_location(),
        LinkerLocation=links_to_lhcbids,
        DumpToROOT=dump_to_root,
        DumpToBinary=dump_to_binary,
        OutputDirectory=root_output_dir,
        MCOutputDirectory=bin_output_dir)


def pv_dumper(odin_location=make_odin, output_dir="dump/MC_info/PVs"):
    return PVDumper(
        MCVerticesLocation=mc_unpackers()["MCVertices"],
        MCPropertyLocation=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        ODINLocation=odin_location(),
        OutputDirectory=output_dir)
