###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import (
    options,
    run_reconstruction,
)
from RecoConf.rich_add_reconstruction_monitoring_checking import (
    add_hlt2_rich, )

from RecoConf.standalone import (
    standalone_hlt2_reco, )

# save output file with histograms
options.histo_file = 'hlt2_reco_baseline_with_data_monitoring_alignment_rich.root'

with add_hlt2_rich.bind(
        do_data_monitoring_rich=True,
        moni_set="Alignment",
        #moni_set="PhotonCherenkovAngles",
        radiator="Rich1Gas",
        align_tasks=
    [
        'Produce',  # fill the production set of histograms
        'Monitor',  # add various checking histograms
        'Map',  # add counters for creation of the HLT1 pre-selection line "map"
        'Optimize',  # add counters for optimization of the RICH2 mirror combinations subset
        'Explore',  # explore influence of RICH1 coordinate systems on distribution shapes
    ],
        track_types=[
            'Long',
            'Upstream',
            'Downstream',
        ],
):
    run_reconstruction(options, standalone_hlt2_reco)
