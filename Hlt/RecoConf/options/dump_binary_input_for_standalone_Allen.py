###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt1_allen import make_dumped_raw_banks
from RecoConf.mc_checking import tracker_dumper, pv_dumper

outputDir = "dump/"


def dump_allen_binary_input():
    """Dump input required for Allen standalone running into binary files

    The following input is dumped:
    - Raw bank content of the Velo, UT, SciFi, Muon, ODIN banks
    - MC info from the TrackerDumper required for the Allen standalone checker
    - Non-event data such as geometry information
    """

    data = []
    data.append(make_dumped_raw_banks(output_dir=outputDir + "banks"))
    data.append(
        tracker_dumper(
            dump_to_root=False,
            dump_to_binary=True,
            bin_output_dir=outputDir + "MC_info/tracks"))
    data.append(pv_dumper(output_dir=outputDir + "MC_info/PVs"))

    return Reconstruction('allen_dump', data)


run_allen_reconstruction(options, dump_allen_binary_input, True)
