###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This set of options is used for reconstruction development purposes,
and assumes that the input contains MCHits (i.e. is of `Exended`
DST/digi type).
"""

from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco

from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.hlt1_tracking import require_gec
from RecoConf.hlt2_tracking import make_hlt2_tracks
from RecoConf.mc_checking import monitor_track_resolution

options.histo_file = 'hlt2_lead_lead_track_monitoring_with_mc.root'
options.ntuple_file = 'hlt2_lead_lead_track_monitoring_with_mc.root'
options.evt_max = 50

# MuonID is currently not thread-safe, temporarily enable it at least in this test which is running single-threaded anyhow.
# A higher gec cut than 40000 leads to unreasonable processing times for the nightlies.
with standalone_hlt2_light_reco.bind(do_mc_checking=True, do_data_monitoring=True), \
     monitor_track_resolution.bind(per_hit_resolutions=False, split_per_type=True), \
     make_charged_protoparticles.bind(enable_muon_id=True),\
     require_gec.bind(cut=40000),\
     make_hlt2_tracks.bind(use_pr_kf=True):
    run_reconstruction(options, standalone_hlt2_light_reco)
