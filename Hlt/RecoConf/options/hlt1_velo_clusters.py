###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import TestVeloClusters
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Moore.config import setup_allen_non_event_data_service
from RecoConf.hlt1_allen import call_allen, make_allen_output
from Moore import options

# Event numbers
options.set_input_and_conds_from_testfiledb('upgrade-baseline-FT64-digi')
options.histo_file = "test_velo_clusters.root"
options.evt_max = 1000

config = configure_input(options)

setup_allen_non_event_data_service()

with call_allen.bind(filter_gec=True):
    allen_wrapper = make_allen_output()
test_clusters = TestVeloClusters(AllenOutput=allen_wrapper)

cf_node = CompositeNode(
    'test_velo_clusters',
    combine_logic=NodeLogic.LAZY_AND,
    children=[test_clusters],
    force_order=True)

config.update(configure(options, cf_node))
