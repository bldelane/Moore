###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_reco
from RecoConf.hlt1_tracking import default_ft_decoding_version
from RecoConf.protoparticles import make_charged_protoparticles

options.set_input_and_conds_from_testfiledb('upgrade-TELL40-UT-miniBias')
default_ft_decoding_version.global_bind(value=6)

# MuonID is currently not thread-safe, temporarily enable it at least in this test which is running single-threaded anyhow.
with standalone_hlt2_reco.bind(do_mc_checking=True, do_data_monitoring=True), \
     make_charged_protoparticles.bind(enable_muon_id=True):
    run_reconstruction(options, standalone_hlt2_reco)
