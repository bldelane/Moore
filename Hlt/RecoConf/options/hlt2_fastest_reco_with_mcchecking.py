###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_light_reco

from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.hlt2_tracking import make_hlt2_tracks
from RecoConf.calorimeter_reconstruction import make_photons_and_electrons, make_electron_and_brem_match

with make_photons_and_electrons.bind(selective_matching=True), \
     make_electron_and_brem_match.bind(selective_matching=True),\
     make_hlt2_tracks.bind(use_pr_kf=True),\
     standalone_hlt2_light_reco.bind(do_mc_checking=True, do_data_monitoring=True, fast_reco=True), \
     make_charged_protoparticles.bind(enable_muon_id=True):
    run_reconstruction(options, standalone_hlt2_light_reco)
