2018-04-20 Moore v28r2
========================================

Production version for 2018
----------------------------------------
Based on Gaudi v29r4, LHCb v44r2, Lbcom v22r0p2, Rec v23r2, Phys v25r2, Hlt v28r2
This version is released on the 2018-patches branch.
