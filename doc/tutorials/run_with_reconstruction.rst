Enable real-time reconstruction
===============================

When running Moore during data-taking, a HLT2 trigger line will take the raw data, run the reconstruction algorithms to reconstruct tracks and particle identification objects and, subsequently, execute selection algorithms, most times in the form of reconstructing decay chains, to trigger on an event and write out the necessary information. Executing the selection algorithms of a single HLT2 trigger line takes little time compared to the reconstruction algorithms.

To provide more flexibility and faster turn around times, Moore supports two running modes. One mode where the reconstruction has been preprocessed and saved in a DST and one mode where the reconstruction is run in real-time. The former has the advantage of being faster but the disadvantage that the reconstruction is the one which was used to produce the input file. Since the reconstruction algorithms themselves are still in development, they can change. To use the latest developments one needs to use the option which runs the reconstruction in real-time. The goal is to keep the differences between the two small but it can happen that for certain use cases it is necessary to run the reconstruction in real-time. An example would be to test the impact of a new reconstruction algorithm on an HLT2 trigger line.

Options
-------

The current default when running a HLT2 trigger line is to take the reconstruction from file. To enable the real-time reconstruction, one has to add the following lines to an options file::

    from RecoConf.reconstruction_objects import reconstruction

    with reconstruction.bind(from_file=False):
        run_moore(options, make_lines, public_tools)

A full example can be found `here <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/options/hlt2_all_lines_with_reco.py>`_.

Implementation
--------------

The ability to switch between the two options is implemented in the module `RecoConf.reconstruction_objects <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/RecoConf/python/RecoConf/reconstruction_objects.py>`_. The function ``reconstruction`` has the configurable parameter ``from_file``. Depending on the value, getting the reconstruction is either delegated to the module `RecoConf.reco_objects_from_file <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/RecoConf/python/RecoConf/reco_objects_from_file.py>`_ (``from_file=True``) or `RecoConf.RecoConf.hlt2_global_reco <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/RecoConf/python/RecoConf/hlt2_global_reco.py>`_ (``from_file=False``). If there is no specific reason, reconstruction objects should always be imported from the module ``RecoConf.reconstruction_objects`` in HLT2 lines, otherwise the global switch will not work.


