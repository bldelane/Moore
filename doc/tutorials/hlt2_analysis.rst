Analysing HLT2 output
=====================

After :doc:`hlt2_line`, it can be useful to analyse the candidates produced
by some HLT2 lines. This means telling Moore to produce an output file, and
then running over this file with a subsequent application, typically DaVinci.

Enabling HLT2 output
--------------------

Moore will write output if it's given a name and type for the output file.
This is done by setting properties on the
`PyConf.application.ApplicationOptions` object passed to `Moore.run_moore`::

    from Moore import options, run_moore

    options.output_file = 'hlt2_example.dst'
    options.output_type = 'ROOT'

    # Assuming the `all_lines` function is defined elsewhere
    run_moore(options, all_lines)

The file ``hlt2_example.dst`` will be produced in the directory you run Moore in.

Some of the Moore configuration parameters must also be written out, so that
they can be used to configure the reading in DaVinci. This can be done by
adding a few lines to the *very end* of the Moore options file::

    import json
    from Configurables import HltANNSvc
    with open("hlt2_tck.json", "w") as outfile:
        json.dump(HltANNSvc().PackedObjectLocations, outfile)

.. note::

    The information saved to the ``hlt2_tck.json`` file can only be used to
    analyse the DST file that was produced alongside it.

    If you try to use a JSON file from a different run of Moore when
    analysing the DST output file you may encounter strange and potentially
    unreported errors.

Output format
-------------

Output objects created by HLT2 lines are placed in the transient event store
(TES) in predefined locations. The lines _candidates_, whose existence
defines whether or not the line fired, are placed in the location::

    /Event/HLT2/<HLT2 line name>/Particles

The HLT2 line name is defined by the string passed to the initialisation of
`Moore.lines.Hlt2Line`::

    def d0_to_kpi():
        dzeros = make_dzeros()
        return Hlt2Line(name="Hlt2CharmD0ToKmPipLine", algs=[dzeros])

In this example, the line has the name ``Hlt2CharmD0ToKmPipLine``, so the
candidates (the objects created by the ``dzeros`` algorithm) will be stored
in the output file at::

    /Event/HLT2/Hlt2CharmD0ToKmPipLine/Particles

Extra outputs are stored in a similar hierarchy::

    /Event/HLT2/<HLT2 line name>/<extra selection name>/Particles

Given this line maker::

    def d0_to_kpi():
        dzeros = make_dzeros()
        # Select pions and kaons that vertex well with the trigger candidate
        soft_pions = make_soft_pions(dzeros)
        soft_kaons = make_soft_kaons(dzeros)
        return Hlt2Line(
            name="Hlt2CharmD0ToKmPipLine",
            algs=[dzeros],
            extra_outputs=[
                ("SoftPions", soft_pions),
                ("SoftKaons", soft_kaons),
            ]
        )

The first element of each 2-tuple in `extra_outputs` defines the extra
selection's name. So, the soft pions will be available in the output file
at::

    /Event/HLT2/Hlt2CharmD0ToKmPipLine/SoftPions/Particles

Currently, the locations of other objects in the event, such tracks and
primary vertices, are not guaranteed by Moore and may change in future
versions.

Reading data with DaVinci
-------------------------

Today, the HLT2 output format is similar to that produced by the Run 2
application Tesla. If you've `analysed Turbo data`_ this configuration will look
familiar to you::

    from Configurables import (
        ApplicationMgr,
        DecayTreeTuple,
        LHCbApp,
    )
    from Gaudi.Configuration import appendPostConfigAction
    from DecayTreeTuple import Configuration
    from PhysSelPython.Selections import (
        AutomaticData,
        CombineSelection,
        SelectionSequence,
    )

    # The output of the HLT2 line
    kpi_line = AutomaticData("/Event/HLT2/Hlt2CharmD0ToKmPipLine/Particles")
    # Extra pions
    soft_pions = AutomaticData("/Event/HLT2/Hlt2CharmD0ToKmPipLine/SoftPions/Particles")
    dstars = CombineSelection(
        "CombineD0pi",
        inputs=[kpi_line, soft_pions],
        DecayDescriptors=[
            "D*(2010)+ -> D0 pi+",
            "D*(2010)- -> D0 pi+",
        ],
        DaughtersCuts={
            "pi+": "PT > 250 * MeV",
        },
        CombinationCut=(
            "in_range(0, (AM - AM1 - AM2), 170)"
        ),
        MotherCut=(
            "(VFASPF(VCHI2PDOF) < 10) &"
            "in_range(0, (M - M1 - M2), 150)"
        )
    )
    selseq = SelectionSequence(
        dstars.name() + "Sequence",
        TopSelection=dstars
    )

    dtt_kpi = DecayTreeTuple(
        "TupleD0ToKpi",
        Inputs=[kpi_line.outputLocation()],
        Decay="D0 -> ^K- ^pi+",
    )
    dtt_kpi.addBranches({
        "D0": "D0 -> K- pi+",
        "D0_h1": "D0 -> ^K- pi+",
        "D0_h2": "D0 -> K- ^pi+",
    })

    dtt_kpi_dst = DecayTreeTuple(
        "TupleDstToD0pi_D0ToKpi",
        Inputs=[selseq.outputLocation()],
        Decay="[D*(2010)+ -> ^([D0]cc -> ^K- ^pi+) ^pi+]CC",
    )
    dtt_kpi_dst.addBranches({
        "Dst": "[D*(2010)+ -> ([D0]cc -> K- pi+) pi+]CC",
        "Dst_pi": "[D*(2010)+ -> ([D0]cc -> K- pi+) ^pi+]CC",
        "D0": "[D*(2010)+ -> ^([D0]cc -> K- pi+) pi+]CC",
        "D0_h1": "[D*(2010)+ -> ([D0]cc -> ^K- pi+) pi+]CC",
        "D0_h2": "[D*(2010)+ -> ([D0]cc -> K- ^pi+) pi+]CC",
    })

    LHCbApp().DataType = "Upgrade"
    ApplicationMgr().TopAlg = [selseq.sequence(), dtt_kpi, dtt_kpi_dst]
    LHCbApp().TupleFile = "hlt2_example.root"

    @appendPostConfigAction
    def read_hlt2():
        """Configures algorithms for reading HLT2 output.

        This is a temporary measure until support for Run 3 HLT2 output is added to
        an LHCb application.
        """
        import json

        from Configurables import HltANNSvc, createODIN
        from GaudiConf import reading

        # NOTE: You will need to set `raw_event_format` below to 0.3 if:
        # - Your Moore output was MDF or;
        # - Your Moore input was (X)DIGI.
        reading_algs = (
            [reading.decoder(raw_event_format=4.3)] +
            reading.unpackers() +
            [createODIN()]
        )
        if LHCbApp().Simulation:
            reading_algs = reading.mc_unpackers() + reading_algs
        ApplicationMgr().TopAlg = reading_algs + ApplicationMgr().TopAlg

        # Load the 'TCK' dumped from the Moore job
        with open("hlt2_tck.json") as f:
            tck = json.load(f)
            HltANNSvc(PackedObjectLocations={str(k): v for k, v in tck.items()})

These options demonstrate using the output of extra selections. If your line
doesn't produce these, the corresponding options will look a lot simpler (you
will only need the `dtt_kpi` algorithm).

Additional options are needed to define the input data, the database tags, and
the simulation flag. These typically looks like this, but may be different for
your specific use-case::

    from GaudiConf import IOHelper

    LHCbApp().Simulation = True
    LHCbApp().CondDBtag = "sim-20171127-vc-md100"
    LHCbApp().DDDBtag = "dddb-20171126"
    IOHelper().inputFiles(["./hlt2_example.dst"])

.. note::

    These options do not use the ``DaVinci`` configurable, but ``LHCbApp``.
    However, you do still need to run these options within the DaVinci
    runtime environment.

    You can run your options within the ``DaVinci`` runtime environment using
    ``lb-run``::

        lb-run DaVinci/VERSION gaudirun.py OPTIONS

    Or using your local stack (see :doc:`developing` for details)::

        ./DaVinci/run gaudirun.py OPTIONS


Monte Carlo association
-----------------------

If you ran Moore over a DST-like input, you will have Monte Carlo relations
tables available in the output. They are persisted in the two locations
defined by the values of :py:const:`Moore.persistence.truth_matching.CHARGED_PP2MC_LOC` and
:py:const:`Moore.persistence.truth_matching.NEUTRAL_PP2MC_LOC`.

To access them, you will first need these lines in your options file::

    from DecayTreeTuple import DecayTreeTupleTruthUtils


    relations = [
        "Relations/ChargedPP2MCP",
        "Relations/NeutralPP2MCP",
    ]
    mc_tools = [
        'MCTupleToolKinematic',
        # ...plus any other MC tuple tools you'd like to use
    ]

To include tuple tools that use truth information, you need to ensure the
``ToolList`` includes the ``TupleToolMCBackgroundInfo`` and
``TupleToolMCTruth`` tools, and then call a helper method::

    dtt.ToolList += [
        "TupleToolMCBackgroundInfo",
        "TupleToolMCTruth",
    ]
    DecayTreeTupleTruthUtils.makeTruth(dtt, relations, mc_tools, stream="/Event/HLT2")

Here, ``dtt`` is the ``DecayTreeTuple`` instance you want to add the truth
tools to.

.. _analysed Turbo data: https://twiki.cern.ch/twiki/bin/view/LHCb/MakeNTupleFromTurbo
